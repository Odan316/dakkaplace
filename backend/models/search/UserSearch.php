<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\user\User;
use yii\db\Query;
use yii\rbac\DbManager;
use yii\rbac\Item;

/**
 * UserSearch represents the model behind the search form about `common\models\user\User`.
 *
 * @see User
 */
class UserSearch extends User
{
    public $role;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [ [ 'status', 'createdAt', 'updatedAt', 'status' ], 'integer' ],
            [ [ 'username', 'nickname', 'email', 'role' ], 'safe' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()
            ->alias('u');


        /** @var DbManager $authManager */
        $authManager = Yii::$app->authManager;
        $subQuery = (new Query)->select('r2u.*')
            ->from([ 'r2u' => $authManager->assignmentTable, 'r' => $authManager->itemTable ])
            ->where('{{r2u}}.[[item_name]]={{r}}.[[name]]')
            ->andWhere([ 'r.type' => Item::TYPE_ROLE ]);


        $query->leftJoin([ 'rolesInfo' => $subQuery ], 'rolesInfo.user_id = u.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'username',
                'nickname',
                'email',
                'status',
                'createdAt',
                'role' => [
                    'asc'  => [ 'rolesInfo.item_name' => SORT_ASC ],
                    'desc' => [ 'rolesInfo.item_name' => SORT_DESC ]
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'id'     => $this->id,
            'status' => $this->status,
            'created_at' => $this->createdAt,
        ]);

        $query->andFilterWhere([ 'like', 'username', $this->username ])
            ->andFilterWhere([ 'like', 'nickname', $this->nickname ])
            ->andFilterWhere([ 'like', 'email', $this->email ]);

        $query->andFilterWhere([ 'rolesInfo.item_name' => $this->role ]);

        return $dataProvider;
    }
}
