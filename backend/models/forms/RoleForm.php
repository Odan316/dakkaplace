<?php

namespace backend\models\forms;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\rbac\DbManager;
use yii\rbac\Permission;
use yii\rbac\Role;

/**
 * Class RoleForm
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 */
class RoleForm extends Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /** @var  Role */
    protected $role;
    /** @var Permission[]|null  */
    protected $_availablePermissions = null;
    /** @var Permission[]|null  */
    protected $_rolePermissions = null;
    /** @var array|null  */
    protected $_rolePermissionsFlat = null;

    /** @var array  */
    public $rolePermissions = [];

    /** @var string */
    public $roleName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [ [ 'rolePermissions' ], 'safe', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE] ],

            [ [ 'roleName' ], 'string', 'on' => self::SCENARIO_CREATE ],
            [ [ 'roleName' ], 'required', 'on' => self::SCENARIO_CREATE ],
        ];
    }

    public function setNewRole()
    {
        $this->role = null;
        $this->setPermissionsLists();
    }

    /**
     * @param Role $role
     */
    public function setRole($role)
    {
        $this->role = $role;
        $this->setPermissionsLists();

    }

    private function setPermissionsLists()
    {
        $this->getRolePermissions();
        $this->getAvailablePermissions();
    }


    /**
     * @return array|null|\yii\rbac\Permission[]
     */
    public function getRolePermissions()
    {
        if ($this->_rolePermissions === null) {
            $this->_rolePermissions = [ ];

            /** @var DbManager $authManager */
            $authManager = Yii::$app->authManager;

            if($this->role === null){
                $this->_rolePermissions = [];
            } else {
                $this->_rolePermissions = $authManager->getPermissionsByRole($this->role->name);
            }
            $this->_rolePermissionsFlat = ArrayHelper::map($this->_rolePermissions, 'name', 'name');
        }

        return $this->_rolePermissions;
    }


    /**
     * @return array|null|\yii\rbac\Permission[]
     */
    public function getAvailablePermissions()
    {
        if ($this->_availablePermissions === null) {
            $this->_availablePermissions = [ ];

            /** @var DbManager $authManager */
            $authManager = Yii::$app->authManager;

            $this->_availablePermissions = $authManager->getPermissions();
        }

        return $this->_availablePermissions;
    }

    public function create()
    {
        /** @var DbManager $authManager */
        $authManager = Yii::$app->authManager;

        $this->role = $authManager->createRole($this->roleName);
        $authManager->add($this->role);

        foreach ($this->rolePermissions as $permName) {
            $permission = $authManager->getPermission($permName);
            $authManager->addChild($this->role, $permission);
        }

        return true;
    }

    public function update()
    {

        /** @var DbManager $authManager */
        $authManager = Yii::$app->authManager;

        $currentPermissions = $this->_rolePermissionsFlat;

        $toAdd = array_diff($this->rolePermissions, $currentPermissions);
        $toRemove = array_diff($currentPermissions, $this->rolePermissions);

        foreach ($toAdd as $permName) {
            $permission = $authManager->getPermission($permName);
            $authManager->addChild($this->role, $permission);
        }

        foreach ($toRemove as $permName) {
            $permission = $authManager->getPermission($permName);
            $authManager->removeChild($this->role, $permission);
        }

        return true;

    }

    /**
     * @param $permission
     * @return bool
     */
    public function hasPermission($permission)
    {
        return in_array($permission, $this->_rolePermissionsFlat);
    }
}