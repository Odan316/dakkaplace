<?php

namespace backend\models\forms;

use common\models\user\Status;
use Yii;
use yii\base\Model;
use common\models\user\User;

/**
 * User Create-Update form
 *
 * @property bool $isNewRecord
 *
 * @see User
 */
class UserForm extends Model
{

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $username;
    public $nickname;
    public $email;
    public $password;
    public $role;

    /** @var User */
    protected $user;

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['username', 'nickname', 'email', 'password', 'role'],
            self::SCENARIO_UPDATE => ['nickname', 'email', 'password', 'role'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nickname', 'email', 'role'], 'required'],
            [['password', 'username'], 'required', 'on' => self::SCENARIO_CREATE],

            [['username', 'nickname', 'email', 'password'], 'trim'],

            ['username', 'string', 'min' => 3, 'max' => 10],
            ['username', 'match', 'pattern' => '/^[0-9a-zA-Z]*$/i'],
            [
                'username',
                'unique',
                'targetClass' => '\common\models\user\User',
                'when'        => function ($model) {
                    return empty($model->user) || $model->username != $model->user->username;
                }
            ],

            ['nickname', 'string', 'min' => 2, 'max' => 64],
            [
                'nickname',
                'unique',
                'targetClass' => '\common\models\user\User',
                'when'        => function ($model) {
                    return empty($model->user) || $model->nickname != $model->user->nickname;
                }
            ],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => '\common\models\user\User',
                'when'        => function ($model) {
                    return empty($model->user) || $model->email != $model->user->email;
                }
            ],

            [
                'password',
                'string',
                'min'  => 6,
                'max'  => 12,
                'when' => function ($model) {
                    return !empty($model->password);
                },
            ],

            ['role', 'in', 'range' => array_keys(Yii::$app->authManager->getRoles())],
        ];
    }

    /**
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username'              => Yii::t('app/backend', "Username"),
            'nickname'              => Yii::t('app/backend', "Nickname"),
            'email'                 => Yii::t('app/backend', "Email"),
            'password'              => Yii::t('app/backend', "Password"),
            'role'                  => Yii::t('app/backend', "Role")
        ];
    }

    /**
     *
     * @return integer
     */
    public function getId()
    {
        return !empty($this->user) ? $this->user->id : false;
    }

    /**
     *
     * @param User $user
     */
    public function setUser($user)
    {
        if (!empty($user)) {
            $this->user = $user;
            $role = $this->user->getRole();
            $this->role = !empty($role) ? $role->name : '';
            $this->load($user->getAttributes(), '');
            $this->username = $user->username;
        }
    }

    /**
     * @return bool|null
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function create()
    {
        if (! $this->validate()) {
            return null;
        }

        $this->user = new User();
        $this->user->username = $this->username;
        $this->user->nickname = $this->nickname;
        $this->user->email = $this->email;
        $this->user->status = Status::active();
        $this->user->setPassword($this->password);
        $this->user->generateAuthKey();

        if ($this->user->save()) {
            if (! empty($this->role)) {
                $auth = Yii::$app->authManager;
                $role = $auth->getRole($this->role);
                $auth->assign($role, $this->user->getId());
            }

            return true;
        } else {
            $this->addErrors($this->user->getErrors());
            return false;
        }
    }

    /**
     * Updates user
     *
     * @return boolean
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function update()
    {
        if (!$this->validate()) {
            return null;
        }

        $this->user->nickname = $this->nickname;
        $this->user->email = $this->email;
        if (!empty($this->password)) {
            $this->user->setPassword($this->password);
        }

        if (!empty($this->role)) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole($this->role);
            $auth->revokeAll($this->user->getId());
            $auth->assign($role, $this->user->getId());
        }

        return $this->user->save();
    }

    /**
     * @return bool
     */
    public function getIsNewRecord()
    {
        return ! empty($this->user) ? $this->user->isNewRecord : true;
    }
}
