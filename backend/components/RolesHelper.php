<?php

namespace backend\components;

use yii\rbac\Permission;

/**
 * Class RolesHelper
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class RolesHelper
{
    /**
     * @param Permission[] $permissions
     * @return array
     */
    public static function convertPermsToTree($permissions)
    {
        $tree = [];

        $tempTree = [];
        foreach($permissions as $permission){
            $path = array_reverse(explode('/', $permission->name));
            $pathArray = $permission->name;
            foreach ($path as $part){
                $pathArray = [$part => $pathArray];
            }
            $tempTree[] = $pathArray;
        }

        foreach($tempTree as $subTree){
            $tree = array_merge_recursive($tree, $subTree);
        }


        return $tree;
    }
}