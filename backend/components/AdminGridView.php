<?php


namespace backend\components;


use yii\grid\GridView;

class AdminGridView extends GridView
{
    public $pager = [
        'class' => 'yii\bootstrap4\LinkPager',
        'listOptions' => ['class' => ['pagination pagination-sm']]
    ];

    public $tableOptions = ['class' => 'table table-striped table-bordered table-hover table-sm'];

    public $layout = "{pager}\n{items}\n{pager}";
}