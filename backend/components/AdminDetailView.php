<?php


namespace backend\components;


use yii\widgets\DetailView;

class AdminDetailView extends DetailView
{
    public $options = ['class' => 'table table-striped table-bordered detail-view table-sm'];
}