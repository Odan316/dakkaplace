<?php

namespace backend\components;

use yii\bootstrap4\ActiveForm;

/**
 * Class AdminActiveForm - version of bootstrap\AdminActiveForm, adapted to backend forms requirements
 * @package backend\components
 */
class AdminActiveForm extends ActiveForm
{

    /**
     * @var string the form layout. Either 'default', 'horizontal' or 'inline'.
     * By choosing a layout, an appropriate default field configuration is applied. This will
     * render the form fields with slightly different markup for each layout. You can
     * override these defaults through [[fieldConfig]].
     * @see \yii\bootstrap4\ActiveField for details on Bootstrap 4 field configuration
     */
    public $layout = 'horizontal';
}