��    *      l  ;   �      �     �     �     �  3   �  <   .  @   k     �     �     �     �     �          4  $   E  !   j     �     �     �  +   �     �                 A     R     g     �     �     �     �  ;   �          4     L     ]     o     �  #   �     �     �     �       i       	     �	     �	  R   �	  T   
  X   p
     �
     �
     �
       '        B     \  9   w  1   �     �     �     �  X        o  
   �     �     �     �  &   �  $   �               1  e   D     �     �     �     �     �  %   
  F   0     w  
   �     �  
   �                    )          (                              	   *   &      $                     !              '              "      
                                      %                       #          app/backendAccounts app/backendAll news &raquo; app/backendAll videos &raquo; app/backendAre you sure you want ban this account? app/backendAre you sure you want lift ban for this account? app/backendAre you sure you want manually confirm this account? app/backendBan app/backendChoose role app/backendConfirm app/backendCreate app/backendCreate new user app/backendDakkaPlace.Admin app/backendEdit app/backendEdit account: {username} app/backendEdit role: {roleName} app/backendEmail app/backendFactions app/backendGame Systems app/backendIncorrect username or password. app/backendLift ban app/backendLog in app/backendLog out ({username}) app/backendNews app/backendNickname app/backendNo news yet! app/backendNo videos yet! app/backendPassword app/backendPermissions app/backendPlaylists app/backendPlease fill out the following fields to log in: app/backendRegistered at app/backendRemember me app/backendRole app/backendRoles app/backendSave app/backendSelect permissions app/backendThis user is not active app/backendTournaments app/backendUsername app/backendUsers app/backendVideos Project-Id-Version: DakkaPlace
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
 Аккаунты Все новости &raquo; Все видео &raquo; Вы уверены, что хотите забанить этот аккаунт? Вы уверены, что хотите разбанить этот аккаунт? Вы уверены, что хотите подтвердить этот аккаунт? Забанить Выберите роль Подтвердить Создать Создать пользователя DakkaPlace.Админка Редактировать Редактирование аккаунта: {username} Редактирование роли: {roleName} Email Фракции Игровые системы Неверное сочетание имени пользователя и пароля. Разбанить Войти Выйти ({username}) Новости Никнейм Еще ни одной новости! Еще ни одного видео! Пароль Разрешения Плейлисты Пожалуйста, заполните поля ниже, что бы войти в систему: Зарегистрирован Запомнить меня Роль Роли Сохранить Выберите разрешения Аккаунт этого пользователя не активен Турниры Логин Пользователи Видео 