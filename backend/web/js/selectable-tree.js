$(function(){
    $('.selectableTree').each(function(){
        $(this).find('.subTree li').addClass('collapsed');
    });

    $(document).on('click', '.permissionSelector', function(){
        var $checkbox = $(this).next('input');
        if($checkbox.is(':checked')){
            $checkbox.prop("checked", false);
            $(this).removeClass('label-success').addClass('label-primary');
        } else {
            $checkbox.prop("checked", true);
            $(this).removeClass('label-primary').addClass('label-success');
        }
    });
});