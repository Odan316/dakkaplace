$(function(){
    $('.collapsibleTree').each(function(){
        $(this).find('.subTree li').addClass('collapsed');
    });

    $(document).on('click', '.subTree li span', function(){
        if($(this).parent().hasClass('collapsed')){
            $(this).parent().removeClass('collapsed');
        } else {
            $(this).parent().addClass('collapsed');
        }
    });
});