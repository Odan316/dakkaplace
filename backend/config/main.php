<?php

use yii\web\UrlNormalizer;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$frontendRoutes = require(__DIR__ . '/../../frontend/config/_routes.php');
$hostInfoFront = require(__DIR__ . '/../../frontend/config/_hostInfo.php');
return [
    'id' => 'app-backend',
    'name' => 'DakkaPlace.Админка',
    'language' => 'ru_RU',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass'   => 'common\models\user\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-backend',
                'httpOnly' => true
            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'app-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n'         => [
            'translations' => [
                'app/backend*'   => [
                    'class'          => 'yii\i18n\GettextMessageSource',
                    'basePath'       => '@backend/messages',
                    'catalog'        => 'backend',
                    'sourceLanguage' => 'en'
                ],
            ],
        ],
        'urlManager'   => [
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'enableStrictParsing' => true,
            'normalizer'          => [
                'class'  => 'yii\web\UrlNormalizer',
                'action' => UrlNormalizer::ACTION_REDIRECT_PERMANENT,
            ],
            'rules'               => [
                ''                                        => 'site/index',
                '<action:(login|logout)>'                 => 'site/<action>',
                '<controller>'                            => '<controller>/index',
                '<controller>/<action>/<id:\d+>'          => '<controller>/<action>',
                '<controller>/<action>'                   => '<controller>/<action>',
            ],
        ],
        'urlManagerFront' => [
            'class'               => 'yii\web\urlManager',
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'enableStrictParsing' => true,
            'hostInfo'             => $hostInfoFront['scheme'] . '://' . $hostInfoFront['hostName'],
            'rules'               => $frontendRoutes,
            'normalizer'          => [
                'class'  => 'yii\web\UrlNormalizer',
                'action' => UrlNormalizer::ACTION_REDIRECT_PERMANENT,
            ],
        ],
    ],
    'controllerMap'       => [
        'elfinder' => [
            'class'            => 'mihaildev\elfinder\Controller',
            'access'           => ['@'],
            'disabledCommands' => ['netmount'],
            /*'plugin'           => [
                'class' => '\backend\widgets\images_picker\PathUpdater',
            ],*/
            'roots'            => [
                [
                    'baseUrl'  => $hostInfoFront['scheme'] . '://' . $hostInfoFront['hostName'],
                    'basePath' => '@backend/web',
                    'path'     => 'uploads/originals',
                    'name'     => 'Uploads',
                    'options'  => [
                        'uploadOrder' => ['allow', 'deny'],
                        'uploadAllow' => ['image/jpeg', 'image/png', 'application/pdf', 'image/vnd.djvu']
                    ],
                ],
            ],
        ],
    ],
    'modules' => [
    ],
    'params' => $params,
];
