<?php

namespace backend\controllers;

use backend\models\forms\UserForm;
use backend\models\search\UserSearch;
use common\components\controllers\WebController;
use common\models\user\Status;
use Yii;
use common\models\user\User;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends WebController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['backend/users/accounts/view']
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['create'],
                        'roles'   => ['backend/users/accounts/create']
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['update', 'confirm', 'ban', 'lift-ban'],
                        'roles'   => ['backend/users/accounts/edit']
                    ]
                ]
            ]
        ];
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Lists all User models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single User model.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new UserForm(['scenario' => UserForm::SCENARIO_CREATE]);

        if ($model->load(Yii::$app->request->post()) && $model->create()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $model = new UserForm(['scenario' => UserForm::SCENARIO_UPDATE]);
        $user = $this->findModel($id);
        $model->setUser($user);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    public function actionConfirm($id)
    {
        $model = User::find()->hasId($id)->one();

        if (!empty($model)) {
            $model->status = Status::active();
            $model->save();
        }

        $this->redirect(['view', 'id' => $id]);
    }

    public function actionBan($id)
    {
        $model = User::find()->hasId($id)->notBanned()->one();

        if (!empty($model)) {
            $model->status = Status::inactive();
            $model->save();
        }

        $this->redirect(['view', 'id' => $id]);
    }

    public function actionLiftBan($id)
    {
        $model = User::find()->hasId($id)->banned()->one();

        if (!empty($model)) {
            $model->status = Status::active();
            $model->save();
        }

        $this->redirect(['view', 'id' => $id]);
    }
}
