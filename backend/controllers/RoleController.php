<?php

namespace backend\controllers;

use backend\models\forms\RoleForm;
use common\components\controllers\WebController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\rbac\DbManager;
use yii\rbac\Item;

/**
 * Class RolesController
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 */
class RoleController extends WebController
{
    const ADMIN_ROLE = 'admin';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index'],
                        'roles'   => ['backend/users/roles/view'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['edit'],
                        'roles'   => ['backend/users/roles/edit'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Roles with theirs permissions.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var DbManager $authManager */
        $authManager = Yii::$app->authManager;

        $rolesQuery = (new Query)
            ->select(['id' => 'r.name', 'roleName' => 'r.name'])
            ->from(['r' => $authManager->itemTable])
            ->andWhere(['r.type' => Item::TYPE_ROLE]);
        $rolesData = new ActiveDataProvider([
            'query' => $rolesQuery
        ]);

        return $this->render('index', [
            'dataProvider' => $rolesData,
        ]);
    }


    /**
     * @param $name
     * @return mixed
     */
    public function actionEdit($name)
    {
        $model = new RoleForm(['scenario' => RoleForm::SCENARIO_UPDATE]);

        /** @var DbManager $authManager */
        $authManager = Yii::$app->authManager;

        $role = $authManager->getRole($name);
        $model->setRole($role);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('edit', [
                'model' => $model,
                'role'  => $role
            ]);
        }
    }
}