<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class CollapsibleTreeAsset
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class CollapsibleTreeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $jsOptions = ['position' => \yii\web\View::POS_END];

    public $css = [
        'css/collapsible-tree.css'
    ];
    public $js = [
        'js/collapsible-tree.js'
    ];

    public $depends = [
        'backend\assets\AppAsset'
    ];
}