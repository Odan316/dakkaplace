<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class RolesAsset
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class SelectableTreeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $jsOptions = ['position' => \yii\web\View::POS_END];

    public $css = [
    ];
    public $js = [
        'js/selectable-tree.js'
    ];

    public $depends = [
        'backend\assets\AppAsset'
    ];
}