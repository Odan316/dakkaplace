<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\components\AdminGridView;
use common\modules\community\models\News;
use common\modules\video\models\Video;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;

/**
 * @var $this yii\web\View
 * @var $videosDataProvider yii\data\ActiveDataProvider
 * @var $tournamentsDataProvider yii\data\ActiveDataProvider
 */

$this->title = Yii::t('app/backend', 'DakkaPlace.Admin');

$videosDataProvider = new ActiveDataProvider(
    [
        'query' => Video::find(),
        'pagination' => [
            'pageSize' => 10
        ]
    ]
);

$newsDataProvider = new ActiveDataProvider(
    [
        'query' => News::find(),
        'pagination' => [
            'pageSize' => 10
        ]
    ]
);

?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?php echo Yii::t('app/backend', 'DakkaPlace.Admin') ?></h1>
    </div>
    <?php if (Yii::$app->user->can('backend/overview/view')) { ?>
        <div class="body-content">

            <div class="row">
                <?php if (Yii::$app->hasModule('videos')) { ?>
                    <div class="col-lg-6">
                        <h2><?php echo Yii::t('app/backend', 'Videos') ?></h2>

                        <?php echo AdminGridView::widget(
                            [
                                'dataProvider' => $videosDataProvider,
                                'layout' => "{items}\n{pager}",
                                'emptyText' => Yii::t('app/backend', 'No videos yet!'),
                                'columns' => [
                                    [
                                        'attribute' => 'title',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            /** @var $model Video */
                                            return Html::a($model->title, ['video/video/view', 'id' => $model->id]);
                                        }
                                    ],
                                ],
                            ]
                        ); ?>

                        <p><?php echo Html::a(
                                Yii::t('app/backend', 'All videos &raquo;'),
                                ['video/video/index'],
                                ['class' => "btn btn-primary"]
                            ) ?></p>
                    </div>
                <?php } ?>

                <?php if (Yii::$app->hasModule('community')) { ?>
                    <div class="col-lg-6">
                        <h2><?php echo Yii::t('app/backend', 'News') ?></h2>

                        <?php echo AdminGridView::widget(
                            [
                                'dataProvider' => $newsDataProvider,
                                'layout' => "{items}\n{pager}",
                                'emptyText' => Yii::t('app/backend', 'No news yet!'),
                                'columns' => [
                                    [
                                        'attribute' => 'title',
                                        'format' => 'raw',
                                        'value' => function ($model) {
                                            /** @var $model News */
                                            return Html::a($model->title, ['community/news/view', 'id' => $model->id]);
                                        }
                                    ],
                                ],
                            ]
                        ); ?>

                        <p><?php echo Html::a(
                                Yii::t('app/backend', 'All news &raquo;'),
                                ['community/news/index'],
                                ['class' => "btn btn-primary"]
                            ) ?></p>
                    </div>
                <?php } ?>
            </div>

        </div>
    <?php } ?>
</div>
