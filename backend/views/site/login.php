<?php

/**
 * @var $this yii\web\View
 * @var $form yii\bootstrap4\ActiveForm
 * @var $model LoginForm
 */

use backend\models\forms\LoginForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = Yii::t('app/backend', 'Log in');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <p><?php echo Yii::t('app/backend','Please fill out the following fields to log in:') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?php echo $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?php echo $form->field($model, 'password')->passwordInput() ?>

                <?php echo $form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group">
                    <?php echo Html::submitButton(Yii::t('app/backend', 'Log in'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
