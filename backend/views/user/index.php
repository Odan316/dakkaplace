<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\components\AdminGridView;
use backend\models\search\UserSearch;
use common\models\user\User;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;
use yii\rbac\Role;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $searchModel UserSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

$this->title = Yii::t('app/backend', "Users");
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?php if (Yii::$app->user->can('backend/users/accounts/create')) { ?>
            <?php echo Html::a(Yii::t('app/backend', "Create"), ['create'], ['class' => 'btn btn-success']) ?>
        <?php } ?>
    </div>

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php Pjax::begin(); ?>
    <?php
    $columns = [
        'id',
        'username' => [
            'attribute' => 'username',
            'format'    => 'html',
            'value'     => function ($model) {
                return Html::a($model->username, ['view', 'id' => $model->id]);
            },
        ],
        'nickname',
        'email',
        [
            'attribute' => 'status',
            'value'     => function ($model) {
                /** @var User $model */
                return $model->getStatusText();
            },
            'filter'    => User::getStatuses(),

        ],
        'role'     => [
            'attribute' => 'role',
            'label'     => Yii::t('app/backend', "Role"),
            'format'    => 'text',
            'value'     => function ($model, $key, $index) {
                /** @var $model User */
                return $model->getRole() !== null ? $model->getRole()->name : '-';

            },
            'filter'    => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', function ($role) {
                /** @var Role $role */
                return $role->name;
            }),

        ],
        [
            'attribute' => 'createdAt',
            'format'    => ['date', 'php:d.m.Y']
        ]
    ];
    $buttons = [
        'view' => function ($url, $model, $key) {
            /** @var $model User */
            return Html::a('<i class="far fa-eye"></i>',
                ['view', 'id' => $model->id]);
        }
    ];
    if (Yii::$app->user->can('backend/users/accounts/edit')) {
        $buttons = ArrayHelper::merge($buttons, [
            'edit' => function ($url, $model, $key) {
                /** @var $model User */
                return Html::a('<i class="fas fa-pencil-alt"></i>', ['update', 'id' => $model->id]);
            }
        ]);
    }

    $columns[] = [
        'class'    => 'yii\grid\ActionColumn',
        'template' => '{view} {edit}',
        'buttons'  => $buttons
    ];

    ?>
    <?php echo AdminGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => $columns
    ]); ?>
    <?php Pjax::end(); ?></div>
