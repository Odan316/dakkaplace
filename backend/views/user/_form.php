<?php

use backend\models\forms\UserForm;
use backend\components\AdminActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;
use yii\rbac\Role;

/**
 * @var $this yii\web\View
 * @var $model UserForm
 */

$availableRoles = Yii::$app->authManager->getRoles();

$availableRoles = ArrayHelper::map($availableRoles, 'name',
    function ($role) {
        /** @var Role $role */
        return $role->name;
    });
?>

<div class="user-form">
    <?php $form = AdminActiveForm::begin(); ?>

    <?php echo $form->field($model, 'username')->textInput(['disabled' => !$model->isNewRecord]) ?>
    <?php echo $form->field($model, 'nickname')->textInput() ?>
    <?php echo $form->field($model, 'email')->textInput() ?>
    <?php echo $form->field($model, 'password')->textInput()
        ->label(Yii::t('app/backend', "Password")) ?>
    <?php echo $form->field($model, 'role')->dropdownList($availableRoles, [
        'prompt'   => Yii::t('app/backend', "Choose role"),
        'disabled' => ($model->getId() == 1 ? 'disabled' : false)
    ]); ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('app/backend', "Save"), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php AdminActiveForm::end(); ?>

</div>
