<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\components\AdminDetailView;
use common\models\user\User;
use yii\bootstrap4\Html;
use yii\widgets\DetailView;

/**
 *
 * @var $this yii\web\View
 * @var $model User
 */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/backend', "Users"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-view">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?php if (Yii::$app->user->can('backend/users/accounts/edit')) { ?>
            <?php echo Html::a(Yii::t('app/backend', "Edit"), ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary']) ?>
            <?php if($model->isJustRegistered()) { ?>
                <?php echo Html::a(Yii::t('app/backend', "Confirm"), ['confirm', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data'  => [
                        'confirm' => Yii::t('app/backend', "Are you sure you want manually confirm this account?"),
                        'method'  => 'post'
                    ]
                ]) ?>
            <?php } ?>
            <?php if ($model->isNotBanned() && !$model->isAdmin()) { ?>
                <?php echo Html::a(Yii::t('app/backend', "Ban"), ['ban', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data'  => [
                        'confirm' => Yii::t('app/backend', "Are you sure you want ban this account?"),
                        'method'  => 'post'
                    ]
                ]) ?>
            <?php } elseif($model->isBanned()) { ?>
                <?php echo Html::a(Yii::t('app/backend', "Lift ban"), ['lift-ban', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data'  => [
                        'confirm' => Yii::t('app/backend', "Are you sure you want lift ban for this account?"),
                        'method'  => 'post'
                    ]
                ]) ?>
            <?php } ?>
        <?php } ?>
    </div>

    <?php

    $attributes = [
        'id'   => [
            'attribute'      => 'id',
            'captionOptions' => ['class' => 'col-md-2'],
            'contentOptions' => ['class' => 'col-md-10']
        ],
        'username',
        'nickname',
        'email',
        [
            'attribute' => 'status',
            'value'     => function ($model) {
                /** @var User $model */
                return $model->getStatusText();
            }
        ],
        'role' => [
            'label'  => Yii::t('app/backend', "Role"),
            'format' => 'text',
            'value'  => $model->getRole() !== null ? $model->getRole()->name : '-'
        ],
        [
            'label'     => Yii::t('app/backend', 'Registered at'),
            'attribute' => 'createdAt',
            'format'    => ['date', 'php:d.m.Y']
        ]
    ];

    ?>

    <?php echo AdminDetailView::widget([
        'model'      => $model,
        'attributes' => $attributes
    ]) ?>

</div>
