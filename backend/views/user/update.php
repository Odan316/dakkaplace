<?php

use common\models\user\User;
use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $model User
 */

$this->title = Yii::t('app/backend', 'Edit account: {username}', [
    'username' => $model->username
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/backend', "Users"), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app/common', "Edit");
?>
<div class="user-update">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php echo $this->render('_form', ['model' => $model]) ?>

</div>
