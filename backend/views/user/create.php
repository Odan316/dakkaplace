<?php

use yii\bootstrap4\Html;
use common\models\videos\Video;

/**
 * @var $this yii\web\View
 * @var $model Video
 */

$this->title = Yii::t('app/backend', 'Create new user');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/backend', "Users"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?php echo Html::encode($this->title)?></h1>

    <?php echo $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
