<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\assets\CollapsibleTreeAsset;
use backend\components\AdminGridView;
use backend\components\RolesHelper;
use yii\bootstrap4\Html;
use yii\rbac\DbManager;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * */

$this->title = Yii::t('app/backend', 'Roles');
$this->params['breadcrumbs'][] = $this->title;
CollapsibleTreeAsset::register($this);
?>
    <div class="role-index">

        <h1><?php echo Html::encode($this->title) ?></h1>

        <?php
        $columns = [
            [
                'attribute' => 'roleName',
                'label'     => Yii::t('app/backend', 'Role'),
                'options'   => ['class' => 'col-sm-2 col-md-1']
            ],
            [
                'attribute' => 'permNames',
                'label'     => Yii::t('app/backend', 'Permissions'),
                'format'    => 'raw',
                'value'     => function ($model) {
                    /** @var DbManager $authManager */
                    $authManager = Yii::$app->authManager;
                    $rolesTree = RolesHelper::convertPermsToTree($authManager->getPermissionsByRole($model['roleName']));

                    $html = Html::beginTag('div', ['class' => 'collapsibleTree']);
                    $html .= printPermTree($rolesTree);
                    $html .= Html::endTag('div');

                    return $html;
                }
            ]
        ];
        if (Yii::$app->user->can('backend/users/roles/edit')) {
            $columns[] = [
                'class'          => 'yii\grid\ActionColumn',
                'contentOptions' => ['class' => 'cell-nowrap'],
                'options'        => ['class' => 'col-sm-1'],
                'template'       => '{edit}',
                'buttons'        => [
                    'edit'   => function ($url, $model) {
                        return Html::a(
                            '<i class="fas fa-pencil-alt"></i>',
                            ['role/edit', 'name' => $model['roleName']]
                        );
                    }
                ]
            ];
        }
        ?>

        <?php Pjax::begin(); ?>
        <?php echo AdminGridView::widget([
            'dataProvider' => $dataProvider,
            'columns'      => $columns
        ]); ?>
        <?php Pjax::end(); ?>

    </div>


<?php

function printPermTree($nodeArr)
{
    $html = Html::beginTag('ul', ['class' => 'subTree']);
    foreach ($nodeArr as $pathPart => $subTree) {
        $html .= Html::beginTag('li');
        if (is_array($subTree)) {
            $html .= Html::tag('span', $pathPart);
            $html .= printPermTree($subTree);
        } else {
            $html .= Html::tag('span', $pathPart, ['class' => 'label label-primary']);
        }
        $html .= Html::endTag('li');
    }

    $html .= Html::endTag('ul');

    return $html;
}

?>