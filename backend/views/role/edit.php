<?php

use backend\models\forms\RoleForm;
use yii\bootstrap4\Html;
use yii\rbac\Role;

/**
 * @var $this yii\web\View
 * @var $model RoleForm
 * @var $role Role
 * */

$this->title = Yii::t('app/backend', 'Edit role: {roleName}', [ 'roleName' => $role->name ]);

$this->params['breadcrumbs'][] = [ 'label' => Yii::t('app/backend', 'Roles'), 'url' => [ 'index' ] ];
$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="role-index">
        <h1><?php echo Html::encode($this->title) ?></h1>
    </div>

<?php echo $this->render('_form', [ 'model' => $model ]) ?>