<?php

use backend\assets\CollapsibleTreeAsset;
use backend\assets\SelectableTreeAsset;
use backend\components\RolesHelper;
use backend\models\forms\RoleForm;
use backend\components\AdminActiveForm;
use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $model RoleForm
 * */
SelectableTreeAsset::register($this);
CollapsibleTreeAsset::register($this);
?>

<?php $form = AdminActiveForm::begin(); ?>

<div class="form-group">
    <?php echo Html::submitButton(Yii::t('app/common', 'Save'), [ 'class' => 'btn btn-primary' ]) ?>
</div>

<div class="row">
        <?php if($model->getScenario() == RoleForm::SCENARIO_CREATE){ ?>
            <div class="col-sm-12">
                <?php echo $form->field($model, 'roleName')
                    ->textInput()
                    ->label(false); ?>
            </div>
        <?php } ?>
        <div class="col-sm-12">
            <h3><?php echo Yii::t('app/backend', 'Select permissions')?></h3>
            <div class="well well-sm">

                <?php
                $rolesTree = RolesHelper::convertPermsToTree($model->getAvailablePermissions());

                $html = Html::beginTag('div', ['class' => 'collapsibleTree selectableTree']);
                $html.= printPermTree($rolesTree, $model);
                $html .= Html::endTag('div');

                echo $html;
                ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('app/common', 'Save'), [ 'class' => 'btn btn-primary' ]) ?>
    </div>

<?php AdminActiveForm::end(); ?>


<?php
/**
 * @param $nodeArr
 * @param RoleForm $model
 * @return string
 */
function printPermTree($nodeArr, $model)
{
    $html = Html::beginTag('ul', ['class' => 'subTree']);
    foreach($nodeArr as $pathPart => $subTree){
        $html .= Html::beginTag('li');
        if(is_array($subTree)){
            $html .= Html::tag('span', $pathPart);
            $html .= printPermTree($subTree,  $model);
        } else {
            $html .= Html::tag('span', $pathPart,
                ['class' => 'permissionSelector label '.($model->hasPermission($subTree) ? 'label-success' : 'label-primary')]);
            $html .= Html::checkbox(
                    Html::getInputName($model, 'rolePermissions').'[]',
                $model->hasPermission($subTree),
                ['value' => $subTree, 'class' => 'hide']);
        }
        $html .= Html::endTag('li');
    }

    $html .= Html::endTag('ul');

    return $html;
}
?>
