<?php
/** @noinspection PhpUnhandledExceptionInspection */

/**
 * @var $this View
 * @var $content string
 */

use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\web\View;

NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar navbar-expand-md fixed-top navbar-dark bg-dark',
    ],
]);
$menuItems = [];
if (!Yii::$app->user->isGuest) {
    // tournaments section
    if (Yii::$app->hasModule('tournaments')) {
        $subItems = [];
        
        $subItems[] = ['label' => Yii::t('app/backend', 'Tournaments'), 'url' => ['/tournaments/tournaments']];
        $subItems[] = ['label' => Yii::t('app/backend', 'Game Systems'), 'url' => ['/tournaments/game-system/index']];
        $subItems[] = ['label' => Yii::t('app/backend', 'Factions'), 'url' => ['/tournaments/faction/index']];
        
        if (!empty($subItems)) {
            $menuItems[] = [
                'label' => Yii::t('app/backend', 'Tournaments'),
                'items' => $subItems
            ];
        }
    }
    
    // разделы по работе с видео
    if (Yii::$app->hasModule('videos')) {
        $subItems = [];
        if (Yii::$app->user->can('backend/videos/view')) {
            $subItems[] = ['label' => Yii::t('app/backend', 'Videos'), 'url' => ['/video/video/index']];
            $subItems[] = ['label' => Yii::t('app/backend', 'Playlists'), 'url' => ['/video/playlist/index']];
        }
        if (!empty($subItems)) {
            $menuItems[] = [
                'label' => Yii::t('app/backend', 'Videos'),
                'items' => $subItems
            ];
        }
    }
    // разделы по работе с комьюнити
    if (Yii::$app->hasModule('community')) {
        if (Yii::$app->user->can('backend/news/view')) {
            $menuItems[] = ['label' => Yii::t('app/backend', 'News'), 'url' => ['/community/news/index']];
        }
    }
    
    // разделы по работе с аккаунтами и ролями
    $subItems = [];
    if (Yii::$app->user->can('backend/users/accounts/view')) {
        $subItems[] = ['label' => Yii::t('app/backend', 'Accounts'), 'url' => ['/user/index']];
    }
    if (Yii::$app->user->can('backend/users/roles/view')) {
        $subItems[] = ['label' => Yii::t('app/backend', 'Roles'), 'url' => ['/role/index']];
    }
    if (!empty($subItems)) {
        $menuItems[] = [
            'label' => Yii::t('app/backend', 'Users'),
            'items' => $subItems
        ];
    }
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => $menuItems,
]);

$menuItems = [];
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => Yii::t('app/backend', 'Log in'), 'url' => ['/site/login']];
} else {
    $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            Yii::t('app/backend', 'Log out ({username})', [
                'username' => Yii::$app->user->identity->username
            ]),
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav ml-md-auto'],
    'items' => $menuItems,
]);
NavBar::end();