<?php

namespace frontend\assets;

use common\components\CustomAssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends CustomAssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/styles.css',
    ];
    public $js = [
        'js/scripts.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'common\assets\FontAwesomeAsset',
    ];
}
