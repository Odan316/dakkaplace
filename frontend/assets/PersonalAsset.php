<?php
/**
 */

namespace frontend\assets;


use yii\web\AssetBundle;

/**
 * @author Andreev Sergey <si.andreev316@gmail.com>
 */
class PersonalAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $jsOptions = ['position' => \yii\web\View::POS_END];

    public $css = [
       // 'css/personal.css'
    ];
    public $js = [
        'js/personal.js',
    ];

    public $depends = [
        'frontend\assets\AppAsset'
    ];
}