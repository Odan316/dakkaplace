<?php
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

/**
 * @var $this View
 * @var $content string
 */

use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\web\View;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/favicon.png?v=0.1"/>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php echo $this->render('_menu') ?>

    <div class="container">
        <?php echo Breadcrumbs::widget([
            'homeLink' => [
                'label' => '<i class="fas fa-home"></i>',
                'url' => '/',
                'encode' => false,
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?php echo \common\widgets\Alert::widget();?>
        <?php echo $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?php echo Html::encode(Yii::$app->name) ?> <?php echo date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
