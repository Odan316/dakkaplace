<?php
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\web\View;

/**
 * @var $this View
 * @var $content string
 */

NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl'   => Yii::$app->homeUrl,
    'options'    => [
        'class' => 'navbar navbar-expand-md fixed-top navbar-dark bg-dark',
    ],
]);

$menuItems = [];

if (Yii::$app->hasModule('tournaments')) {
    $menuItems[] = ['label' => Yii::t('app/frontend', 'Tournaments'), 'url' => ['/tournaments']];
}

// разделы по работе с видео
if (Yii::$app->hasModule('videos')) {
    $subItems = [];
    if (Yii::$app->user->can('backend/videos/view')) {
        $subItems[] = ['label' => Yii::t('app/frontend', 'Videos'), 'url' => ['/video/video/index']];
        $subItems[] = ['label' => Yii::t('app/frontend', 'Playlists'), 'url' => ['/video/playlist/index']];
    }
    if (!empty($subItems)) {
        $menuItems[] = [
            'label' => Yii::t('app/frontend', 'Videos'),
            'items' => $subItems
        ];
    }
}
// разделы по работе с комьюнити
if (Yii::$app->hasModule('community')) {
    $menuItems[] = ['label' => Yii::t('app/frontend', 'News'), 'url' => ['/community/news/index']];
}
if (Yii::$app->hasModule('outcomes')) {
    $menuItems[] = ['label' => Yii::t('app/frontend', 'Outcomes'), 'url' => ['/outcomes/calculator/index']];
}
// разделы по работе с аккаунтами
//$menuItems[] = ['label' => Yii::t('app/frontend', 'Users'), 'url' => ['/user/index']];

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items'   => $menuItems,
]);

$menuItems = [];
// ссылки на аккаунт
if (true) {
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => Yii::t('app/frontend', 'Registration'), 'url' => ['/site/register']];
        $menuItems[] = ['label' => Yii::t('app/frontend', 'Log in'), 'url' => ['/site/login']];
    } else {
        $menuItems[] = ['label' => Yii::t('app/frontend', 'Cabinet'), 'url' => ['/personal/index']];
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                Yii::t(
                    'app/frontend',
                    'Log out ({username})',
                    [
                        'username' => Yii::$app->user->identity->username
                    ]
                ),
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav ml-md-auto'],
    'items'   => $menuItems,
]);
NavBar::end();

