<?php

declare(strict_types=1);

use frontend\models\forms\PasswordResetRequestForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\web\View;

/**
 * @var $this View
 * @var $model PasswordResetRequestForm
 * @var $result string
 * @var $errorText string
 */

$this->title = Yii::t('app/frontend', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php if ($result === 'success') { ?>
        <p><?php echo Yii::t('app/frontend', 'Check out your email for further instructions.', ['email' => $model->email]) ?></p>
    <?php } elseif ($result === 'error') { ?>
        <p><?php echo $errorText ?></p>
    <?php } else { ?>

        <p><?php echo Yii::t('app/frontend', 'Please fill out your email. A link to reset password will be sent there.') ?></p>

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?php echo $form->field($model, 'email')->textInput(['autofocus' => true])->label(false) ?>

                <div class="form-group">
                    <?php echo Html::submitButton(Yii::t('app/common', 'Send'), ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php } ?>
</div>
