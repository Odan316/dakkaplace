<?php

declare(strict_types=1);

use frontend\models\forms\ResetPasswordForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model ResetPasswordForm
 * @var $result string
 */

$this->title = Yii::t('app/frontend', 'Reset password');
?>
<div class="site-reset-password text-center">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php if ($result === 'success') { ?>
        <p><?php echo Yii::t('app/frontend', 'New password saved.') ?></p>

        <div class="form-group">
            <?php echo Html::a(Yii::t('app/frontend', 'Log in'), ['site/login'], ['class' => 'btn btn-primary']) ?>
        </div>
    <?php } else { ?>
        <p><?php echo Yii::t('app/frontend', 'Please choose your new password:') ?></p>

        <div class="row">
            <div class="col-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
                <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?php echo $form->field($model, 'password')->passwordInput(['autofocus' => true])->label(false) ?>

                <div class="form-group">
                    <?php echo Html::submitButton(Yii::t('app/frontend', 'Save'), ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <?php } ?>
</div>
