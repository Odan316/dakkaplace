<?php
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use App\Tournaments\Presentation\Yii\Widgets\MainPageWidget\MainPageWidget;
use common\modules\community\models\News;
use common\modules\video\models\Video;
use frontend\components\helpers\ImperialDateHelper;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\web\View;

/**
 * @var $this View
 */

$this->title = Yii::t('app/frontend', '{{ Mainpage title}}');
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?php echo Yii::t('app/frontend', 'Welcome!'); ?></h1>

        <p class="lead"><?php echo Yii::t(
                'app/frontend',
                'Now is {imperial_date}',
                [
                    'imperial_date' => ImperialDateHelper::getImperialDate()
                ]
            ) ?></p>

    </div>

    <div class="body-content">

        <?php /*<div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4">
                <?php echo Html::a(Yii::t('app/frontend', 'Paints'), ['paints/paint/index'],
                    ['class' => 'btn btn-block btn-danger btn-lg']) ?>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <?php echo Html::a(Yii::t('app/frontend', 'Outcomes'), ['outcomes/calculator/index'],
                    ['class' => 'btn btn-block btn-warning btn-lg']) ?>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4">
                <?php echo Html::a(Yii::t('app/frontend', 'Useful info'), ['database/categories/index'],
                    ['class' => 'btn btn-block btn-success btn-lg']) ?>
            </div>
        </div> */ ?>
        <div class="row">
            <div class="col-sm-12">

                <div class="row">
                    <?php if (Yii::$app->hasModule('tournaments')) { ?>
                        <div class="col-sm-6">
                            <?php echo MainPageWidget::getRendered(); ?>
                        </div>
                    <?php } ?>
                    
                    <?php if (Yii::$app->hasModule('videos')) { ?>
                        <div class="col-sm-6">
                            <h2><?php echo Yii::t('app/frontend', 'Videos') ?></h2>

                            <div class="row mainpageList">
                                <?php
                                $query = News::find()->published();

                                $dataPovider = new ActiveDataProvider(
                                    [
                                        'query' => Video::find()->onlyPublic()->orderByPublish(),
                                        'pagination' => [
                                            'pageSize' => 10
                                        ]
                                    ]
                                );
                                /** @var Video $model */
                                foreach ($dataPovider->getModels() as $model) { ?>
                                    <div class="col-md-12 mb-3">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <?php echo Html::a(
                                                    Html::img($model->getVideoThumbs()[0], ['class' => 'listPreview']),
                                                    ['/video/video/view', 'slug' => $model->slug],
                                                    ['class' => 'preview', 'target' => '_blank']
                                                ); ?>
                                            </div>
                                            <div class="col-md-8">
                                                <h4><?php echo Html::a(
                                                        $model->title,
                                                        ['/video/video/view', 'slug' => $model->slug]
                                                    ) ?></h4>
                                                <span class="listDate"><?php echo date(
                                                        'd.m.Y H:i',
                                                        $model->publishedAt
                                                    ) ?></span>
                                                <div><?php echo Html::a(
                                                        $model->shortDesc,
                                                        ['/video/video/view', 'slug' => $model->slug],
                                                        ['class' => 'listSnippet']
                                                    ) ?> </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <p><?php echo Html::a(
                                    Yii::t('app/frontend', 'All videos &raquo;'),
                                    ['video/video/index'],
                                    ['class' => "btn btn-block btn-primary btn-lg"]
                                ) ?>
                                <?php echo Html::a(
                                    Yii::t('app/frontend', 'All playlists &raquo;'),
                                    ['video/playlist/index'],
                                    ['class' => "btn btn-block btn-primary btn-lg"]
                                ) ?></p>

                        </div>
                    <?php } ?>
                    <?php if (Yii::$app->hasModule('community')) { ?>
                        <div class="col-sm-6">
                            <h2><?php echo Yii::t('app/frontend', 'News') ?></h2>

                            <div class="row mainpageList">
                                <?php
                                $query = News::find()->published();

                                $dataPovider = new ActiveDataProvider(
                                    [
                                        'query' => News::find()->published()->orderByPublish(),
                                        'pagination' => [
                                            'pageSize' => 10
                                        ]
                                    ]
                                );
                                /** @var News $model */
                                foreach ($dataPovider->getModels() as $model) { ?>
                                    <div class="col-md-12 mb-3">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <?php echo Html::a(
                                                    Html::img(Yii::$app->images->getSrc($model->preview, 'thumb')),
                                                    ['/community/news/view', 'slug' => $model->slug],
                                                    ['class' => 'preview', 'target' => '_blank']
                                                ); ?>
                                            </div>
                                            <div class="col-md-8">
                                                <h3><?php echo Html::a(
                                                        $model->title,
                                                        ['/community/news/view', 'slug' => $model->slug]
                                                    ) ?></h3>
                                                <span class="listDate"><?php echo date(
                                                        'd.m.Y H:i',
                                                        $model->publishedAt
                                                    ) ?></span>
                                                <p><?php echo Html::a(
                                                        $model->shortDesc,
                                                        ['/community/news/view', 'slug' => $model->slug],
                                                        ['class' => 'listSnippet']
                                                    ) ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <p><?php echo Html::a(
                                    Yii::t('app/frontend', 'All news &raquo;'),
                                    ['community/news/index'],
                                    ['class' => "btn btn-block btn-primary btn-lg"]
                                ) ?></p>

                        </div>
                    <?php } ?>
                </div>

            </div>
        </div>

    </div>
</div>
