<?php

declare(strict_types=1);

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use frontend\models\forms\RegisterForm;
use yii\web\View;

/**
 * @var $this View
 * @var $model RegisterForm
 */

$this->title = Yii::t('app/frontend', 'Register');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-register">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <p><?php echo Yii::t('app/frontend', 'Please fill out the following fields to register:') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?php echo $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?php echo $form->field($model, 'nickname')->textInput() ?>

            <?php echo $form->field($model, 'email') ?>

            <?php echo $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
                <?php echo Html::submitButton(Yii::t('app/frontend', 'Register'),
                    ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
