<?php

declare(strict_types=1);

use frontend\models\forms\LoginForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $model LoginForm
 */

$this->title = Yii::t('app/frontend', 'Log in');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <p><?php echo Yii::t('app/frontend','Please fill out the following fields to log in:') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?php echo $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?php echo $form->field($model, 'password')->passwordInput() ?>

                <?php echo $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    <?php echo Yii::t('app/frontend', 'If you forgot your password you can <a href="{link}">reset it</a>', [
                        'link' => Url::to(['site/request-password-reset'])
                    ]) ?>
                </div>

                <div class="form-group">
                    <?php echo Html::submitButton(Yii::t('app/frontend', 'Log in'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
