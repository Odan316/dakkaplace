<?php

declare(strict_types=1);

use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $result string
 * @var $cause string
 */

$this->title = Yii::t('app/frontend', 'Confirm registration');
?>
<div class="site-confirm-registration text-center">
    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php if ($result === 'success') { ?>
        <p><?php echo Yii::t('app/frontend', 'You have successfully confirmed your registration') ?></p>
    <?php } elseif ($cause == 'alreadyConfirmed') { ?>
        <p><?php echo Yii::t('app/frontend', 'You have already confirmed your registration') ?></p>
    <?php } else { ?>
        <p><?php echo Yii::t('app/frontend', 'Sorry, we are unable to confirm your registration by this token') ?></p>
    <?php } ?>

    <div class="form-group">
        <?php echo Html::a(Yii::t('app/frontend', 'To personal section'), ['personal/index'],
            ['class' => 'btn btn-success']) ?>
    </div>

</div>
