<?php

use common\models\user\User;
use yii\grid\GridView;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;

/**
 * @var $this yii\web\View
 */

$this->title = Yii::t('app/frontend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?php echo $this->title ?></h1>

    <?php echo GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'      => User::find()->active(),
            'pagination' => [
                'pageSize' => 50
            ]
        ]),
        'layout'       => "{items}\n{pager}",
        'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
        'columns'      => [
            [
                'attribute' => 'nickname',
                'format'    => 'raw',
                'value'     => function ($model) {
                    /** @var $model User */
                    return Html::a($model->nickname, ['view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'createdAt',
                'format'    => ['date', 'php:d.m.Y']
            ]
        ]
    ]); ?>

</div>
