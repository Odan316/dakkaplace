<?php

use App\Tournaments\Infrastructure\Yii\ActiveRecords\Tournament;
use common\models\user\User;
use frontend\components\AdvHtmlHelper;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/**
 * @var $this yii\web\View
 * @var $model User
 */

$this->title = $model->nickname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/frontend', "Users"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center"><?php echo $this->title ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-5">
            <div class="row">
                <div class="col-12 personalInfo">
                    <h2 class="text-center"><?php echo Yii::t('app/frontend', 'Personal info') ?></h2>
                    <form class="form-horizontal">
                        <?php echo Html::img($model->getAvatarSrc(), ['class' => 'avatar']) ?>
                        <?php echo AdvHtmlHelper::activeStaticControl($model, 'nickname') ?>
                        <?php echo AdvHtmlHelper::staticControl(
                            Yii::t('app/frontend', 'Registered at'),
                            date('d-m-Y', $model->createdAt)
                        ) ?>
                    </form>
                    <div class="form-group text-center">
                        <?php if ($model->id == Yii::$app->user->id) { ?>
                            <?php echo Html::button(Yii::t('app/frontend', 'Edit'),
                                ['class' => 'btn btn-primary btnEditUser']) ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-12">
                    <h3 class="text-center"><?php echo Yii::t('app/frontend', 'Account type') ?></h3>
                    <p class="text-center"><?php echo $model->getRoleTitle() ?></p>
                </div>
            </div>
        </div>
        <?php if(Yii::$app->hasModule('moduleName')) { ?>
        <div class="col-12 col-md-7">
            <h3><?php echo Yii::t('app/frontend', 'User tournaments') ?></h3>
            <div class="tournamentsTabs">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#host">
                            <?php echo Yii::t("app/frontend", "User hosts"); ?>
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#play">
                            <?php echo Yii::t("app/frontend", "User plays"); ?>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="host" class="tab-pane fade in active">
                        <?php echo GridView::widget([
                            'dataProvider' => new ActiveDataProvider([
                                'query'      => Tournament::find()->withOwner($model->id),
                                'pagination' => [
                                    'pageSize' => 50
                                ]
                            ]),
                            'layout'       => "{items}\n{pager}",
                            'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
                            'emptyText'    => Yii::t('app/frontend', 'No tournaments yet!'),
                            'columns'      => [
                                [
                                    'attribute' => 'title',
                                    'format'    => 'raw',
                                    'value'     => function ($model) {
                                        /** @var $model Tournament */
                                        return Html::a($model->title, ['view', 'id' => $model->id]);
                                    }
                                ],
                                [
                                    'attribute' => 'state',
                                    'value'     => 'stateText'
                                ],
                                [
                                    'attribute' => 'gameSystemId',
                                    'value'     => 'gameSystem.title'
                                ]
                            ]
                        ]); ?>
                    </div>
                    <div id="play" class="tab-pane fade">
                        <?php echo GridView::widget([
                            'dataProvider' => new ActiveDataProvider([
                                'query'      => Tournament::find()->withParticipant($model->id),
                                'pagination' => [
                                    'pageSize' => 50
                                ]
                            ]),
                            'layout'       => "{items}\n{pager}",
                            'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
                            'emptyText'    => Yii::t('app/frontend', 'No tournaments yet!'),
                            'columns'      => [
                                [
                                    'attribute' => 'title',
                                    'format'    => 'raw',
                                    'value'     => function ($model) {
                                        /** @var $model Tournament */
                                        return Html::a($model->title, ['view', 'id' => $model->id]);
                                    }
                                ],
                                [
                                    'attribute' => 'state',
                                    'value'     => 'stateText'
                                ],
                                [
                                    'attribute' => 'gameSystemId',
                                    'value'     => 'gameSystem.title'
                                ]
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
