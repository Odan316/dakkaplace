<?php /** @noinspection PhpUnhandledExceptionInspection */

/**
 * @var $this View
 * @var $userForm UserEditForm
 */

use common\assets\CroppieAsset;
use common\widgets\crop_upload\CropUpload;
use frontend\models\forms\UserEditForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\web\View;

CroppieAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'id'     => 'personal-form',
    'action' => '/personal/save-user-form'
]) ?>
<div class="row">
    <div class="col-12 col-md-6">
        <?php echo $form->field($userForm, 'avatarNew')->widget(CropUpload::class, []); ?>
    </div>
    <div class="col-12 col-md-6">
        <?php echo $form->field($userForm, 'nickname')->textInput(['autofocus' => true]) ?>
        <?php echo $form->field($userForm, 'email')->textInput() ?>
        <?php echo $form->field($userForm, 'password')->passwordInput() ?>
        <?php echo $form->field($userForm, 'repeatPassword')->passwordInput() ?>

    </div>
</div>

<div class="form-group text-center">
    <?php echo Html::button(Yii::t('app/common', 'Save'),
        ['class' => 'btn btn-success btnSaveUser']) ?>
</div>

<?php ActiveForm::end() ?>
