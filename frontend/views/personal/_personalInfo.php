<?php
/**
 * @var $this yii\web\View
 * @var $user User
 */

use common\models\user\User;
use frontend\components\AdvHtmlHelper;
use yii\bootstrap4\Html;

?>
<h2 class="text-center"><?php echo Yii::t('app/frontend', 'Personal info') ?></h2>

<form class="form-horizontal">
    <div class="row">
        <div class="col-12 col-md-6">
            <?php echo Html::img($user->getAvatarSrc() . '?' . time(), ['class' => 'avatar']) ?>
        </div>
        <div class="col-12 col-md-6">
            <?php echo AdvHtmlHelper::activeStaticControl($user, 'username') ?>
            <?php echo AdvHtmlHelper::activeStaticControl($user, 'nickname') ?>
            <?php echo AdvHtmlHelper::activeStaticControl($user, 'email') ?>
            <?php echo AdvHtmlHelper::staticControl(
                Yii::t('app/frontend', 'Registered at'),
                date('d-m-Y', $user->createdAt)
            ) ?>
        </div>
    </div>
</form>
<div class="form-group text-center">
    <?php echo Html::button(Yii::t('app/common', 'Edit'),
        ['class' => 'btn btn-primary btnEditUser']) ?>
</div>
