<?php

/**
 * @var $this yii\web\View
 * @var $user User
 * @var $userForm UserEditForm
 */

use App\Tournaments\Presentation\Yii\Widgets\ProfileWidget\ProfileWidget;
use frontend\assets\PersonalAsset;
use yii\bootstrap4\Html;
use frontend\models\forms\UserEditForm;
use common\models\user\User;

PersonalAsset::register($this);

$this->title = Yii::t('app/frontend', 'Personal section');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="user-index">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center"><?php echo $this->title ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-md-8 personalInfo">
                    <?php echo $this->render('_personalInfo', ['user' => $user])?>
                </div>
                <div class="col-12 col-md-8 personalForm">
                    <?php echo $this->render('_personalForm', ['userForm' => $userForm])?>
                </div>
                <div class="col-12 col-md-4">
                    <h3 class="text-center"><?php echo Yii::t('app/frontend', 'Account type') ?></h3>
                    <p class="text-center"><?php echo $user->getRoleTitle() ?></p>
                    <?php if ($user->canBeUpgraded()) { ?>
                        <div class="form-group text-center">
                            <?php echo Html::button(Yii::t('app/frontend', 'Upgrade'),
                                ['class' => 'btn btn-primary btnUpgradeAccount']) ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    
        <?php if (Yii::$app->hasModule('tournament')) { ?>
            <?php echo ProfileWidget::getRendered($user->id); ?>
        <?php } ?>
    </div>
</div>
