<?php
namespace frontend\controllers;

use common\components\controllers\WebController;
use common\models\user\User;
use yii\filters\AccessControl;

/**
 * Site controller
 *
 * @see User
 */
class UserController extends WebController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'      => true,
                        'actions'    => ['index', 'view'],
                        'roles'      => [],
                    ]
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }

    public function actionView($id)
    {
        $model = User::find()->hasId($id)->one();

        return $this->render('view', [
            'model' => $model
        ]);
    }
}
