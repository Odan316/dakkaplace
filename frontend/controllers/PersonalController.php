<?php

namespace frontend\controllers;

use common\components\controllers\WebController;
use common\models\user\User;
use frontend\models\forms\UserEditForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class PersonalController
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class PersonalController extends WebController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'save-user-form', 'upgrade-account'],
                        'roles'   => ['@'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['view']
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('login');
        }

        $user = User::findOne(Yii::$app->user->id);

        $userForm = new UserEditForm($user);

        return $this->render('index', [
            'user'     => $user,
            'userForm' => $userForm
        ]);
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionSaveUserForm()
    {
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            $user = User::findOne(Yii::$app->user->id);

            $model = new UserEditForm($user);

            if($model->load(Yii::$app->request->post()) && $model->update()){
                return [
                    'result' => 'success',
                    'html' => $this->renderAjax('_personalInfo', ['user' => $model->getUser()])
                ];
            }

            return [
                'result' => 'error',
                'errors' => $model->getErrors(),
                'html' => $this->renderAjax('_personalForm', ['userForm' => $model])
            ];
        } else{
            throw new NotFoundHttpException();
        }
    }

    /**
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function actionUpgradeAccount()
    {
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            $auth = Yii::$app->authManager;
            $vipRole = $auth->getRole('vip_customer');
            $auth->revokeAll(Yii::$app->user->id);

            return $auth->assign($vipRole, Yii::$app->user->id);
        } else{
            throw new NotFoundHttpException();
        }
    }
}