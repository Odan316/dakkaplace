<?php
namespace frontend\controllers;

use common\components\controllers\WebController;
use common\models\user\Status;
use common\models\user\User;
use frontend\models\forms\RegisterForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\forms\LoginForm;
use frontend\models\forms\PasswordResetRequestForm;
use frontend\models\forms\ResetPasswordForm;

/**
 * Site controller
 */
class SiteController extends WebController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'register'],
                'rules' => [
                    [
                        'actions' => ['register'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     * @throws \Exception
     */
    public function actionRegister()
    {
        $model = new RegisterForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->register()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('register', [
            'model' => $model,
        ]);
    }

    /**
     * @param $token
     * @return string|\yii\web\Response
     */
    public function actionConfirmRegistration( $token ) {
        if ( ! empty( $token ) ) {
            $user = User::findByConfirmationToken( $token );
            if ( $user->isJustRegistered() ) {
                $user->status = Status::active();
                if ( $user->save() ) {
                    Yii::$app->user->login( $user, 3600 * 24 * 30 );
                    return $this->render( 'confirmRegistration', ['result' => 'success'] );
                }
            } elseif($user->isActive()) {
                return $this->render( 'confirmRegistration', ['result' => 'error', 'cause' => 'alreadyConfirmed'] );
            }
        }

        //return $this->goHome();
        return $this->render( 'confirmRegistration', [
        ] );
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::find()->andWhere(['email' => $model->email])->one();

            if (empty($user)) {
                return $this->render('requestPasswordResetToken', [
                    'result' => 'error',
                    'errorText' => Yii::t('app/frontend', 'There is no user with this email address.'),
                    'model' => $model,
                ]);
            }
            if ($user->isJustRegistered()) {
                return $this->render('requestPasswordResetToken', [
                    'result' => 'error',
                    'errorText' => Yii::t('app/frontend', 'User with this email is not confirmed. Confirm it by email or contact administration for help.'),
                    'model' => $model,
                ]);
            }

            if ($model->sendEmail()) {
                return $this->render('requestPasswordResetToken', ['result' => 'success', 'model' => $model]);
            } else {
                return $this->render('requestPasswordResetToken', [
                    'result' => 'error',
                    'errorText' => Yii::t('app/frontend', 'Sorry, we are unable to reset password for the provided email address.'),
                    'model' => $model,
                ]);
            }
        }

        return $this->render('requestPasswordResetToken', [
            'result' => '',
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     * @throws \yii\base\Exception
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            return $this->render('resetPassword', ['result' => 'success']);
        }

        return $this->render('resetPassword', [
            'result' => '',
            'model' => $model,
        ]);
    }
}
