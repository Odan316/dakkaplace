<?php
$indexActions = implode('|', [
    "login",
    "logout",
    "register",
    "request-password-reset",
    "reset-password",
    "confirm-registration",
    "unsubscribe"
]);
$controllersList = implode('|', [
    'personal',
    'user',
    //'video',
]);
return [
    ''                                                          => 'site/index',
    '<action:(' . $indexActions . ')>'                          => 'site/<action>',
    '<controller:(' . $controllersList . ')>'                   => '<controller>/index',
    '<controller:(' . $controllersList . ')>/<action>/<id:\d+>' => '<controller>/<action>',
    '<controller:(' . $controllersList . ')>/<action>'          => '<controller>/<action>'
];
