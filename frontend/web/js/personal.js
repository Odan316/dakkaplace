$(document).ready(function () {
    //--- USER PROPERTIES ---//

    bindFormValidation($(userFormSelector), handleUserSaveSuccess);

    $(document).on('click', '.btnEditUser', function () {
        $('.personalInfo').hide();
        $('.personalForm').show();
    });

    $(document).on('click', '.btnSaveUser', function () {
        $(userFormSelector).trigger('submit');
    });

    //--- UPGRADE ACCOUNT ---//

    $(document).on('click', '.btnUpgradeAccount', function () {
        upgradeAccount();
    });

});

//-- USER PERSONAL --//

var userFormSelector = '#personal-form';

function handleUserSaveSuccess(data) {
    if (data.result === 'success') {
        $('.personalForm').hide();
        $('.personalInfo').html(data.html).show();
    } else {
        $('.personalForm').html(data.html);
        bindFormValidation($(userFormSelector), handleUserSaveSuccess);
    }
}


//-- UPGRADE ACCOUNT --//

function upgradeAccount(data) {
    $.post(
        '/personal/upgrade-account',
        {},
        function (data) {
            location.reload();
        });
}