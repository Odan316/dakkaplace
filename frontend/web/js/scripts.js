//-- COMMON--//
function bindFormValidation($form, successCallback) {
    $form.on('afterValidate', function (event, messages, errorAttributes) {
        if (errorAttributes.length === 0) {
            sendForm($form, successCallback);
        }
        return true;
    });

    $form.on('submit', function () {
        return false;
    });
}

function sendForm($form, successCallback) {
    $.post($form.attr('action'),
        $form.serialize(),
        function (data) {
            successCallback(data);
        });
}