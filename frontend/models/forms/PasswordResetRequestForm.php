<?php

namespace frontend\models\forms;

use common\models\user\Status;
use common\models\user\User;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app/frontend', 'Email')
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     * @throws \yii\base\Exception
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => Status::active()->getRawValue(),
            'email'  => $this->email,
        ]);

        if (!$user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->passwordResetToken)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return
            Yii::$app->mailer
                ->compose('passwordReset/index', [
                    'username'           => $user->username,
                    'passwordResetToken' => $user->passwordResetToken
                ])
                ->setTo($this->email)
                ->setFrom(Yii::$app->params['noReplyAddress'])
                ->setSubject(Yii::t('app/frontend', 'You have requested password reset'))
                ->send();
    }
}
