<?php

namespace frontend\models\forms;

use Yii;
use yii\base\Model;
use common\models\user\User;
use yii\db\Query;

/**
 * Form for edit user params via personal section
 */
class UserEditForm extends Model
{
    public $nickname;
    public $email;
    public $password;
    public $repeatPassword;

    public $avatarNew;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nickname', 'email'], 'trim'],
            [['nickname', 'email'], 'required'],

            ['nickname', 'string', 'min' => 2, 'max' => 64],
            [
                'nickname',
                'unique',
                'targetClass' => '\common\models\user\User',
                'message'     => Yii::t('app/frontend', 'This nickname is already used'),
                'filter'      => function ($query) {
                    /** @var Query $query */
                    $query->andWhere(['not', ['id' => Yii::$app->user->id]]);
                }
            ],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => '\common\models\user\User',
                'message'     => Yii::t('app/frontend', 'This email address has already been taken.'),
                'filter'      => function ($query) {
                    /** @var Query $query */
                    $query->andWhere(['not', ['id' => Yii::$app->user->id]]);
                }
            ],

            ['password', 'string', 'min' => 6],

            ['repeatPassword', 'string', 'min' => 6],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password'],

            [['avatarNew'], 'base64img'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'nickname'       => Yii::t('app/frontend', 'Nickname'),
            'email'          => Yii::t('app/frontend', 'Email'),
            'password'       => Yii::t('app/frontend', 'New password'),
            'repeatPassword' => Yii::t('app/frontend', 'Repeat password'),
            'avatarNew'      => Yii::t('app/frontend', 'Avatar')
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function base64img($attribute, $params, $validator)
    {
        $result = false;

        $data = explode(',', $this->$attribute);
        $img = imagecreatefromstring(base64_decode($data[1]));

        $tmpPath = Yii::getAlias('@frontend/web/uploads/tmpFiles/') . uniqid("base64test", true) . '.png';

        if ($img) {
            imagepng($img, $tmpPath);
            $info = getimagesize($tmpPath);

            unlink($tmpPath);

            if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
                $result = true;
            }
        }

        if (!$result) {
            $this->addError($attribute, Yii::t('app/common', "The {attribute} should be image in base64 encoding.",
                ['attribute' => $attribute]));
        }
    }

    /**
     * UserEditForm constructor.
     * @param User $user
     * @param array $config
     */
    public function __construct($user = null, array $config = [])
    {
        parent::__construct($config);

        if (!empty($user)) {
            $this->nickname = $user->nickname;
            $this->email = $user->email;
        }
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws \yii\base\Exception
     */
    public function update()
    {
        if (!$this->validate()) {
            return null;
        }

        $this->_user = User::findOne(Yii::$app->user->id);
        $this->_user->nickname = $this->nickname;
        $this->_user->email = $this->email;
        if (!empty($this->password)) {
            $this->_user->setPassword($this->password);
            $this->_user->generateAuthKey();
        }

        if (!empty($this->avatarNew)) {
            $data = explode(',', $this->avatarNew);

            $fileName = Yii::getAlias('@frontend/web/uploads/userFiles/') . 'avatar_' . $this->_user->id . '.png';
            $file = fopen($fileName, "wb");
            fwrite($file, base64_decode($data[1]));
            fclose($file);
        }

        return $this->_user->save();
    }

    public function getUser()
    {
        return $this->_user;
    }
}
