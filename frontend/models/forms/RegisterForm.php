<?php

namespace frontend\models\forms;

use common\models\user\Role;
use common\models\user\Status;
use common\models\user\User;
use Yii;
use yii\base\Model;

/**
 * Register form
 */
class RegisterForm extends Model
{
    public $username;
    public $nickname;
    public $email;
    public $password;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'nickname', 'email'], 'trim'],
            [['username', 'nickname', 'email', 'password'], 'required'],

            ['username', 'string', 'min' => 3, 'max' => 10],
            [
                'username',
                'unique',
                'targetClass' => '\common\models\user\User',
                'message'     => Yii::t('app/frontend', 'This username is already used')
            ],

            ['nickname', 'string', 'min' => 2, 'max' => 64],
            [
                'nickname',
                'unique',
                'targetClass' => '\common\models\user\User',
                'message'     => Yii::t('app/frontend', 'This nickname is already used')
            ],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [
                'email',
                'unique',
                'targetClass' => '\common\models\user\User',
                'message'     => Yii::t('app/frontend', 'This email is already used')
            ],

            ['password', 'string', 'min' => 6, 'max' => 18],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username'       => Yii::t('app/frontend', 'Username'),
            'nickname'       => Yii::t('app/frontend', 'Nickname'),
            'email'          => Yii::t('app/frontend', 'Email'),
            'password'       => Yii::t('app/frontend', 'Password'),
            'repeatPassword' => Yii::t('app/frontend', 'Repeat password')
        ];
    }

    public function attributeHints()
    {
        return [
            'username' => 'From 3 to 10 letters, latin characters or digits'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws \Exception
     */
    public function register()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->username;
        $user->nickname = $this->nickname;
        $user->email = $this->email;
        $user->status = Status::registered();
        $user->generateConfirmationToken();
        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($user->save()) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole(Role::CUSTOMER_ROLE);
            $auth->assign($role, $user->getId());

            Yii::$app->mailer
                ->compose('registration/index', [
                    'username'          => $user->username,
                    'confirmationToken' => $user->mailConfirmationToken
                ])
                ->setTo($user->email)
                ->setFrom(Yii::$app->params['noReplyAddress'])
                ->setSubject(Yii::t('app/frontend', 'You have registered on DakkaPlace.ru'))
                ->send();

            return $user;
        } else {
            $this->addErrors($user->getErrors());
            return null;
        }
    }
}
