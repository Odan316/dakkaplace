<?php

namespace frontend\components;

use Yii;
use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/**
 * Class AdvHtmlHelper
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class AdvHtmlHelper
{
    /**
     * @param string $labelText
     * @param string $value
     * @param array $options
     *
     * @return string
     */
    public static function staticControl($labelText, $value, $options = [])
    {
        $html = Html::beginTag('div', ['class' => 'form-group']);

        $html .= Html::label(
            Html::encode($labelText),
            null,
            ArrayHelper::merge(['class' => 'control-label col-sm-4'], isset($options['labelOptions'])
                ? $options['labelOptions']
                : [])
        );

        $valueHtml = Html::staticControl($value, ['encode' => true]);

        if(isset($options['valueWrapper']) && $options['valueWrapper'] !== null){
            $html.= Html::tag($options['valueWrapper']['tag'], $valueHtml, $options['valueWrapper']['options']);
        } elseif(isset($options['valueWrapper']) && $options['valueWrapper'] === null) {
            $html .= $valueHtml;
        } else {
            $html.= Html::tag('div', $valueHtml, ['class' => 'col-sm-8']);
        }

        $html .= Html::endTag('div');

        return $html;
    }

    /**
     * @param Model $model
     * @param string $attribute
     * @param array $options
     *
     * @return string
     */
    public static function activeStaticControl($model, $attribute, $options = [])
    {
        return self::staticControl($model->getAttributeLabel($attribute), $model->$attribute, $options);
    }

}