<?php

namespace frontend\components\helpers;


/**
 * Class ImperialDateHelper
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class ImperialDateHelper
{
    const MARK_CONSTANT = 0.11407955;

    /**
     * Returns current date, converted to Imperial dating system
     * @return string
     */
    public static function getImperialDate()
    {
        $checkNumber = '0'; // Terra
        $yearFraction = str_pad(round((date('z')*24+date('G'))*self::MARK_CONSTANT), 3, '0', STR_PAD_LEFT);
        $year = str_pad(date('y'), 3, '0', STR_PAD_LEFT);
        $millennium = '3'; // constant))
        return $checkNumber.$yearFraction.$year.'.M'.$millennium;
    }
}