<?php
/**
 * Is modified copy of m140506_102106_rbac_init
 */

use common\models\user\User;
use console\components\SchemaHelper;
use yii\base\InvalidConfigException;
use yii\rbac\DbManager;
use yii\rbac\Item;

class m000001_000002_basic_roles extends \yii\db\Migration
{
    /**
     * @throws yii\base\InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }

    /**
     * @return bool|void
     * @throws InvalidConfigException
     * @throws \yii\base\Exception
     * @throws Exception
     */
    public function up()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        if (!isset(Yii::$app->params['adminEmail']) || empty(Yii::$app->params['adminEmail'])) {
            throw new InvalidConfigException('You should configure "adminEmail" param before executing this migration.');
        }

        $tableOptions = SchemaHelper::getTableOptions($this->db->driverName);

        //---- These tables are copied from m140506_102106_rbac_init ----//

        $this->createTable($authManager->ruleTable, [
            'name' => $this->string(64)->notNull(),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'PRIMARY KEY (name)',
        ], $tableOptions);

        $this->createTable($authManager->itemTable, [
            'name' => $this->string(64)->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'rule_name' => $this->string(64),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'PRIMARY KEY (name)',
            'FOREIGN KEY (rule_name) REFERENCES ' . $authManager->ruleTable . ' (name) ON DELETE SET NULL ON UPDATE CASCADE',
        ], $tableOptions);
        $this->createIndex('idx-auth_item-type', $authManager->itemTable, 'type');

        $this->createTable($authManager->itemChildTable, [
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
            'PRIMARY KEY (parent, child)',
            'FOREIGN KEY (parent) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY (child) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->createTable($authManager->assignmentTable, [
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->string(64)->notNull(),
            'created_at' => $this->integer(),
            'PRIMARY KEY (item_name, user_id)',
            'FOREIGN KEY (item_name) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        //---- Init basic user roles ----//

        $this->insert($authManager->itemTable, [
            "name" => "admin",
            "type" => Item::TYPE_ROLE,
            "description" => "Role group, that permit administrative access",
            "created_at" => time(),
            "updated_at" => time()
        ]);

        $this->insert($authManager->itemTable, [
            "name" => "vip_customer",
            "type" => Item::TYPE_ROLE,
            "description" => "Role group, that permit all basic actions for registered user and viewing of premium content",
            "created_at" => time(),
            "updated_at" => time()
        ]);

        $this->insert($authManager->itemTable, [
            "name" => "customer",
            "type" => Item::TYPE_ROLE,
            "description" => "Role group, that permit all basic actions for registered user",
            "created_at" => time(),
            "updated_at" => time()
        ]);



        $admin = new User();
        $admin->username = "admin";
        $admin->nickname = "Admin";
        $admin->email = Yii::$app->params['adminEmail'];
        $admin->setPassword("adminPass");
        $admin->generateAuthKey();
        $admin->save(false);

        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('admin');
        $auth->assign($adminRole, $admin->getId());

    }

    /**
     * @return bool|void
     * @throws InvalidConfigException
     */
    public function down()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $this->dropTable($authManager->assignmentTable);
        $this->dropTable($authManager->itemChildTable);
        $this->dropTable($authManager->itemTable);
        $this->dropTable($authManager->ruleTable);
    }
}
