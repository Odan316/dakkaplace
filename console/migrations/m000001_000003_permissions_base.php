<?php

/**
 */
class m000001_000003_permissions_base extends \yii\db\Migration
{
    /**
     * @return bool|void
     * @throws Exception
     */
    public function up()
    {
        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('admin');

        $perm = $auth->createPermission('backend/overview/view');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);

        $perm = $auth->createPermission('backend/users/roles/view');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $perm = $auth->createPermission('backend/users/roles/edit');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);


        $perm = $auth->createPermission('backend/users/accounts/view');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $perm = $auth->createPermission('backend/users/accounts/create');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $perm = $auth->createPermission('backend/users/accounts/edit');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);

        $perm = $auth->createPermission('backend/dictionaries/view');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $perm = $auth->createPermission('backend/dictionaries/edit');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $auth = Yii::$app->authManager;

        $perm = $auth->getPermission('backend/users/roles/view');
        $auth->remove($perm);
        $perm = $auth->getPermission('backend/users/roles/edit');
        $auth->remove($perm);

        $perm = $auth->getPermission('backend/users/accounts/view');
        $auth->remove($perm);
        $perm = $auth->getPermission('backend/users/accounts/create');
        $auth->remove($perm);
        $perm = $auth->getPermission('backend/users/accounts/edit');
        $auth->remove($perm);

        $perm = $auth->getPermission('backend/dictionaries/view');
        $auth->remove($perm);
        $perm = $auth->getPermission('backend/dictionaries/edit');
        $auth->remove($perm);
    }
}
