<?php

use console\components\SchemaHelper;
use yii\db\Migration;

class m000001_000001_create_users_table extends Migration
{
    public function up()
    {
        $tableOptions = SchemaHelper::getTableOptions($this->db->driverName);

        $this->createTable('{{%user}}', [
            'id'                    => $this->primaryKey(),
            'username'              => $this->string(32)->notNull()->unique(),
            'email'                 => $this->string()->notNull()->unique(),
            'nickname'              => $this->string()->notNull()->unique(),
            'authKey'               => $this->string(32)->notNull(),
            'passwordHash'          => $this->string()->notNull(),
            'passwordResetToken'    => $this->string()->unique(),
            'mailConfirmationToken' => $this->string(32),
            'status'                => $this->smallInteger()->notNull()->defaultValue(10),
            'createdAt'             => $this->integer()->notNull(),
            'updatedAt'             => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
