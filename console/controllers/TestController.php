<?php

declare(strict_types=1);

namespace console\controllers;

use Yii;
use yii\console\Controller;

final class TestController extends Controller
{
    public function actionMail()
    {
        echo "SENDING\r\n";

        $result = Yii::$app->mailer
            ->compose()
            ->setFrom(Yii::$app->params['noReplyAddress'])
            ->setTo('test@mail.com')
            ->setSubject('It is test email from Dakka')
            ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
            ->send();

        echo "SENT\r\n";
    }
}