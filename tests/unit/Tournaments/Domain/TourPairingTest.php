<?php

namespace Tests\unit\Tournaments\Domain;

use App\Tournaments\Domain\Faction;
use App\Tournaments\Domain\PairsCollection;
use App\Tournaments\Domain\Participant;
use App\Tournaments\Domain\ParticipantCollection;
use App\Tournaments\Domain\Tour;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class TourPairingTest extends TestCase
{
    /**
     * @dataProvider examples
     *
     * @param ParticipantCollection $participantCollection
     * @param int $expectedPairsCount
     */
    public function test(ParticipantCollection $participantCollection, int $expectedPairsCount)
    {
        $tour = new Tour(1, new PairsCollection());

        $tour->createPairs($participantCollection);

        $this->assertEquals($expectedPairsCount, $tour->getPairs()->getCount());

        $userIds = [];
        foreach($tour->getPairs()->getAll() as $pair){
            $userIds[] = $pair->getPlayerOne()->getUserId();
            $userIds[] = $pair->getPlayerTwo()->getUserId();
        }

        $frequencies = array_count_values($userIds);

        foreach($frequencies as $userId){
            $this->assertEquals(1, $frequencies[$userId]);
        }

    }

    public function examples(): array
    {
        return [
            [
                new ParticipantCollection([
                    new Participant(1, 't1', new Faction(1, 'f1'), new DateTimeImmutable()),
                    new Participant(2, 't2', new Faction(1, 'f2'), new DateTimeImmutable()),
                    new Participant(3, 't3', new Faction(1, 'f1'), new DateTimeImmutable()),
                    new Participant(4, 't4', new Faction(1, 'f3'), new DateTimeImmutable()),
                ]),
                2,
            ],
            [
                new ParticipantCollection([
                    new Participant(1, 't1', new Faction(1, 'f1'), new DateTimeImmutable()),
                    new Participant(2, 't2', new Faction(1, 'f2'), new DateTimeImmutable()),
                    new Participant(3, 't3', new Faction(1, 'f1'), new DateTimeImmutable()),
                    new Participant(4, 't4', new Faction(1, 'f3'), new DateTimeImmutable()),
                    new Participant(5, 't5', new Faction(1, 'f1'), new DateTimeImmutable()),
                ]),
                3,
            ],
            [
                new ParticipantCollection([
                    new Participant(1, 't1', new Faction(1, 'f1'), new DateTimeImmutable()),
                    new Participant(2, 't2', new Faction(1, 'f2'), new DateTimeImmutable()),
                    new Participant(3, 't3', new Faction(1, 'f1'), new DateTimeImmutable()),
                    new Participant(4, 't4', new Faction(1, 'f3'), new DateTimeImmutable()),
                    new Participant(5, 't5', new Faction(1, 'f1'), new DateTimeImmutable()),
                    new Participant(6, 't6', new Faction(1, 'f4'), new DateTimeImmutable()),
                ]),
                3,
            ],
        ];
    }
}