all: build app

build:
	docker-compose build

app:
	docker-compose -p dakka up webapp webserver
