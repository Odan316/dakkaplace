<?php

namespace common\components;

/**
 * Class AdvStringHelper
 * @package common\components
 */
class AdvStringHelper
{
    public static function mbUcfirst($string, $encoding = 'utf-8')
    {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }

    /**
     * @param $string
     * @param $maxLengthInChars
     * @param string $finalizeWith
     * @param string $delimiter
     * @return string
     */
    public static function wrapStringByWords($string, $maxLengthInChars, $finalizeWith = '', $delimiter = " ")
    {
        if(mb_strlen($string) > $maxLengthInChars){
            $str = mb_substr($string, 0, $maxLengthInChars - 1, 'utf-8');
            $lastSpace = mb_strrpos($str, $delimiter, 'utf-8');
            $str = mb_substr($string, 0, $lastSpace, 'utf-8');
            $str .= $finalizeWith;
        } else {
            $str = $string;
        }

        return $str;
    }

    public static function mbCutString($string, $maxLengthInChars, $finalizeWith = '')
    {
        $str = mb_substr($string, 0, $maxLengthInChars - 1, 'utf-8');
        if(mb_strlen($string) > $maxLengthInChars){
            $str .= $finalizeWith;

        }

        return (string)$str;
    }

    public static function snakeToCamel($val)
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $val))));
    }

    /**
     *
     * @link http://www.manhunter.ru/webmaster/852_funkciya_transliteracii_russkogo_teksta_na_php.html
     *
     * @param $str
     * @return mixed|string
     */
    public static function slugify($str, $maxLength = 24)
    {
        // Установить опции и кодировку регулярных выражений
        mb_regex_set_options('pd');
        mb_internal_encoding('UTF-8');

        // Привести строку к UTF-8
        if (strtolower(mb_detect_encoding($str,
                'utf-8, windows-1251'))=='windows-1251') {
            $str=mb_convert_encoding($str, 'utf-8', 'windows-1251');
        }

        // Регулярки для удобства
        $regexp1='(?=[A-Z0-9А-Я])';
        $regexp2='(?<=[A-Z0-9А-Я])';

        // Массивы для замены заглавных букв, идущих последовательно
        $rus=array(
            '/(Ё'.$regexp1.')|('.$regexp2.'Ё)/u',
            '/(Ж'.$regexp1.')|('.$regexp2.'Ж)/u',
            '/(Ч'.$regexp1.')|('.$regexp2.'Ч)/u',
            '/(Ш'.$regexp1.')|('.$regexp2.'Ш)/u',
            '/(Щ'.$regexp1.')|('.$regexp2.'Щ)/u',
            '/(Ю'.$regexp1.')|('.$regexp2.'Ю)/u',
            '/(Я'.$regexp1.')|('.$regexp2.'Я)/u'
        );

        $eng=array(
            'yo','zh','ch','sh','sch','yu','ya'
        );

        // Заменить заглавные буквы, идущие последовательно
        $str=preg_replace($rus,$eng,$str);

        // Массивы для замены одиночных заглавных и строчных букв
        $rus=array(
            '/а/u','/б/u','/в/u','/г/u','/д/u','/е/u','/ё/u',
            '/ж/u','/з/u','/и/u','/й/u','/к/u','/л/u','/м/u',
            '/н/u','/о/u','/п/u','/р/u','/с/u','/т/u','/у/u',
            '/ф/u','/х/u','/ц/u','/ч/u','/ш/u','/щ/u','/ъ/u',
            '/ы/u','/ь/u','/э/u','/ю/u','/я/u',

            '/А/u','/Б/u','/В/u','/Г/u','/Д/u','/Е/u','/Ё/u',
            '/Ж/u','/З/u','/И/u','/Й/u','/К/u','/Л/u','/М/u',
            '/Н/u','/О/u','/П/u','/Р/u','/С/u','/Т/u','/У/u',
            '/Ф/u','/Х/u','/Ц/u','/Ч/u','/Ш/u','/Щ/u','/Ъ/u',
            '/Ы/u','/Ь/u','/Э/u','/Ю/u','/Я/u'
        );

        $eng=array(
            'a','b','v','g','d','e','yo',
            'zh','z','i','y','k','l','m',
            'n','o','p','r','s','t','u',
            'f','h','c','ch','sh','sch','',
            'i','','e','yu','ya',

            'a','b','v','g','d','e','yo',
            'zh','z','i','y','k','l','m',
            'n','o','p','r','s','t','u',
            'f','h','c','ch','sh','sch','',
            'i','','e','yu','ya'
        );

        // Заменить оставшиеся заглавные и строчные буквы
        $str=preg_replace($rus,$eng,$str);

        $str = preg_replace(['/[^A-z0-9]/'], '_', $str);
        $str = preg_replace(['/[_]{2,}/'], '_', $str);

        if(strrpos($str, '_') == strlen($str)-1){
            $str = substr($str, 0, strlen($str)-1);
        }

        $str = mb_strtolower($str, 'utf-8');

        $str = static::wrapStringByWords($str, $maxLength, "", "_");

        return $str;
    }
}