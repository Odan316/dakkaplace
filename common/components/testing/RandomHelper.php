<?php


namespace common\components\testing;

use common\components\AdvStringHelper;
use Faker\Factory;

/**
 * Class RandomHelper
 * @package app\components\helpers
 */
class RandomHelper
{
    /**
     * @param int $digitsNumber
     * @return string
     */
    public static function randomNumberString(int $digitsNumber)
    {
        $number = "";
        for ($i = 0; $i < $digitsNumber; $i++) {
            $min = ($i == 0) ? 1 : 0;
            $number .= mt_rand($min, 9);
        }
        return $number;
    }

    public static function entitiyTitle($words = 2)
    {
        $faker = Factory::create();

        $words = $faker->words($words);

        return AdvStringHelper::mbUcfirst(implode(' ', $words));
    }
}