<?php

namespace common\components;

use Yii;
use yii\helpers\VarDumper;
use yii\web\AssetBundle;

/**
 * Class CustomAssetBundle
 *
 * We can't simply append timestamp to ALL assets via AssetBundle::appendTimestamp, because some vendor components has
 * included JS libraries (i.e. CKEditor has included jquery.js), with different mtime. This libraries conflicted
 * when included in DOM via html insert (from AJAX)
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class CustomAssetBundle extends AssetBundle
{
    /**
     * Initializes the bundle.
     *
     * Adds version timestamp for bundle files
     */
    public function init()
    {
        parent::init();

        foreach ($this->js as &$file){
            $file = $this->getVersionTime($file);
        }
        foreach ($this->css as &$file){
            $file = $this->getVersionTime($file);
        }
    }

    protected function  getVersionTime($file)
    {
        if(file_exists($this->basePath.'/'.$file)){
            $version = filemtime($this->basePath.'/'.$file);
        } elseif(file_exists($this->sourcePath.'/'.$file)) {
            $version = filemtime($this->sourcePath . '/' . $file);
        } else {
            $version = time();
        }

        return $file.'?fv='.$version;
    }

}