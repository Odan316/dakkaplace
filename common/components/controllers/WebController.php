<?php

namespace common\components\controllers;

use Yii;
use yii\web\Controller;

/**
 * Class WebController
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class WebController extends Controller
{
    public function init()
    {
        Yii::$app->language = 'ru_RU';
        parent::init();
    }
}