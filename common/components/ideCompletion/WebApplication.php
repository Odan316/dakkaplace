<?php

use common\components\ImagesHelper;
use yii\web\Application;
use yii\web\UrlManager;

/**
 * Class WebApplication
 *
 * Include only Web application related components here
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 *
 * @property User $user The user component. This property is read-only.
 * @property UrlManager $urlManagerFront
 * @property ImagesHelper $images
 */
abstract class WebApplication extends Application
{

}