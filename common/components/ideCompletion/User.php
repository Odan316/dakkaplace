<?php

/**
 * Class WebApplication
 *
 * Include only Web application related components here
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 *
 * @property \common\models\user\User $identity
 */
abstract class User extends \yii\web\User
{

}