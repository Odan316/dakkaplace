<?php

namespace common\components\rbac;

use common\models\user\User;
use Yii;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * Class VideoViewPrivateRule
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class VideoViewPrivateRule extends Rule
{
    public $name = 'canViewPrivateVideo';

    /**
     * @param string|integer $user   the user ID.
     * @param Item           $item   the role or permission that this rule is associated with
     * @param array          $params parameters passed to ManagerInterface::checkAccess().
     *
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->identity->getRole()->name === 'admin') {
            return true;
        }

        $user = User::find()->hasId($user)->one();

        if(!$user->canBeUpgraded()) {
            return true;
        }

        return false;
    }
}