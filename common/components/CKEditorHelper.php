<?php

namespace common\components;

/**
 * Class CKEditorHelper
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class CKEditorHelper
{
    public static function getMinimalPreset()
    {
        return [
            'toolbarGroups' => [
                ['name' => 'clipboard', 'groups' => ['undo', 'clipboard']],
                ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                ['name' => 'colors'],
                ['name' => 'styles'],
                ['name' => 'paragraph', 'groups' => [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ]],
                ['name' => 'links', 'groups' => ['links']],
                ['name' => 'insert', 'groups' => ['insert']],
                ['name' => 'document', 'groups' => ['mode', 'document', 'doctools']],
            ],
            'removeButtons' => 'Image,Templates,Save,NewPage,Preview,Print,Cut,PasteText,PasteFromWord,Copy,Find,SelectAll,Scayt,Replace,Form,HiddenField,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,Subscript,Superscript,CopyFormatting,Outdent,Indent,Blockquote,CreateDiv,BidiLtr,BidiRtl,Language,Flash,Smiley,SpecialChar,PageBreak,Iframe,Font,FontSize,About,Maximize,ShowBlocks,Styles',
            //'removePlugins' => 'elementspath',
            'resize_enabled' => false,
            'extraPlugins' => 'colorbutton,justify'
        ];
    }
}