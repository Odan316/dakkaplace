<?php
/**
 * Class ImagesHelper
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 */


namespace common\components;


use alexBond\thumbler\Thumbler;
use Imagick;
use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\log\Logger;

/**
 * Class ImagesHelper
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 *
 * @property $sourcePath
 * @property $thumbsPath
 */
class ImagesHelper extends Thumbler
{
    public $cachePath;
    public $originalsPath;

    /**
     * @param string $filePath
     * @param string|array $size
     * @param string|bool $background
     * @param bool $isFilesystemPath
     *
     * @return string
     */
    public function getSrc($filePath, $size = 'original', $background = false, $isFilesystemPath = false)
    {
        if (empty($filePath)) {
            return null;
        }

        try {
            if (!is_array($size)) {
                switch ($size) {
                    case 'medium':
                        $size = [320, 320];
                        break;
                    case 'thumb':
                        $size = [150, 150];
                        break;
                    case 'micro':
                        $size = [50, 50];
                        break;
                    case 'original':
                    default:
                        if ($isFilesystemPath == false) {
                            return $filePath;
                        } else {
                            return $this->makeFilePath($filePath);
                        }
                        break;
                }
            }
            if ($isFilesystemPath == false) {
                $result = $this->resize($filePath, $size[0], $size[1], $background);
            } else {
                $result = $this->resizeInFilesystem($filePath, $size[0], $size[1], $background);
            }
            return $result;
        } catch (Exception $e) {
            Yii::$app->log->logger->log($e->getMessage(), Logger::LEVEL_WARNING, 'imagesHelper/getSrc');
            return $filePath;
        }
    }

    /**
     * Method to get right path to image resized by Thumbler.
     * If there is any problems from double slash between image path and cache folder - set [[$trimSlash]] true
     *
     * @param string $image
     * @param int $width
     * @param int $height
     * @param int|string $method
     * @param boolean|string $backgroundColor
     * @param bool $callExceptionOnError
     *
     * @return string URL to image
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function resize(
        $image,
        $width,
        $height,
        $backgroundColor = false,
        $method = Thumbler::METHOD_NOT_BOXED,
        $callExceptionOnError = true
    ) {
        return Yii::getAlias($this->cachePath) . $this->resizeCore($image, $width, $height, $backgroundColor, $method,
                $callExceptionOnError);
    }

    /**
     * Method to get right path to image resized by Thumbler.
     * If there is any problems from double slash between image path and cache folder - set [[$trimSlash]] true
     *
     * @param string $image
     * @param int $width
     * @param int $height
     * @param int|string $method
     * @param boolean|string $backgroundColor
     * @param bool $callExceptionOnError
     *
     * @return string absolute path to image
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function resizeInFilesystem(
        $image,
        $width,
        $height,
        $backgroundColor = false,
        $method = Thumbler::METHOD_NOT_BOXED,
        $callExceptionOnError = true
    ) {


        $resizeCoreResult = $this->resizeCore($image, $width, $height, $backgroundColor, $method,
            $callExceptionOnError);
        if ($resizeCoreResult <> $image && $resizeCoreResult <> urldecode($image)) {
            $result = Yii::getAlias($this->thumbsPath) . $resizeCoreResult;
        } else {
            $result = Yii::getAlias($this->sourcePath) . $resizeCoreResult;
        }
        return $result;
    }

    /**
     * Core method to get right path to image resized by Thumbler.
     * If there is any problems from double slash between image path and cache folder - set [[$trimSlash]] true
     *
     * @param string $image
     * @param int $width
     * @param int $height
     * @param int|string $method
     * @param boolean|string $backgroundColor
     * @param bool $callExceptionOnError
     *
     * @return string relative path to image started with / (eg.:/lala/lla.jpg)
     * @throws Exception
     * @throws InvalidConfigException
     */
    protected function resizeCore(
        $image,
        $width,
        $height,
        $backgroundColor = false,
        $method = Thumbler::METHOD_NOT_BOXED,
        $callExceptionOnError = true
    ) {
        if ($backgroundColor === false) {
            $backgroundPath = 'transparent';
            $backgroundColor = -1;
        } else {
            $backgroundPath = $backgroundColor;
            $backgroundColor = '#' . $backgroundColor;
        }

        $this->checkConfig();

        $image = urldecode($image);
        $imageProperPath = str_replace($this->originalsPath, '', $image);
        $imageFSPath = Yii::getAlias($this->sourcePath) . $image;

        $cacheFolder = $method . '_' . $width . 'x' . $height . '_' . $backgroundPath;
        $resizedPath = DIRECTORY_SEPARATOR . $cacheFolder . $imageProperPath;
        $resizedFullPath = Yii::getAlias($this->thumbsPath) . DIRECTORY_SEPARATOR . $cacheFolder . DIRECTORY_SEPARATOR . $imageProperPath;
        if (!file_exists($resizedFullPath)) {
            $this->zebraInstance->enlarge_smaller_images = false;
            $this->zebraInstance->source_path = $imageFSPath;
            $this->zebraInstance->target_path = $resizedFullPath;

            $targetInfo = pathinfo($resizedFullPath);
            if (!is_dir($targetInfo['dirname'])) {
                mkdir($targetInfo['dirname'], 0777, true);
            }

            // We need to check validity of image before pass it to resize to handle GD bug
            // https://stackoverflow.com/questions/45174672/imagecreatefrompng-and-imagecreatefromstring-causes-to-unrecoverable-fatal-err
            try {
                $imagick = new Imagick($imageFSPath);
                $imagick->valid();
            } catch (\Exception $e) {
                // simply return non-resized image
                Yii::$app->log->logger->log($e->getMessage(), Logger::LEVEL_WARNING, 'imagesHelper/resizeCore');
                return $image;
            }

            // Wrap resizing to catch in rare (but experienced) case of internal uncatched error
            try {
                if (!$this->zebraInstance->resize($width, $height, $method, $backgroundColor)) {
                    if ($callExceptionOnError) {
                        $this->callException($this->zebraInstance->error);
                    }
                }
                return $resizedPath;

            } catch (\Exception $e) {
                Yii::$app->log->logger->log($e->getMessage(), Logger::LEVEL_WARNING, 'imagesHelper/resizeCore');
                // simply return non-resized image
                return $image;
            }

        }

        return $resizedPath;
    }

    public function makeFilePath($filePath)
    {
        return Yii::getAlias($this->sourcePath).''.urldecode($filePath);
    }

    /**
     * @param $error
     * @throws Exception
     */
    private function callException($error)
    {
        switch ($error) {
            case 1:
                throw new Exception('Source file could not be found!');
                break;
            case 2:
                throw new Exception('Source file is not readable!');
                break;
            case 3:
                throw new Exception('Could not write target file!');
                break;
            case 4:
                throw new Exception('Unsupported source file format!');
                break;
            case 5:
                throw new Exception('Unsupported target file format!');
                break;
            case 6:
                throw new Exception('GD library version does not support target file format!');
                break;
            case 7:
                throw new Exception('GD library is not installed!');
                break;
        }
    }
}