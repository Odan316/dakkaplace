<?php
return [
    'bsVersion' => '4.x', // this will set globally `bsVersion` to Bootstrap 5.x for all Krajee Extensions
    'adminEmail' => 'admin@dakkaplace.com',
    'supportEmail' => 'support@dakkaplace.com',
    'noReplyAddress' => 'noreply@dakkaplace.com',
    'user.passwordResetTokenExpire' => 3600,
    'user.authenticationTime' => 3600 * 24 * 30,
];
