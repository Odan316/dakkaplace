<?php

declare(strict_types=1);

return yii\helpers\ArrayHelper::merge(
    [
        'definitions' => [
        
        ],
    ],
    require(__DIR__ . '/../../src/Common/config/container.php'),
    // TODO: move to module
    require(__DIR__ . '/../../src/Tournaments/config/container.php'),
    require(__DIR__ . '/../../src/Tournaments/Modules/Admin/config/container.php'),
);