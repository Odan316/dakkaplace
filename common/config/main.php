<?php

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => [
        //'video',
        //'community',
        //'outcomes',
        'tournaments',
        'tournaments/admin',
    ],
    'container' => yii\helpers\ArrayHelper::merge(
        require __DIR__ . '/container.php',
        require __DIR__ . '/container-local.php',
    ),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'assetManager' => [
            'forceCopy' => YII_DEBUG,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
        'images' => [
            'class' => 'common\components\ImagesHelper',
            'sourcePath' => '@app/web',
            'thumbsPath' => '@app/web/uploads/resizeCache',
            'cachePath' => '@web/uploads/resizeCache',
            'originalsPath' => '/uploads/originals',
        ],
        'i18n' => [
            'translations' => [
                'yii' => [
                    'class' => 'yii\i18n\GettextMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en',
                    'catalog' => 'yii',
                ],
                'app/common*' => [
                    'class' => 'yii\i18n\GettextMessageSource',
                    'basePath' => '@common/messages',
                    'catalog' => 'common',
                    'sourceLanguage' => 'en',
                ],
                'common*' => [
                    'class' => 'yii\i18n\GettextMessageSource',
                    'basePath' => '@common/messages',
                    'catalog' => 'common',
                    'sourceLanguage' => 'en',
                ],
            ],
        ],
    ],
    'modules' => [
        /**'video' => [
         * 'class' => 'common\modules\video\Module',
         * ],*/
        /**'community' => [
         * 'class' => 'common\modules\community\Module',
         * ],*/
        /**'outcomes' => [
         * 'class' => 'common\modules\outcomes\Module',
         * ]*/
        'tournaments' => [
            'class' => 'App\Tournaments\Infrastructure\Yii\Module',
            'viewPath' => '@App/Tournaments/Presentation/Yii/views',
            'modules' => [
                'admin' => [
                    'class' => 'App\Tournaments\Modules\Admin\Infrastructure\Yii\Module',
                    'viewPath' => '@App/Tournaments/Modules/Admin/Presentation/Yii/views',
                ],
            ],
        ],
    ],
];
