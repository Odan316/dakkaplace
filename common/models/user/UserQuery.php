<?php

namespace common\models\user;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see \common\models\user\User
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class UserQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $id
     * @return UserQuery
     */
    public function hasId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['status' => Status::active()->getRawValue()]);
    }

    /**
     * @param $email
     * @return UserQuery
     */
    public function withEmail($email)
    {
        return $this->andWhere(['email' => $email]);
    }

    /**
     * @return UserQuery
     */
    public function notBanned()
    {
        return $this->andWhere(['not', ['status' => Status::inactive()->getRawValue()]]);
    }

    /**
     * @return UserQuery
     */
    public function banned()
    {
        return $this->andWhere(['status' => Status::inactive()->getRawValue()]);
    }

    /**
     * @param $tag
     * @return UserQuery
     */
    public function withRole($tag)
    {
        return $this
            ->leftJoin('auth_assignment', "auth_assignment.user_id = user.id")
            ->andWhere(['auth_assignment.item_name' => $tag]);
    }
}