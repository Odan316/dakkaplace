<?php
declare(strict_types=1);

namespace common\models\user;

use LitGroup\Enumerable\Enumerable;

final class Status extends Enumerable
{
    public static function inactive(): self {
        return self::createEnum(1);
    }

    public static function registered(): self
    {
        return self::createEnum(5);
    }

    public static function active(): self
    {
        return self::createEnum(10);
    }
}