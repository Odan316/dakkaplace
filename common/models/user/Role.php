<?php

namespace common\models\user;

/**
 * Class Role
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class Role
{
    const ADMIN_ROLE = 'admin';
    const VIP_CUSTOMER_ROLE = 'vip_customer';
    const CUSTOMER_ROLE = 'customer';
}