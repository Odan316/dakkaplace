<?php

namespace common\models\user;

use LitGroup\Enumerable\Enumerable;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\AttributeTypecastBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $nickname
 * @property string $email
 * @property string $authKey
 * @property Status $status
 * @property string $passwordHash
 * @property string $passwordResetToken
 * @property string $mailConfirmationToken
 * @property integer $createdAt
 * @property integer $updatedAt
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ADMIN_ROLE = 'admin';
    const CUSTOMER_ROLE = 'customer';
    const VIP_ROLE = 'vip_customer';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'typecast' => [
                'class' => AttributeTypecastBehavior::class,
                'attributeTypes' => [
                    'status' => function ($value) {
                        if(is_object($value)){
                            return $value->getRawValue();
                        }
                        return Status::getValueOf($value);
                    },
                ],
                'typecastAfterValidate' => false,
                'typecastBeforeSave' => true,
                'typecastAfterFind' => true,
            ],
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => Status::active()],
            ['status', 'in', 'range' => Status::getValues()],

            [['passwordHash', 'passwordResetToken', 'mailConfirmationToken', 'authKey'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('app/common', "ID"),
            'username'  => Yii::t('app/common', "Username"),
            'nickname'  => Yii::t('app/common', "Nickname"),
            'email'     => Yii::t('app/common', "Email"),
            'password'  => Yii::t('app/common', "Password"),
            'status'    => Yii::t('app/common', "Status"),
            'createdAt' => Yii::t('app/common', "Registered at"),
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            Status::inactive()->getRawValue()   => Yii::t('app/common', "Banned"),
            Status::registered()->getRawValue() => Yii::t('app/common', "Registered"),
            Status::active()->getRawValue()     => Yii::t('app/common', "Active")
        ];
    }

    /**
     * @return \yii\rbac\Role|null
     */
    public function getRole()
    {
        $allRoles = Yii::$app->authManager->getRolesByUser($this->id);

        if (!empty($allRoles)) {
            return current($allRoles);
        } else {
            return null;
        }
    }
    /**
     * @return string
     */
    public function getRoleTitle()
    {
        $titles = [
            self::ADMIN_ROLE           => Yii::t('app/common', "Admin"),
            self::CUSTOMER_ROLE        => Yii::t('app/common', "Customer"),
            self::VIP_ROLE         => Yii::t('app/common', "VIP Account"),
        ];

        return isset($titles[$this->role->name]) ? $titles[$this->role->name] : current($titles);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => [
            Status::registered()->getRawValue(),
            Status::active()->getRawValue()
        ]]);
    }

    /**
     * {@inheritdoc}
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername(string $username)
    {
        return static::findOne(['username' => $username, 'status' => [
            Status::registered()->getRawValue(),
            Status::active()->getRawValue()
        ]]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'passwordResetToken' => $token,
            'status'             =>  Status::active()->getRawValue(),
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     * @throws \yii\base\Exception
     */
    public function generatePasswordResetToken()
    {
        $this->passwordResetToken = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->passwordResetToken = null;
    }

    /** SIGN UP CONFIRMATION SECTION */

    /**
     * Generates confirmation token
     * @throws \yii\base\Exception
     */
    public function generateConfirmationToken()
    {
        $this->mailConfirmationToken = Yii::$app->security->generateRandomString();
    }

    /**
     * Finds user by confirmation token
     *
     * @param string $token
     *
     * @return static|null
     */
    public static function findByConfirmationToken($token)
    {
        return static::findOne(['mailConfirmationToken' => $token]);
    }

    /**
     * @return bool
     */
    public function isJustRegistered()
    {
        return $this->status === Status::registered();
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status === Status::active();
    }

    /**
     * @return bool
     */
    public function isInactive()
    {
        return $this->status === Status::inactive();
    }

    /**
     * @return bool
     */
    public function isNotBanned()
    {
        return $this->status !== Status::inactive();
    }
    /**
     * @return bool
     */
    public function isBanned()
    {
        return $this->status === Status::inactive();
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->getRole()->name == 'admin';
    }

    public function canBeUpgraded()
    {
        return $this->getRole()->name == 'customer';
    }

    /**
     * @return bool|string
     */
    public function getAvatarSrc()
    {
        $avatarFileName = 'avatar_'.$this->id.'.png';
        $avatarPath = Yii::getAlias("@app/web/uploads/userFiles/{$avatarFileName}");
        if(file_exists($avatarPath)){
            return Yii::getAlias("@web/uploads/userFiles/{$avatarFileName}");
        } else {
            return Yii::getAlias("@web/images/no_avatar.png");
        }
    }

    public function getStatusText(): string
    {
        return User::getStatuses()[$this->status->getRawValue()];
    }
}
