<?php

namespace common\modules\tournament\rbac;

use App\Tournaments\Infrastructure\Yii\ActiveRecords\Tournament;
use Yii;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * Class TournamentEditRule
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class TournamentEditRule extends Rule
{
    public $name = 'canEditTournament';

    /**
     * @param string|integer $user   the user ID.
     * @param Item           $item   the role or permission that this rule is associated with
     * @param array          $params parameters passed to ManagerInterface::checkAccess().
     *
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->identity->getRole()->name === 'admin') {
            return true;
        }
        if(!isset($params['tournamentId'])){
            return false;
        }

        return Tournament::find()->hasNumber($params['tournamentId'])->withOwner($user)->exists();
    }
}