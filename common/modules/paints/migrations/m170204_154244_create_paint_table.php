<?php

use yii\db\Migration;

/**
 * Handles the creation of table `paint`.
 */
class m170204_154244_create_paint_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('paint', [
            'id'       => $this->primaryKey(),
            'type'     => $this->integer(11)->notNull(),
            'title'    => $this->string(32)->notNull(),
            'hex_code' => $this->string(8)->notNull(),
            'is_metal' => $this->boolean()->notNull()->defaultValue(false),
            'rgb_r'    => $this->smallInteger(),
            'rgb_g'    => $this->smallInteger(),
            'rgb_b'    => $this->smallInteger(),
            'hsv_h'    => $this->smallInteger(),
            'hsv_s'    => $this->smallInteger(),
            'hsv_v'    => $this->smallInteger(),
            'hsl_h'    => $this->smallInteger(),
            'hsl_s'    => $this->smallInteger(),
            'hsl_l'    => $this->smallInteger()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('paint');
    }
}
