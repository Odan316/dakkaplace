<?php

use yii\db\Migration;

/**
 * Class m190113_115209_permissions_paint
 */
class m190113_115209_permissions_paint extends Migration
{
    /**
     * @return bool|void
     * @throws Exception
     */
    public function up()
    {
        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('admin');

        $perm = $auth->createPermission('backend/paint/view');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $perm = $auth->createPermission('backend/paint/edit');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $auth = Yii::$app->authManager;

        $perm = $auth->getPermission('backend/paint/view');
        $auth->remove($perm);
        $perm = $auth->getPermission('backend/paint/edit');
        $auth->remove($perm);
    }
}
