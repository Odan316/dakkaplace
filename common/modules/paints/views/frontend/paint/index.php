<?php

/**
 * @var $this yii\web\View
 * @var $searchModel PaintSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use common\modules\paints\assets\PaintAsset;
use common\modules\paints\components\ColorHelper;
use common\modules\paints\models\search\PaintSearch;

$this->title = Yii::$app->name;

PaintAsset::register($this);
?>
<div class="site-index">
    <h1 class="text-center">Interactive color chart</h1>

    <h3 class="text-center">for Games Workshop Citadel Miniatures</h3>

    <?php echo $this->render('index/_search', [
            'model' => $searchModel
    ]); ?>

    <?php echo $this->render('index/_paints_chart', [ 'paints' => ColorHelper::sort($dataProvider->getModels()) ]) ?>

    <?php /*foreach (Paint::getTypes() as $typeId => $typeTitle) { ?>
        <?php echo $this->render('index/_paints_chart', [ 'paints' => ColorHelper::sort(Paint::find()->hasType($typeId)->all()) ]) ?>
    <?php } */?>

</div>
