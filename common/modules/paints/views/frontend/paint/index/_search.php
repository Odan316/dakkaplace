<?php

use common\modules\paints\models\Paint;
use common\modules\paints\models\search\PaintSearch;
use yii\bootstrap4\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model PaintSearch
 * @var $action string
 */
?>

<div class="paints-search">
    <h2><?php echo Yii::t('app/modules/paints', 'Filter') ?></h2>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-lg-2 col-md-3 col-sm-6 col-12">
            <?php echo $form->field($model, 'title') ?>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-6 col-12">
            <?php echo $form->field($model, 'type')->dropDownList(Paint::getTypes(), ['prompt' => '']) ?>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-6 col-12">
            <?php echo $form->field($model, 'colorGroup')->dropDownList(Paint::getColorGroups(), ['prompt' => '']) ?>
        </div>

        <div class="col-lg-2 col-md-3 col-sm-6 col-12">
            <?php echo $form->field($model, 'isMetallic')->dropDownList([
                1 => Yii::t('app/backend', 'No'),
                2 => Yii::t('app/backend', 'Yes')
            ], [ 'prompt' => '']) ?>
        </div>
        <?php echo Html::submitButton(Yii::t('app/backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app/backend', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


