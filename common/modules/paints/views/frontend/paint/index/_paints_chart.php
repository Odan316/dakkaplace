<?php
/**
 * @var $this yii\web\View
 * @var Paint[] $paints
 */

use common\modules\paints\models\Paint;


?>
<?php if (count($paints)) { ?>
    <div class="row">
        <!--<h3><?php echo !empty($paints) ? $paints[0]->typeName : '' ?>&nbsp;(<?php echo count($paints) ?>)</h3>-->
        <?php foreach ($paints as $paint) {
            $addClass = '';
            if ($paint->is_metal) {
                $addClass = 'metallic';
            }
            if (in_array($paint->type, [Paint::TYPE_SHADE, Paint::TYPE_GLAZE])) {
                $addClass = 'fleck';
            }

            ?>
            <div class="col-lg-2">
                <div class="paintCard">
                    <?php if (!($paint->hex_code == Paint::TRANSPARENT)) { ?>
                        <div class="paintPreview <?php echo $addClass ?>"
                             style="background-color: <?php echo $paint->hex_code ?>"></div>
                    <?php } else { ?>
                        <div class="paintPreview <?php echo $addClass ?>">Clear</div>
                    <?php } ?>
                    <h4 class="text-center <?php echo mb_strlen($paint->title) > 15 ? 'long' : '' ?>"><?php echo $paint->title ?></h4>
                    <?php if (YII_ENV_DEV && false) { ?>
                        <span class="label label-default"><?php echo round($paint->hsl_h) ?></span>
                        <span class="label label-default"><?php echo round($paint->hsl_s) ?></span>
                        <span class="label label-default"><?php echo round($paint->hsl_l) ?></span>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>