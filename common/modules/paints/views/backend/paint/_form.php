<?php

use common\modules\paints\models\Paint;
use yii\bootstrap4\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model Paint
 * @var $form yii\widgets\ActiveForm
 */
?>

<div class="paint-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'type')->dropDownList(Paint::getTypes()) ?>

    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'hex_code')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'is_metal')->checkbox() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord
            ? Yii::t('app', 'Create')
            : Yii::t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
