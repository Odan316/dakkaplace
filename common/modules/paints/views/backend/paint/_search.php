<?php

use common\modules\paints\models\search\PaintSearchAdmin;
use yii\bootstrap4\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model PaintSearchAdmin
 * @var $form yii\widgets\ActiveForm
 */
?>

<div class="paint-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>

    <?php echo $form->field($model, 'type') ?>

    <?php echo $form->field($model, 'title') ?>

    <?php echo $form->field($model, 'hex_code') ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
