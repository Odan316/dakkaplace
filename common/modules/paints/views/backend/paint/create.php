<?php

use common\modules\paints\models\Paint;
use yii\bootstrap4\Html;


/**
 * @var $this yii\web\View
 * @var $model Paint
 */

$this->title = Yii::t('app', 'Create Paint');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Paints'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paint-create">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
