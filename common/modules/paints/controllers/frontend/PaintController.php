<?php

namespace common\modules\paints\controllers\frontend;

use common\modules\paints\models\search\PaintSearch;
use Yii;
use yii\web\Controller;

/**
 * Class PaintController
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class PaintController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
        ];
    }

    /**
     * Displays paints chart.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PaintSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}