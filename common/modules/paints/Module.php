<?php

namespace common\modules\paints;

use Yii;

/**
 * tournament module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\paints\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $controllers = [];

        if (Yii::$app->id == 'app-backend') {
            $this->controllerNamespace .= '\backend';
            $this->viewPath .= '/backend';
            $controllers = [
                'paint',
            ];
        } elseif(Yii::$app->id == 'app-frontend') {
            $this->controllerNamespace .= '\frontend';
            $this->viewPath .= '/frontend';
            $controllers = [
                'paint'
            ];
        }

        $controllersList = implode('|', $controllers);
        Yii::$app->getUrlManager()->addRules([
            '<controller:(' . $controllersList . ')>'                   => 'paints/<controller>/index',
            '<controller:(' . $controllersList . ')>/<action>/<id:\d+>' => 'paints/<controller>/<action>',
            '<controller:(' . $controllersList . ')>/<action>'          => 'paints/<controller>/<action>',
        ], false);

    }
}
