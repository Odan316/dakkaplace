<?php
/**
 */

namespace common\modules\paints\assets;


use yii\web\AssetBundle;

/**
 * @author Andreev Sergey <si.andreev316@gmail.com>
 */
class PaintAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/paints/web';

    public $jsOptions = ['position' => \yii\web\View::POS_END];

    public $css = [
        'css/paint.css',
    ];
    public $js = [
    ];

    public $depends = [
        'frontend\assets\AppAsset'
    ];
}