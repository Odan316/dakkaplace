<?php

namespace common\modules\video\models;

use common\components\AdvStringHelper;
use common\modules\video\models\queries\PlaylistQuery;
use common\modules\video\models\queries\PlaylistVideoQuery;
use common\modules\video\models\queries\VideoQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "playlist".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $shortDesc
 * @property string $description
 * @property int $createdAt
 * @property int $updatedAt
 */
class Playlist extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'playlist';
    }

    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 128],
            [['slug'], 'string', 'max' => 32],
            [['shortDesc'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app/common', 'ID'),
            'title'       => Yii::t('app/modules/video', 'Title'),
            'slug'        => Yii::t('app/modules/video', 'Slug'),
            'shortDesc'   => Yii::t('app/modules/video', 'Short description'),
            'description' => Yii::t('app/modules/video', 'Description'),
            'createdAt'   => Yii::t('app/modules/video', 'Created at'),
            'updatedAt'   => Yii::t('app/modules/video', 'Updated at'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (empty($this->slug) && !empty($this->title)) {
            $this->slug = AdvStringHelper::slugify($this->title);

        }

        return parent::beforeValidate();
    }

    /**
     * {@inheritdoc}
     * @return PlaylistQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PlaylistQuery(get_called_class());
    }

    /**
     * @return ActiveQuery|PlaylistVideoQuery
     */
    public function getVideosList()
    {
        return $this->hasMany(PlaylistVideo::class, ['playlistId' => 'id']);
    }

    /**
     * @return ActiveQuery|VideoQuery
     * @throws InvalidConfigException
     */
    public function getVideos()
    {
        return $this->hasMany(Video::class, ['id' => 'videoId'])
            ->viaTable(PlaylistVideo::tableName(), ['playlistId' => 'id']);
    }
}
