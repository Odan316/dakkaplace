<?php

namespace common\modules\video\models\queries;

use common\modules\video\models\Playlist;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\video\models\Playlist]].
 *
 * @method Playlist[]|array all($db = null)
 * @method Playlist|array one($db = null)
 *
 * @see \common\modules\video\models\Playlist
 */
class PlaylistQuery extends ActiveQuery
{
    /**
     * @param int|int[] $id
     * @return PlaylistQuery
     */
    public function hasId(int $id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param string $slug
     *
     * @return PlaylistQuery
     */
    public function hasSlug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }

}
