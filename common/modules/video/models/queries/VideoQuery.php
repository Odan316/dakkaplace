<?php

namespace common\modules\video\models\queries;

use common\modules\video\models\Video;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Video]].
 *
 * @method Video[]|array all($db = null)
 * @method Video|array one($db = null)
 *
 * @see Video
 */
class VideoQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return VideoQuery
     */
    public function hasId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param string $slug
     *
     * @return VideoQuery
     */
    public function hasSlug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }

    /**
     * @return VideoQuery
     */
    public function published()
    {
        return $this->andWhere(['visibility' => [Video::VIS_VIP, Video::VIS_PUBLIC]]);
    }

    /**
     * @return VideoQuery
     */
    public function onlyPublic()
    {
        return $this->andWhere(['visibility' => Video::VIS_PUBLIC]);
    }

    /**
     * @return VideoQuery
     */
    public function notPublished()
    {
        return $this->andWhere(['visibility' => Video::VIS_NOT_PUBLISHED]);
    }

    /**
     * @return VideoQuery
     */
    public function onlyVip()
    {
        return $this->andWhere(['visibility' => Video::VIS_VIP]);
    }

    /**
     * @return VideoQuery
     */
    public function orderByPublish()
    {
        return $this->orderBy(['publishedAt' => SORT_DESC]);
    }
}
