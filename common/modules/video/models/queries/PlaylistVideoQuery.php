<?php

namespace common\modules\video\models\queries;

use common\modules\video\models\PlaylistVideo;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\video\models\PlaylistVideo]].
 *
 * @method PlaylistVideo[]|array all($db = null)
 * @method PlaylistVideo|array one($db = null)
 *
 * @see \common\modules\video\models\PlaylistVideo
 */
class PlaylistVideoQuery extends ActiveQuery
{
    /**
     * @param int $videoId
     * @param int $playlistId
     * @return PlaylistVideoQuery
     */
    public function exact(int $videoId, int $playlistId)
    {
        return $this->andWhere(['videoId' => $videoId, 'playlistId' => $playlistId]);
    }

    /**
     * @param $playlistId
     * @return PlaylistVideoQuery
     */
    public function forPlaylist($playlistId)
    {
        return $this->andWhere(['playlistId' => $playlistId]);
    }
}
