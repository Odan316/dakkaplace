<?php

namespace common\modules\video\models;

use common\modules\video\models\queries\PlaylistVideoQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "playlist_video".
 *
 * @property int $playlistId
 * @property int $videoId
 * @property int $sort
 *
 * @property Video $video
 * @property Playlist $playlist
 */
class PlaylistVideo extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'playlist_video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['playlistId', 'videoId', 'sort'], 'required'],
            [['playlistId', 'videoId', 'sort'], 'integer'],
            [['playlistId', 'videoId'], 'unique', 'targetAttribute' => ['playlistId', 'videoId']],
            [
                ['videoId'],
                'exist',
                'targetClass'     => Video::class,
                'targetAttribute' => ['videoId' => 'id']
            ],
            [
                ['playlistId'],
                'exist',
                'targetClass'     => Playlist::class,
                'targetAttribute' => ['playlistId' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'playlistId' => Yii::t('app/modules/video', 'Playlist'),
            'videoId'    => Yii::t('app/modules/video', 'Video'),
            'sort'       => Yii::t('app/modules/video', 'Sort'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getVideo()
    {
        return $this->hasOne(Video::class, ['id' => 'videoId']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPlaylist()
    {
        return $this->hasOne(Playlist::class, ['id' => 'playlistId']);
    }

    /**
     * {@inheritdoc}
     * @return PlaylistVideoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PlaylistVideoQuery(get_called_class());
    }
}
