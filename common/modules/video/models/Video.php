<?php

namespace common\modules\video\models;

use common\components\AdvStringHelper;
use common\modules\video\models\queries\PlaylistQuery;
use common\modules\video\models\queries\VideoQuery;
use Yii;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\bootstrap\Html;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "video".
 *
 * @property int $id
 * @property int $createdAt
 * @property int $updatedAt
 * @property int $publishedAt
 * @property string $shortDesc
 * @property string $title
 * @property string $slug
 * @property string $link
 * @property string $description
 * @property int $visibility
 *
 * @property string $visibilityTitle
 * @property Playlist[] $playlists
 */
class Video extends ActiveRecord
{
    const VIS_NOT_PUBLISHED = 0;
    const VIS_VIP = 10;
    const VIS_PUBLIC = 20;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'video';
    }

    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'link', 'slug'], 'trim'],
            [['title', 'link'], 'required'],
            [['description'], 'string'],
            [['visibility', 'publishedAt'], 'integer'],
            [['visibility'], 'in', 'range' => array_keys(self::getVisibilities())],
            [['title', 'link'], 'string', 'max' => 64],
            [['shortDesc'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 32],
            [
                ['slug'],
                'match',
                'pattern' => '/^[a-z0-9_-]*$/i',
                'message' => Yii::t('app/common', 'You should use only latin characters and symbols "-" and "_"')
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('app/common', 'ID'),
            'title'           => Yii::t('app/modules/video', 'Title'),
            'link'            => Yii::t('app/modules/video', 'Link'),
            'slug'            => Yii::t('app/modules/video', 'Slug'),
            'shortDesc'       => Yii::t('app/modules/video', 'Short description'),
            'description'     => Yii::t('app/modules/video', 'Description'),
            'visibility'      => Yii::t('app/modules/video', 'Visibility'),
            'publishedAt'     => Yii::t('app/modules/video', 'Published at'),
            'createdAt'       => Yii::t('app/modules/video', 'Created at'),
            'updatedAt'       => Yii::t('app/modules/video', 'Updated at'),
            'visibilityTitle' => Yii::t('app/modules/video', 'Visibility'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (empty($this->slug) && !empty($this->title)) {
            $this->slug = AdvStringHelper::slugify($this->title);

        }

        return parent::beforeValidate();
    }

    /**
     * {@inheritdoc}
     * @return VideoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VideoQuery(get_called_class());
    }

    /**
     * @return string[]
     */
    public static function getVisibilities()
    {
        return [
            self::VIS_NOT_PUBLISHED => Yii::t('app/modules/video', 'Not published'),
            self::VIS_VIP           => Yii::t('app/modules/video', 'Only subscribers'),
            self::VIS_PUBLIC        => Yii::t('app/modules/video', 'Public')
        ];
    }

    /**
     * @return string
     */
    public function getVisibilityTitle()
    {
        return self::getVisibilities()[$this->visibility];
    }

    /**
     * @return boolean
     */
    public function isNotPublished()
    {
        return $this->visibility == self::VIS_NOT_PUBLISHED;
    }

    /**
     * @return boolean
     */
    public function isPrivate()
    {
        return $this->visibility == self::VIS_VIP;
    }

    /**
     * @return bool
     */
    public function isPublic()
    {
        return $this->visibility == self::VIS_PUBLIC;
    }

    /**
     * @param int $width
     * @param int $height
     * @return string
     */
    public function getEmbedded($width = 560, $height = 315)
    {
        $videoId = $this->getVideoID();

        return Html::tag('iframe', '', [
            'width'           => $width,
            'height'          => $height,
            'src'             => 'https://www.youtube.com/embed/' . $videoId,
            'frameborder'     => 0,
            'allow'           => 'autoplay; encrypted-media',
            'allowfullscreen' => '1'
        ]);
    }

    public function getVideoThumbs()
    {
        $videoId = $this->getVideoID();
        $urlBase = "https://img.youtube.com/vi/{$videoId}";

        return [
            "{$urlBase}/0.jpg",
            "{$urlBase}/1.jpg",
            "{$urlBase}/2.jpg",
            "{$urlBase}/3.jpg",
            "{$urlBase}/hqdefault.jpg",
            "{$urlBase}/mqdefault.jpg",
            "{$urlBase}/maxresdefault.jpg",
            "{$urlBase}/sddefault.jpg",
        ];
    }

    /**
     * @return string
     */
    public function getVideoID()
    {
        $matches = [];
        $regexp1 = '/youtu.be\/(.*)$/';
        $regexp2 = '/watch\?v=(.*)$/';
        preg_match($regexp1, $this->link, $matches);
        if (empty($matches)) {
            preg_match($regexp2, $this->link, $matches);
        }

        if (empty($matches)) {
            return "";
        }

        return $matches[1];
    }

    /**
     * @return ActiveQuery|PlaylistQuery
     * @throws InvalidConfigException
     */
    public function getPlaylists()
    {
        return $this->hasMany(Playlist::class, ['id' => 'playlistId'])
            ->viaTable(PlaylistVideo::tableName(), ['videoId' => 'id']);
    }
}
