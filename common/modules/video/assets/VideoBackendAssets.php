<?php


namespace common\modules\video\assets;


use common\components\CustomAssetBundle;
use yii\web\View;

class VideoBackendAssets extends CustomAssetBundle
{
    public $sourcePath = '@common/modules/video/web';

    public $jsOptions = ['position' => View::POS_END];

    public $css = [
    ];
    public $js = [
        'js/video.js',
    ];

    public $depends = [
        'backend\assets\AppAsset'
    ];

}