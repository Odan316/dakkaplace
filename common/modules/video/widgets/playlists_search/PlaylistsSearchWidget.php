<?php

namespace common\modules\video\widgets\playlists_search;

use yii\base\Widget;

/**
 * Class PlaylistsSearchWidget
 */
class PlaylistsSearchWidget extends Widget
{
    public $video;
    public $viewName;
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        if($this->viewName === 'modal'){
            return $this->render('modal');
        }

        return $this->render('default', [
            'video' => $this->video
        ]);
    }

}