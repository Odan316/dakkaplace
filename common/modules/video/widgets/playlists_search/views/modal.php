<?php /** @noinspection PhpUnhandledExceptionInspection */

/**
 * @var $this View
 */

use common\modules\video\widgets\playlists_search\PlaylistsSearchAsset;
use common\modules\video\models\Playlist;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\web\View;

PlaylistsSearchAsset::register($this);
?>

<?php Modal::begin([
    'id'      => 'playlistsModal',
    'title'   => Yii::t('app/modules/video/backend', 'Choose playlists'),
    'options' => [
        'data-pjax-id' => '0'
    ]
]); ?>
<div class="playlistsModalInner">
</div>
<?php Modal::end(); ?>
