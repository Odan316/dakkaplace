<?php /** @noinspection PhpUnhandledExceptionInspection */

/**
 * @var $this View
 * @var $video Video
 */

use common\modules\video\models\Video;
use common\modules\video\widgets\playlists_search\PlaylistsSearchAsset;
use common\modules\video\models\Playlist;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

PlaylistsSearchAsset::register($this);

$selectedPlaylists = ArrayHelper::map($video->playlists, 'id', 'id');
?>

<div class="form-group playlistsInput">
    <?php echo Html::checkboxList('videoPlaylists',
        $selectedPlaylists, ArrayHelper::map(Playlist::find()->all(), 'id', 'title'))?>
</div>
<div class="form-group">
    <?php echo Html::button(Yii::t('app/common', 'Save'), ['class' => 'btn btn-success savePlaylists']); ?>
</div>
