<?php

namespace common\modules\video\widgets\playlists_search;


use common\components\CustomAssetBundle;

class PlaylistsSearchAsset extends CustomAssetBundle
{
    public $sourcePath = '@modules/video/widgets/playlists_search/assets/';
    public $baseUrl = '@web/assets';

    public $css = [
    ];
    public $js = [
        'js/playlists_search.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'backend\assets\AppAsset'
    ];
}