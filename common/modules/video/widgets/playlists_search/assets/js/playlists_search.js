$(function(){
    "use strict";
    $(document).on('click', '.addToPlaylists', function(){
        let videoId = $(this).parents('tr').data('key');
        $.ajax({
            type: "POST",
            url: "/playlist/get-list-widget/" + videoId,
            success: function (data) {
                $('.playlistsModalInner').html(data).data('video-id', videoId);
                $('#playlistsModal').modal('show');
            }
        });
    });

    $(document).on('click', '.savePlaylists', function(){
        let $modalInner = $('.playlistsModalInner');
        let videoId = $modalInner.data('video-id');
        let playlistsIds = [];
        $('.playlistsInput input:checked').each(function() {
            playlistsIds.push($(this).attr('value'));
        });
        $(document).trigger('playlist_search_choose', {'videoId': videoId, 'playlistsIds': playlistsIds});
        $('#playlistsModal').modal('hide');
        $modalInner.html("");
    });
});