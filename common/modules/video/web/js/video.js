$(function () {
    $(document).on('playlist_search_choose', function (e, data) {

        $.ajax({
            type: "POST",
            url: "/video/add-to-playlists/" + data.videoId,
            data: {
                playlistsIds: data.playlistsIds
            },
            success: function (data) {
                if (data.result) {
                    bootbox.alert({
                        title: "Успешно!",
                        message: "Изменения сохранены."
                    });
                } else {
                    // TODO: Add details, use message from server
                    bootbox.alert({
                        title: "Ошибка!",
                        message: "Во время сохранения возникла ошибка! \r\n Сообщите администратору это число: " + Date.now()
                    });
                }
            }
        });
    });

    $(document).on('click', '.removeVideo', function (e) {
        e.preventDefault();
        let link = $(this).attr('href');
        let videoId = $(this).data('video-id');
        bootbox.confirm("Вы уверены, что хотите удалить видео из плейлиста?", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: link,
                    data:{
                        videoId : videoId
                    },
                    success: function (data) {
                        if (data.result) {
                            bootbox.alert({
                                title: "Успешно!",
                                message: "Видео удалено из плейлиста"
                            });

                            $.pjax.reload({
                                container: "#playlistVideosPjax",
                                timeout: 5000
                            });
                        } else {
                            // TODO: Add details, use message from server
                            bootbox.alert({
                                title: "Ошибка!",
                                message: "Во время удаления возникла ошибка! \r\n Сообщите администратору это число: " + Date.now()
                            });
                        }
                    }
                });
            }
        });

    });
});