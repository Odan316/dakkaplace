<?php /** @noinspection PhpUnhandledExceptionInspection */

use common\modules\video\models\Video;
use yii\bootstrap4\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model Video
 */


$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/modules/video/frontend', "Videos"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-view">



    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <?php echo $model->getEmbedded(); ?>
            </div>
            <div class="col-md-12">
                <h1><?php echo Html::encode($this->title) ?></h1>
                <span><?php echo date('d.m.Y H:i', $model->publishedAt) ?></span>
                <p><?php echo $model->description?> </p>
            </div>
        </div>
    </div>

</div>
