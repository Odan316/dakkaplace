<?php

use common\modules\video\models\Playlist;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;

/**
 * @var $this yii\web\View
 */

$this->title = Yii::t('app/modules/video/frontend', 'All playlists');
$this->params['breadcrumbs'][] = Yii::t('app/modules/video/frontend', "Playlists");
?>
<div class="video-index">

    <h1><?php echo Yii::t('app/modules/video/frontend', 'All playlists') ?></h1>

    <div class="row">
    <?php
    $query = Playlist::find();

    /** @var Playlist $model */
    $dataPovider = new ActiveDataProvider([
        'query' => $query->orderBy(['updatedAt' => SORT_DESC]),
        'pagination' => [
            'pageSize' => 20
        ]
    ]);
    foreach ($dataPovider->getModels() as $model) { ?>
        <div class="col-md-12 mb-3">
            <div class="row">
                <div class="col-md-6">
                    <?php //echo $model->getEmbedded(); ?>
                </div>
                <div class="col-md-6">
                    <h3><?php echo Html::a($model->title, ['/video/playlist/view', 'slug' => $model->slug]); ?></h3>
                    <span><?php echo date('d.m.Y H:i', $model->updatedAt); ?></span>
                    <p><?php echo $model->description; ?> </p>
                </div>
            </div>
        </div>
    <?php }?>
    </div>

</div>