<?php /** @noinspection PhpUnhandledExceptionInspection */

use common\modules\video\models\Playlist;
use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $model Playlist
 */


$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/modules/video/frontend', "Playlists"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo Html::encode($this->title) ?></h1>
                <span><?php echo date('d.m.Y H:i', $model->updatedAt) ?></span>
                <p><?php echo $model->description; ?> </p>
            </div>
        </div>
    </div>

</div>
