<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\components\AdminGridView;
use common\modules\video\widgets\playlists_search\PlaylistsSearchWidget;
use common\modules\video\assets\VideoBackendAssets;
use common\modules\video\models\Video;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;

/**
 * @var $this yii\web\View
 */
VideoBackendAssets::register($this);

$this->title = Yii::t('app/modules/video/backend', 'Videos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-index">

    <h1><?php echo Yii::t('app/modules/video/backend', 'Videos') ?></h1>

    <div class="form-group">
        <?php if (Yii::$app->user->can('backend/videos/edit')) { ?>
            <?php echo Html::a(Yii::t('app/common', 'Add'),
                ['create'], ['class' => 'btn btn-primary']) ?>
        <?php } ?>
    </div>


    <?php echo AdminGridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'      => Video::find(),
            'pagination' => [
                'pageSize' => 50
            ],
            'sort'       => ['defaultOrder' => ['createdAt' => SORT_DESC]]
        ]),
        'emptyText'    => Yii::t('app/modules/video/backend', 'No videos yet!'),
        'columns'      => [
            [
                'attribute' => 'title',
                'format'    => 'raw',
                'value'     => function ($model) {
                    /** @var $model Video */
                    return Html::a($model->title, ['view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'visibility',
                'value'     => 'visibilityTitle'
            ],
            [
                'attribute' => 'createdAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{edit} {playlist}',
                'buttons'       => [
                    'edit'   => function ($url, $model, $key) {
                        if (Yii::$app->user->can('backend/videos/edit')) {
                            /** @var $model Video */
                            return Html::a('<i class="fas fa-pencil-alt"></i>',
                                ['update', 'id' => $model->id]);
                        }
                        return '';
                    },
                    'playlist'   => function ($url, $model, $key) {
                        if (Yii::$app->user->can('backend/videos/edit')) {
                            /** @var $model Video */
                            return Html::button('<i class="fas fa-tasks text-success"></i>', [
                                    'class' => 'btn btn-link btn-sm addToPlaylists',
                                    'title' => Yii::t('app/modules/video/backend', 'Playlists')
                            ]);
                        }
                        return '';
                    }
                ]
            ]
        ]
    ]); ?>

</div>

<?php echo PlaylistsSearchWidget::widget(['viewName' => 'modal']); ?>

