<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\components\AdminDetailView;
use common\modules\video\models\Video;
use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $model Video
 */


$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/modules/video/backend', "Videos"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-view">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?php if (Yii::$app->user->can('backend/videos/edit')) { ?>
            <?php echo Html::a(Yii::t('app/common', "Edit"), ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary']) ?>

            <?php if ($model->isNotPublished()) { ?>
                <?php echo Html::a(Yii::t('app/modules/video/backend', "Publish"), ['publish', 'id' => $model->id],
                    ['class' => 'btn btn-warning']) ?>
            <?php } ?>

            <?php if ($model->isPrivate()) { ?>
                <?php echo Html::a(Yii::t('app/modules/video/backend', "Make public"), ['make-public', 'id' => $model->id],
                    ['class' => 'btn btn-warning']) ?>
            <?php } ?>


            <?php if ($model->isPublic() || $model->isPrivate()) { ?>
                <?php echo Html::a(Yii::t('app/modules/video/backend', "Hide"), ['hide', 'id' => $model->id],
                    ['class' => 'btn btn-danger']) ?>
            <?php } ?>
        <?php } ?>
    </div>

    <?php echo AdminDetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'slug',
                'format'    => 'html',
                'value'     => function ($model) {
                    /** @var Video $model */
                    $url = Yii::$app->urlManagerFront->createAbsoluteUrl(['/video/video/view', 'slug' => $model->slug]);
                    $html = Html::a($url, $url);
                    return $html;
                }
            ],
            'link',
            'shortDesc',
            [
                'attribute' => 'description',
                'format'    => 'html',
            ],
            'visibilityTitle',
            [
                'attribute' => 'createdAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ],
            [
                'attribute' => 'updatedAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ],
            [
                'attribute' => 'publishedAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ]
        ],
    ]) ?>

</div>
