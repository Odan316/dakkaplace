<?php

use common\modules\video\models\Video;
use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $model Video
 */

$this->title = Yii::t('app/modules/video/backend', 'Edit video');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/modules/video/backend', "Videos"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-update">

    <h1><?php echo Html::encode($this->title)?></h1>

    <?php echo $this->render('_form', [
        'model' => $model
    ]) ?>
</div>
