<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\components\AdminDetailView;
use backend\components\AdminGridView;
use common\modules\video\assets\VideoBackendAssets;
use common\modules\video\models\Playlist;
use common\modules\video\models\PlaylistVideo;
use common\modules\video\models\Video;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $model Playlist
 */


$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/modules/video/backend', "Playlists"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

VideoBackendAssets::register($this);
?>
<div class="page-view">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?php if (Yii::$app->user->can('backend/videos/edit')) { ?>
            <?php echo Html::a(Yii::t('app/common', "Edit"), ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary']) ?>
        <?php } ?>
    </div>

    <?php echo AdminDetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'slug',
                'format'    => 'html',
                'value'     => function ($model) {
                    /** @var Playlist $model */
                    $url = Yii::$app->urlManagerFront->createAbsoluteUrl([
                        '/video/playlist/view',
                        'slug' => $model->slug
                    ]);
                    $html = Html::a($url, $url);
                    return $html;
                }
            ],
            'shortDesc',
            [
                'attribute' => 'description',
                'format'    => 'html',
            ],
            [
                'attribute' => 'createdAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ],
            [
                'attribute' => 'updatedAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ]
        ],
    ]) ?>

    <h2><?php echo Yii::t('app/modules/video/backend', "Videos"); ?>
        <?php if (Yii::$app->user->can('backend/videos/edit')) { ?>
            <?php echo Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                ['/video/playlist/remove-video', 'id' => $model->id]); ?>
        <?php } ?></h2>

    <?php Pjax::begin(['id'      => 'playlistVideosPjax']); ?>
    <?php echo AdminGridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'      => $model->getVideosList(),
            'pagination' => [
                'pageSize' => 50
            ],
            'sort'       => ['defaultOrder' => ['sort' => SORT_ASC]]
        ]),
        'emptyText'    => Yii::t('app/modules/video/backend', 'No videos yet!'),
        'columns'      => [
            [
                'attribute' => 'sort'
            ],
            [
                'attribute' => 'videoId',
                'format'    => 'raw',
                'value'     => function ($model) {
                    /** @var $model PlaylistVideo */
                    return Html::a($model->video->title, ['view', 'id' => $model->videoId]);
                }
            ],
            [
                'attribute' => 'video.visibility',
                'value'     => 'video.visibilityTitle'
            ],
            [
                'attribute' => 'video.createdAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{remove}',
                'buttons'  => [
                    'remove' => function ($url, $model, $key) {
                        if (Yii::$app->user->can('backend/videos/edit')) {
                            /** @var $model PlaylistVideo */
                            return Html::a('<i class="far fa-trash-alt text-danger"></i>',
                                ['/video/playlist/remove-video', 'id' => $model->playlistId],
                                ['class' => 'removeVideo', 'data-video-id' => $model->videoId]);
                        }
                        return '';
                    }
                ]
            ]
        ]
    ]); ?>

    <?php Pjax::end(); ?>
</div>
