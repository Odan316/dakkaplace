<?php

use common\modules\video\models\Playlist;
use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $model Playlist
 */

$this->title = Yii::t('app/modules/video/backend', 'Edit playlist');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/modules/video/backend', "Playlists"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-update">

    <h1><?php echo Html::encode($this->title)?></h1>

    <?php echo $this->render('_form', [
        'model' => $model
    ]) ?>
</div>
