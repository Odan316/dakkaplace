<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\components\AdminGridView;
use common\modules\video\models\Playlist;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;

/**
 * @var $this yii\web\View
 */

$this->title = Yii::t('app/modules/video/backend', 'Playlists');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?php echo Yii::t('app/modules/video/backend', 'Playlists') ?></h1>

    <div class="form-group">
        <?php if (Yii::$app->user->can('backend/videos/edit')) { ?>
            <?php echo Html::a(Yii::t('app/common', 'Add'),
                ['create'], ['class' => 'btn btn-primary']) ?>
        <?php } ?>
    </div>


    <?php echo AdminGridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'      => Playlist::find(),
            'pagination' => [
                'pageSize' => 50
            ],
            'sort'       => ['defaultOrder' => ['createdAt' => SORT_DESC]]
        ]),
        'emptyText'    => Yii::t('app/modules/video/backend', 'No Playlists yet!'),
        'columns'      => [
            [
                'attribute' => 'title',
                'format'    => 'raw',
                'value'     => function ($model) {
                    /** @var $model Playlist */
                    return Html::a($model->title, ['view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'createdAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view} {edit}',
                'buttons'       => [
                    'edit'   => function ($url, $model, $key) {
                        if (Yii::$app->user->can('backend/videos/edit')) {
                            /** @var $model Playlist */
                            return Html::a('<i class="fas fa-pencil-alt"></i>',
                                ['update', 'id' => $model->id]);
                        }
                        return '';
                    }
                ]
            ]
        ]
    ]); ?>

</div>
