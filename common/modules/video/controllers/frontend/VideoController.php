<?php
namespace common\modules\video\controllers\frontend;

use common\components\controllers\WebController;
use common\modules\video\models\Video;

/**
 * Site controller
 */
class VideoController extends WebController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
        ];
    }


    /**
     * Displays list of public videos.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }

    public function actionView($slug)
    {
        $model = Video::find()->hasSlug($slug)->one();

        return $this->render('view', [
            'model' => $model
        ]);
    }

}
