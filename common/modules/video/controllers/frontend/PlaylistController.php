<?php
namespace common\modules\video\controllers\frontend;

use common\components\controllers\WebController;
use common\modules\video\models\Playlist;

/**
 * Playlist controller
 */
class PlaylistController extends WebController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
        ];
    }


    /**
     * Displays list of public videos.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }

    public function actionView($slug)
    {
        $model = Playlist::find()->hasSlug($slug)->one();

        return $this->render('view', [
            'model' => $model
        ]);
    }

}
