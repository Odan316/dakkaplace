<?php

namespace common\modules\video\controllers\backend;

use common\components\controllers\WebController;
use common\modules\video\models\Playlist;
use common\modules\video\models\PlaylistVideo;
use common\modules\video\models\Video;
use common\modules\video\widgets\playlists_search\PlaylistsSearchWidget;
use Exception;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Playlist CRUD controller
 *
 * @see Video
 */
class PlaylistController extends WebController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['backend/videos/view']
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['update', 'create', 'get-list-widget', 'remove-video'],
                        'roles'   => ['backend/videos/edit']
                    ]
                ]
            ]
        ];
    }

    /**
     * Displays list of videos.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }

    public function actionView($id)
    {
        $model = Playlist::findOne($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionCreate()
    {
        $model = new Playlist();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save();
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Playlist::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionGetListWidget($id)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_HTML;

            $video = Video::find()->hasId($id)->one();

            return PlaylistsSearchWidget::widget(['video' => $video]);

        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @param $id
     * @return array|Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionRemoveVideo($id)
    {
        $videoId = Yii::$app->request->post('videoId', 0);

        $link = PlaylistVideo::find()->exact($videoId, $id)->one();

        $result = false;
        if(!empty($link)){
            $result = $link->delete();
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['result' => $result];
        } else {
            return $this->redirect(['/video/playlist/view', 'id' => $id]);
        }
    }
}
