<?php

namespace common\modules\video\controllers\backend;

use common\components\controllers\WebController;
use common\modules\video\models\Playlist;
use common\modules\video\models\PlaylistVideo;
use common\modules\video\models\Video;
use Throwable;
use Yii;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Video CRUD controller
 *
 * @see Video
 */
class VideoController extends WebController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['backend/videos/view']
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['update', 'create', 'publish', 'make-public', 'hide', 'add-to-playlists'],
                        'roles'   => ['backend/videos/edit']
                    ]
                ]
            ]
        ];
    }

    /**
     * Displays list of videos.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }

    public function actionView($id)
    {
        $model = Video::findOne($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionCreate()
    {
        $model = new Video();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save();
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Video::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionPublish($id)
    {
        $model = Video::find()->hasId($id)->notPublished()->one();

        if (!empty($model)) {
            $model->publishedAt = time();
            $model->visibility = Video::VIS_VIP;
            $model->save();
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionMakePublic($id)
    {
        $model = Video::find()->hasId($id)->onlyVip()->one();

        if (!empty($model)) {
            $model->visibility = Video::VIS_PUBLIC;
            $model->save();
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionHide($id)
    {
        $model = Video::find()->hasId($id)->one();

        if (!empty($model)) {
            $model->visibility = Video::VIS_NOT_PUBLISHED;
            $model->publishedAt = null;
            $model->save();
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @param $id
     * @return array
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionAddToPlaylists($id)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = false;

            $video = Video::find()->hasId($id)->one();

            if (!empty($video)) {
                $result = true;
                $lastingIds = Yii::$app->request->post('playlistsIds', []);
                $currentIds = ArrayHelper::getColumn($video->playlists, 'id');

                $toAdd = array_diff($lastingIds, $currentIds);
                $toRemove = array_diff($currentIds, $lastingIds);

                foreach ($toAdd as $playlistId) {
                    $model = new PlaylistVideo();
                    $model->sort = PlaylistVideo::find()->forPlaylist($playlistId)->count() + 1;
                    $model->videoId = $video->id;
                    $model->playlistId = $playlistId;
                    $result = $result && $model->save();
                }

                foreach ($toRemove as $playlistId) {
                    $model = PlaylistVideo::find()->exact($video->id, $playlistId)->one();
                    $result = $result && $model->delete();
                }
            }

            return ['result' => $result];

        } else {
            throw new NotFoundHttpException();
        }
    }

}
