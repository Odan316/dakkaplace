<?php

namespace common\modules\video;

use Yii;

/**
 * tournament module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\video\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        Yii::setAlias('@video', '@common/modules/video');

        Yii::$app->i18n->translations['app/modules/video*'] = [
            'class'          => 'yii\i18n\GettextMessageSource',
            'basePath'       => '@video/messages',
            'catalog'        => 'video',
            'sourceLanguage' => 'en'
        ];

        if (Yii::$app->id == 'app-backend') {
            $this->controllerNamespace .= '\backend';
            $this->viewPath .= '/backend';
            $controllers = [
                'video',
                'playlist'
            ];

            $controllersList = implode('|', $controllers);
            Yii::$app->getUrlManager()->addRules([
                '<controller:(' . $controllersList . ')>'                   => 'video/<controller>/index',
                '<controller:(' . $controllersList . ')>/<action>/<id:\d+>' => 'video/<controller>/<action>',
                '<controller:(' . $controllersList . ')>/<action>'          => 'video/<controller>/<action>',
            ], false);
            Yii::$app->urlManagerFront->addRules([
                '<controller:(' . $controllersList . ')>'                   => 'video/<controller>/index',
                '/video/<slug:(\w|\d|\/|-|_)+>'                             => 'video/video/view',
                '/playlist/<slug:(\w|\d|\/|-|_)+>'                             => 'video/playlist/view',
                '<controller:(' . $controllersList . ')>/<action>'          => 'video/<controller>/<action>',
            ], false);

        } elseif (Yii::$app->id == 'app-frontend') {
            $this->controllerNamespace .= '\frontend';
            $this->viewPath .= '/frontend';
            $controllers = [
                'video',
                'playlist'
            ];

            $controllersList = implode('|', $controllers);
            Yii::$app->getUrlManager()->addRules([
                '<controller:(' . $controllersList . ')>'                   => 'video/<controller>/index',
                '/video/<slug:(\w|\d|\/|-|_)+>'                             => 'video/video/view',
                '/playlist/<slug:(\w|\d|\/|-|_)+>'                             => 'video/playlist/view',
                '<controller:(' . $controllersList . ')>/<action>'          => 'video/<controller>/<action>',
            ], false);
        }


    }
}
