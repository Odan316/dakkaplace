<?php

use console\components\SchemaHelper;

/**
 */
class m000002_000004_playlists_videos extends \yii\db\Migration
{
    const NOT_PUBLISHED = 0;

    /**
     * @return bool|void
     * @throws Exception
     */
    public function up()
    {
        $tableOptions = SchemaHelper::getTableOptions($this->db->driverName);

        $this->createTable('playlist_video', [
            'playlistId' => $this->integer()->notNull(),
            'videoId'    => $this->integer()->notNull(),
            'sort'       => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PK_playlist_video', 'playlist_video', ['playlistId', 'videoId']);

        $this->addForeignKey('FK_playlist_video',
            'playlist_video', 'videoId',
            'video', 'id',
            'CASCADE', 'CASCADE');

        $this->addForeignKey('FK_video_playlist',
            'playlist_video', 'playlistId',
            'playlist', 'id',
            'CASCADE', 'CASCADE');
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $this->dropForeignKey('FK_playlist_video', 'playlist_video');
        $this->dropForeignKey('FK_video_playlist', 'playlist_video');

        $this->dropTable('playlist_video');
    }
}
