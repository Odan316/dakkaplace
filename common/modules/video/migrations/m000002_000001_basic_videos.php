<?php

use console\components\SchemaHelper;

/**
 */
class m000002_000001_basic_videos extends \yii\db\Migration
{
    const NOT_PUBLISHED = 0;

    /**
     * @return bool|void
     * @throws Exception
     */
    public function up()
    {
        $tableOptions = SchemaHelper::getTableOptions($this->db->driverName);

        $this->createTable('video', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string(128)->notNull(),
            'slug'        => $this->string(32)->notNull(),
            'link'        => $this->string(128)->notNull(),
            'shortDesc'   => $this->string(255),
            'description' => $this->text(),
            'visibility'  => $this->integer()->notNull()->defaultValue(self::NOT_PUBLISHED),
            'publishedAt' => $this->integer(),
            'createdAt'   => $this->integer()->notNull(),
            'updatedAt'   => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $this->dropTable('video');
    }
}
