<?php

use console\components\SchemaHelper;

/**
 */
class m000002_000003_playlists extends \yii\db\Migration
{
    const NOT_PUBLISHED = 0;

    /**
     * @return bool|void
     * @throws Exception
     */
    public function up()
    {
        $tableOptions = SchemaHelper::getTableOptions($this->db->driverName);

        $this->createTable('playlist', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string(128)->notNull(),
            'slug'        => $this->string(32)->notNull(),
            'shortDesc'   => $this->string(255),
            'description' => $this->text(),
            'createdAt'   => $this->integer()->notNull(),
            'updatedAt'   => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $this->dropTable('playlist');
    }
}
