<?php

use common\components\rbac\VideoViewPrivateRule;
use yii\db\Migration;

/**
 */
class m000002_000002_permissions_video extends Migration
{
    /**
     * @return bool|void
     * @throws Exception
     */
    public function up()
    {
        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('admin');
        $vipRole = $auth->getRole('vip_customer');

        $perm = $auth->createPermission('backend/videos/view');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $perm = $auth->createPermission('backend/videos/edit');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);


        $rule = new VideoViewPrivateRule();
        $auth->add($rule);
        $perm = $auth->createPermission('frontend/videos/viewPrivate');
        $perm->ruleName = $rule->name;
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $auth->addChild($vipRole, $perm);

    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $auth = Yii::$app->authManager;

        $perm = $auth->getPermission('backend/videos/view');
        $auth->remove($perm);
        $perm = $auth->getPermission('backend/videos/edit');
        $auth->remove($perm);

        $perm = $auth->getPermission('frontend/videos/viewPrivate');
        $auth->remove($perm);
        $rule = new VideoViewPrivateRule();
        $auth->remove($rule);
    }
}
