<?php

namespace common\modules\community\controllers\backend;

use common\components\controllers\WebController;
use common\modules\community\models\News;
use Yii;
use yii\filters\AccessControl;

/**
 * News controller
 *
 * @see News
 */
class NewsController extends WebController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['backend/news/view']
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['update', 'create', 'publish', 'hide'],
                        'roles'   => ['backend/news/edit']
                    ]
                ]
            ]
        ];
    }

    /**
     * Displays list of videos.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }

    public function actionView($id)
    {
        $model = News::findOne($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save();
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = News::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionPublish($id)
    {
        $model = News::find()->hasId($id)->notPublished()->one();

        if (!empty($model)) {
            $model->publishedAt = time();
            $model->visibility = News::VIS_PUBLIC;
            $model->save();
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionHide($id)
    {
        $model = News::find()->hasId($id)->one();

        if (!empty($model)) {
            $model->visibility = News::VIS_NOT_PUBLISHED;
            $model->publishedAt = null;
            $model->save();
        }

        return $this->redirect(['view', 'id' => $id]);
    }

}
