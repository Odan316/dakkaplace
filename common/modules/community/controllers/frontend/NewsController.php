<?php
namespace common\modules\community\controllers\frontend;

use common\components\controllers\WebController;
use common\modules\community\models\News;

/**
 * Site controller
 */
class NewsController extends WebController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
        ];
    }


    /**
     * Displays list of news.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', []);
    }

    public function actionView($slug)
    {
        $model = News::find()->hasSlug($slug)->one();

        return $this->render('view', [
            'model' => $model
        ]);
    }

}
