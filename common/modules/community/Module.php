<?php

namespace common\modules\community;

use Yii;

/**
 * community module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\community\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        Yii::setAlias('@community', '@common/modules/community');

        Yii::$app->i18n->translations['app/modules/community*'] = [
            'class'          => 'yii\i18n\GettextMessageSource',
            'basePath'       => '@community/messages',
            'catalog'        => 'community',
            'sourceLanguage' => 'en'
        ];

        if (Yii::$app->id == 'app-backend') {
            $this->controllerNamespace .= '\backend';
            $this->viewPath .= '/backend';
            $controllers = [
                'news',
            ];

            $controllersList = implode('|', $controllers);
            Yii::$app->getUrlManager()->addRules([
                '<controller:(' . $controllersList . ')>'                   => 'community/<controller>/index',
                '<controller:(' . $controllersList . ')>/<action>/<id:\d+>' => 'community/<controller>/<action>',
                '<controller:(' . $controllersList . ')>/<action>'          => 'community/<controller>/<action>',
            ], false);
            Yii::$app->urlManagerFront->addRules([
                '<controller:(' . $controllersList . ')>'                   => 'community/<controller>/index',
                '/news/<slug:(\w|\d|\/|-|_)+>'                             => 'community/news/view',
                '<controller:(' . $controllersList . ')>/<action>'          => 'community/<controller>/<action>',
            ], false);

        } elseif(Yii::$app->id == 'app-frontend') {
            $this->controllerNamespace .= '\frontend';
            $this->viewPath .= '/frontend';
            $controllers = [
                'news'
            ];

            $controllersList = implode('|', $controllers);
            Yii::$app->getUrlManager()->addRules([
                '<controller:(' . $controllersList . ')>'                   => 'community/<controller>/index',
                '/news/<slug:(\w|\d|\/|-|_)+>'                             => 'community/news/view',
                '<controller:(' . $controllersList . ')>/<action>'          => 'community/<controller>/<action>',
            ], false);
        }

    }
}
