<?php /** @noinspection PhpUnhandledExceptionInspection */

use common\modules\community\models\News;
use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $model News
 */


$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/modules/community/frontend', "News"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?php echo Html::encode($this->title) ?></h1>


    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <?php echo Html::img(Yii::$app->images->getSrc($model->mainPicture)) ?>
            </div>
            <div class="col-md-12">
                <span><?php echo date('d.m.Y H:i', $model->publishedAt) ?></span>
                <p><?php echo $model->text; ?></p>
            </div>
        </div>
    </div>

</div>
