<?php

use common\modules\community\models\News;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;

/**
 * @var $this yii\web\View
 */

$this->title = Yii::t('app/modules/community/frontend', 'All news');
$this->params['breadcrumbs'][] = Yii::t('app/modules/community/frontend', "News");
?>
<div class="news-index">

    <h1><?php echo Yii::t('app/modules/community/frontend', 'All news') ?></h1>

    <div class="row">
        <?php
        $query = News::find()->published();

        /** @var News $model */
        $dataPovider = new ActiveDataProvider([
            'query'      => $query->orderBy(['updatedAt' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);
        foreach ($dataPovider->getModels() as $model) { ?>
            <div class="col-md-12 mb-3">
                <div class="row">
                    <div class="col-md-6">
                        <?php echo Html::a(
                            Html::img(Yii::$app->images->getSrc($model->preview, 'medium')),
                            ['/community/news/view', 'slug' => $model->slug],
                            ['class' => 'preview', 'target' => '_blank']
                        ); ?>
                    </div>
                    <div class="col-md-6">
                        <h3><?php echo Html::a($model->title, ['/community/news/view', 'slug' => $model->slug]) ?></h3>
                        <span><?php echo date('d.m.Y H:i', $model->publishedAt) ?></span>
                        <p><?php echo Html::a($model->shortDesc,
                                ['/community/news/view', 'slug' => $model->slug]) ?> </p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

</div>