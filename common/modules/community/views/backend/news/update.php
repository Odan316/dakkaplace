<?php

use common\modules\community\models\News;
use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $model News
 */

$this->title = Yii::t('app/modules/community/backend', 'Edit news');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/modules/community/backend', "News"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-update">

    <h1><?php echo Html::encode($this->title)?></h1>

    <?php echo $this->render('_form', [
        'model' => $model
    ]) ?>
</div>
