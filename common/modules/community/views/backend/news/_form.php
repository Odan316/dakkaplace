<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\components\AdminActiveForm;
use common\components\CKEditorHelper;
use common\modules\community\models\News;
use common\widgets\images_picker\ImagesPickerWidget;
use dosamigos\ckeditor\CKEditor;
use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $model News
 */

$baseUrl = Yii::$app->urlManagerFront->createAbsoluteUrl(['/community/news/index']); // Not real news view page, but it is simpler this way
?>

<?php $form = AdminActiveForm::begin(); ?>

<?php echo $form->field($model, 'title')->textInput(['autofocus' => true]) ?>
<?php echo $form->field($model, 'slug', [
    'template' => "{label}\n<div class=\"col-sm-10\"><div class=\"input-group mb-3\"><div class=\"input-group-prepend\">
    <span class=\"input-group-text\" id=\"basic-addon3\">{$baseUrl}/</span></div>{input}</div></div>"
])
    ->textInput([
        'placeholder' => Yii::t('app/common', 'Leave empty for auto-generated slug')
    ]);
?>
<?php echo $form->field($model, 'shortDesc')->textarea() ?>
<?php echo $form->field($model, 'text')->widget(CKEditor::class, [
    'options'       => ['rows' => 6],
    'preset'        => 'custom',
    'clientOptions' => CKEditorHelper::getMinimalPreset()
]); ?>

<?php echo $form->field($model, 'preview')->widget(ImagesPickerWidget::class, [
    'isMultiple' => false
]); ?>

<?php echo $form->field($model, 'mainPicture')->widget(ImagesPickerWidget::class, [
    'isMultiple' => false
]); ?>

<div class="form-group">
    <?php echo Html::submitButton(Yii::t('app/common', 'Save'), ['class' => 'btn btn-primary']) ?>
</div>

<?php AdminActiveForm::end(); ?>
