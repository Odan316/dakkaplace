<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\components\AdminDetailView;
use common\modules\community\models\News;
use yii\bootstrap4\Html;

/**
 * @var $this yii\web\View
 * @var $model News
 */


$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/modules/community/backend', "News"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?php if (Yii::$app->user->can('backend/news/edit')) { ?>
            <?php echo Html::a(Yii::t('app/common', "Edit"), ['update', 'id' => $model->id],
                ['class' => 'btn btn-primary']) ?>

            <?php if ($model->isNotPublished()) { ?>
                <?php echo Html::a(Yii::t('app/modules/community/backend', "Publish"), ['publish', 'id' => $model->id],
                    ['class' => 'btn btn-warning']) ?>
            <?php } ?>

            <?php if ($model->isPublic()) { ?>
                <?php echo Html::a(Yii::t('app/modules/community/backend', "Hide"), ['hide', 'id' => $model->id],
                    ['class' => 'btn btn-danger']) ?>
            <?php } ?>
        <?php } ?>
    </div>

    <?php echo AdminDetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'title',
            [
                'attribute' => 'slug',
                'format'    => 'html',
                'value'     => function ($model) {
                    /** @var News $model */
                    $url = Yii::$app->urlManagerFront->createAbsoluteUrl(['/community/news/view', 'slug' => $model->slug]);
                    $html = Html::a($url, $url);
                    return $html;
                }
            ],
            'visibilityTitle',
            'shortDesc',
            [
                'attribute' => 'text',
                'format'    => 'html',
            ],
            [
                'attribute' => 'preview',
                'format'    => 'raw',
                'value'     => function ($model) {
                    $html = Html::beginTag('div', ['class' => 'imagesList']);
                    /** @var News $model */
                    if (!empty($model->preview)) {
                        $html .= Html::a(
                            Html::img(Yii::$app->images->getSrc($model->preview, 'medium')),
                            $model->preview,
                            ['class' => 'preview', 'target' => '_blank']
                        );
                    }
                    $html .= Html::endTag('div');
                    return $html;
                }
            ],
            [
                'attribute' => 'mainPicture',
                'format' => 'raw',
                'value'  => function ($model) {
                    $html = Html::beginTag('div', ['class' => 'imagesList']);
                    /** @var News $model */
                    if (!empty($model->mainPicture)) {
                        $html .= Html::a(
                            Html::img(Yii::$app->images->getSrc($model->mainPicture, 'medium')),
                            $model->mainPicture,
                            ['class' => 'preview', 'target' => '_blank']
                        );
                    }
                    $html .= Html::endTag('div');
                    return $html;
                }
            ],
            [
                'attribute' => 'createdAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ],
            [
                'attribute' => 'updatedAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ],
            [
                'attribute' => 'publishedAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ]
        ],
    ]) ?>

</div>
