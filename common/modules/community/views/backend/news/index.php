<?php /** @noinspection PhpUnhandledExceptionInspection */

use backend\components\AdminGridView;
use common\modules\community\models\News;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;

/**
 * @var $this yii\web\View
 */

$this->title = Yii::t('app/modules/community/backend', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?php echo Yii::t('app/modules/community/backend', 'News') ?></h1>

    <div class="form-group">
        <?php if (Yii::$app->user->can('backend/news/edit')) { ?>
            <?php echo Html::a(Yii::t('app/common', 'Add'),
                ['create'], ['class' => 'btn btn-primary']) ?>
        <?php } ?>
    </div>


    <?php echo AdminGridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query'      => News::find(),
            'pagination' => [
                'pageSize' => 50
            ]
        ]),
        'emptyText'    => Yii::t('app/modules/community/backend', 'No news yet!'),
        'columns'      => [
            [
                'attribute' => 'title',
                'format'    => 'raw',
                'value'     => function ($model) {
                    /** @var $model News */
                    return Html::a($model->title, ['view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'visibility',
                'value'     => 'visibilityTitle'
            ],
            [
                'attribute' => 'createdAt',
                'format'    => ['date', 'php:d.m.Y H:i']
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view} {edit}',
                'buttons'       => [
                    'edit'   => function ($url, $model, $key) {
                        if (Yii::$app->user->can('backend/news/edit')) {
                            /** @var $model News */
                            return Html::a('<i class="fas fa-pencil-alt"></i>',
                                ['update', 'id' => $model->id]);
                        }
                        return '';
                    }
                ]
            ]
        ]
    ]); ?>

</div>
