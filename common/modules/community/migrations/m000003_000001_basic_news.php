<?php

use console\components\SchemaHelper;
use yii\db\Migration;

/**
 */
class m000003_000001_basic_news extends Migration
{
    const NOT_PUBLISHED = 0;

    /**
     * @return bool|void
     * @throws Exception
     */
    public function up()
    {
        $tableOptions = SchemaHelper::getTableOptions($this->db->driverName);

        $this->createTable('news', [
            'id'          => $this->primaryKey(),
            'createdAt'   => $this->integer()->notNull(),
            'updatedAt'   => $this->integer()->notNull(),
            'publishedAt' => $this->integer(),
            'title'       => $this->string(128)->notNull(),
            'slug'        => $this->string(32)->notNull(),
            'shortDesc'   => $this->string(255),
            'text'        => $this->text(),
            'preview'     => $this->string(255),
            'mainPicture' => $this->string(255),
            'visibility'  => $this->integer()->notNull()->defaultValue(self::NOT_PUBLISHED)
        ], $tableOptions);
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
