<?php

use yii\db\Migration;

/**
 */
class m000003_000002_permissions_news extends Migration
{
    /**
     * @return bool|void
     * @throws Exception
     */
    public function up()
    {
        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('admin');

        $perm = $auth->createPermission('backend/news/view');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $perm = $auth->createPermission('backend/news/edit');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);

    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $auth = Yii::$app->authManager;

        $perm = $auth->getPermission('backend/news/view');
        $auth->remove($perm);
        $perm = $auth->getPermission('backend/news/edit');
        $auth->remove($perm);

    }
}
