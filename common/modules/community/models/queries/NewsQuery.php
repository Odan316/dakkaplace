<?php

namespace common\modules\community\models\queries;

use common\modules\community\models\News;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\community\models\News]].
 *
 * @method News[]|array all($db = null)
 * @method News|array one($db = null)
 *
 * @see \common\modules\community\models\News
 */
class NewsQuery extends ActiveQuery
{

    /**
     * @param $id
     * @return NewsQuery
     */
    public function hasId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param string $slug
     *
     * @return NewsQuery
     */
    public function hasSlug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }

    /**
     * @return NewsQuery
     */
    public function published()
    {
        return $this->andWhere(['visibility' => [News::VIS_PUBLIC]]);
    }


    /**
     * @return NewsQuery
     */
    public function notPublished()
    {
        return $this->andWhere(['visibility' => News::VIS_NOT_PUBLISHED]);
    }

    /**
     * @return NewsQuery
     */
    public function orderByPublish()
    {
        return $this->orderBy(['publishedAt' => SORT_DESC]);
    }
}
