<?php

namespace common\modules\community\models;

use common\components\AdvStringHelper;
use common\modules\community\models\queries\NewsQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $createdAt
 * @property int $updatedAt
 * @property int $publishedAt
 * @property string $title
 * @property string $slug
 * @property string $shortDesc
 * @property string $text
 * @property string $preview
 * @property string $mainPicture
 * @property int $visibility
 *
 * @property string $visibilityTitle
 */
class News extends ActiveRecord
{
    const VIS_NOT_PUBLISHED = 0;
    const VIS_PUBLIC = 20;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    public function behaviors()
    {
        return [
            [
                'class'              => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'shortDesc'], 'trim'],
            [['title', 'shortDesc'], 'required'],
            [['visibility', 'publishedAt'], 'integer'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 128],
            [['slug'], 'string', 'max' => 32],
            [['shortDesc', 'preview', 'mainPicture'], 'string', 'max' => 255],
            [['visibility'], 'in', 'range' => array_keys(self::getVisibilities())],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app/common', 'ID'),
            'createdAt'   => Yii::t('app/common', 'Created at'),
            'updatedAt'   => Yii::t('app/common', 'Updated at'),
            'publishedAt' => Yii::t('app/modules/community', 'Published at'),
            'title'       => Yii::t('app/modules/community', 'Title'),
            'slug'        => Yii::t('app/modules/community', 'Slug'),
            'shortDesc'   => Yii::t('app/modules/community', 'Short description'),
            'text'        => Yii::t('app/modules/community', 'Text'),
            'preview'     => Yii::t('app/modules/community', 'Preview'),
            'mainPicture' => Yii::t('app/modules/community', 'Main picture'),
            'visibility'  => Yii::t('app/modules/community', 'Visibility'),
            'visibilityTitle'  => Yii::t('app/modules/community', 'Visibility'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (empty($this->slug) && !empty($this->title)) {
            $this->slug = AdvStringHelper::slugify($this->title);

        }

        return parent::beforeValidate();
    }

    /**
     * @return string[]
     */
    public static function getVisibilities()
    {
        return [
            self::VIS_NOT_PUBLISHED => Yii::t('app/modules/community', 'Not published'),
            self::VIS_PUBLIC        => Yii::t('app/modules/community', 'Public')
        ];
    }

    /**
     * @return string
     */
    public function getVisibilityTitle()
    {
        return self::getVisibilities()[$this->visibility];
    }

    /**
     * @return boolean
     */
    public function isNotPublished()
    {
        return $this->visibility == self::VIS_NOT_PUBLISHED;
    }

    /**
     * @return bool
     */
    public function isPublic()
    {
        return $this->visibility == self::VIS_PUBLIC;
    }
}
