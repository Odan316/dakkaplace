<?php

namespace common\modules\outcomes\controllers\frontend;

use common\modules\outcomes\models\forms\AttackForm;
use Yii;
use yii\web\Controller;

/**
 * Class CalculatorController
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class CalculatorController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new AttackForm();
        if ($model->load(Yii::$app->request->post()) && $model->calculate()) {
            return $this->goBack();
        }
        return $this->render('index',[
            'model' => $model
        ]);
    }

}