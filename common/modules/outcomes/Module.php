<?php

namespace common\modules\outcomes;

use Yii;

/**
 * tournament module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'common\modules\outcomes\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        Yii::setAlias('@outcomes', '@common/modules/outcomes');

        Yii::$app->i18n->translations['app/modules/outcomes*'] = [
            'class'          => 'yii\i18n\GettextMessageSource',
            'basePath'       => '@outcomes/messages',
            'catalog'        => 'outcomes',
            'sourceLanguage' => 'en'
        ];

        $controllers = [];

        if (Yii::$app->id == 'app-backend') {
            $this->controllerNamespace .= '\backend';
            $this->viewPath .= '/backend';
            $controllers = [
            ];
        } elseif(Yii::$app->id == 'app-frontend') {
            $this->controllerNamespace .= '\frontend';
            $this->viewPath .= '/frontend';
            $controllers = [
                'calculator'
            ];
        }

        $controllersList = implode('|', $controllers);
        Yii::$app->getUrlManager()->addRules([
            '<controller:(' . $controllersList . ')>'                   => 'outcomes/<controller>/index',
            '<controller:(' . $controllersList . ')>/<action>/<id:\d+>' => 'outcomes/<controller>/<action>',
            '<controller:(' . $controllersList . ')>/<action>'          => 'outcomes/<controller>/<action>',
        ], false);



    }
}
