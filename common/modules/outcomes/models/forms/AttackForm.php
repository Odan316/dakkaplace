<?php

namespace common\modules\outcomes\models\forms;

use common\modules\outcomes\components\DiceHelper;
use yii\base\Model;

/**
 * AttackForm is the model behind the attack constructor form.
 */
class AttackForm extends Model
{
    public $attacksAmount;
    public $toHit;
    public $toHitBonus;
    public $toWound;
    public $toWoundBonus;
    public $save;
    public $fnp;
    public $wounds;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['dicesAmount', 'goalValue', 'algorithm'], 'required'],
            [['dicesAmount', 'goalValue', 'algorithm'], 'integer'],
            [['algorithm'], 'in', 'range' => array_keys(DiceHelper::getAlgorithmsList())],
        ];
    }


    /**
     */
    public function calculate()
    {
        if ($this->validate()) {
            $this->result = (new DiceHelper())->calculateChances($this->goalValue, $this->dicesAmount, $this->algorithm);
        }
    }
}
