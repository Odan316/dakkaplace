<?php
/**
 * @var $this yii\web\View
 * @var $model DiceForm
 */

use common\modules\outcomes\components\DiceHelper;
use common\modules\outcomes\models\forms\DiceForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = Yii::t('app/modules/outcomes', 'Outcomes calculator');

?>

<?php  /*$form = AdminActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?php echo $form->field($model, 'algorithm')->dropDownList(DiceHelper::getAlgorithmsList()) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php echo $form->field($model, 'goalValue')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php echo $form->field($model, 'dicesAmount')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <div class="form-group">
            <label>&nbsp;</label>
            <p><?php echo Html::submitButton(Yii::t('app', 'GO!'), ['class' => 'btn btn-success']) ?></p>
        </div>
    </div>
</div>
<h3><?php echo Yii::t('app', 'Result: {result}%', ['result' => round($model->result*100, 2)]) ?></h3>
<?php AdminActiveForm::end(); */?>

<?php echo $this->render('_formAttack', ['model' => $model])?>





