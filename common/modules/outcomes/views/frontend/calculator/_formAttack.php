<?php

use common\modules\outcomes\models\forms\AttackForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/**
 * @var $this yii\web\View
 * @var $model AttackForm
 */

?>

<?php $form = ActiveForm::begin(['id' => 'attackForm']); ?>
    <h2>Attack calculator</h2>
    <h3>Attacker params</h3>
    <div class="row">
        <div class="col-md-2 col-sm-6">
            <?php echo $form->field($model, 'attacksAmount')->textInput() ?>
        </div>
        <div class="col-md-2 col-sm-6">
            <?php echo $form->field($model, 'toHit')->textInput() ?>
        </div>
        <div class="col-md-2 col-sm-6">
            <?php echo $form->field($model, 'toHitBonus')->textInput() ?>
        </div>
        <div class="col-md-2 col-sm-6">
            <?php echo $form->field($model, 'toWound')->textInput() ?>
        </div>
        <div class="col-md-2 col-sm-6">
            <?php echo $form->field($model, 'toWoundBonus')->textInput() ?>
        </div>
    </div>
    <h3>Defender params</h3>
    <div class="row">
        <div class="col-md-2 col-sm-6">
            <?php echo $form->field($model, 'save')->textInput() ?>
        </div>
        <div class="col-md-2 col-sm-6">
            <?php echo $form->field($model, 'fnp')->textInput() ?>
        </div>
        <div class="col-md-2 col-sm-6">
            <?php echo $form->field($model, 'wounds')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-12">
            <div class="form-group">
                <label>&nbsp;</label>
                <p><?php echo Html::submitButton(Yii::t('app', 'GO!'), ['class' => 'btn btn-success']) ?></p>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>