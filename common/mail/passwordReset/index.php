<?php
/**
 * @var \yii\mail\MessageInterface $message the message being composed
 * @var string $username
 * @var string $passwordResetToken
 */

$this->title = Yii::t('app/common', '{{Registration mail title}}');
$lang = \Yii::$app->language;
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
        <td width="850" bgcolor="#f7f7f7" valign="top" align="center"
            style="border: none; padding: 0 44px 16px 44px; text-align: center">
            <p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">
                <?php echo Yii::t('app/common', 'Hello!') ?>
            </p>
            <p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">
                <?php echo Yii::t('app/common', '{{Reset password mail description}}', [
                    'username'     => $username,
                    'reset_link' => Yii::$app->urlManager->createAbsoluteUrl([
                        'site/reset-password',
                        'token' => $passwordResetToken
                    ])
                ]) ?>
            </p>
        </td>
    </tr>
</table>

