function bindCropUploadEvents(widgetId) {
    var $uploadCrop;

    var $widgetDiv = $("#"+widgetId);

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $widgetDiv.addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    console.log('jQuery bind complete');
                });
                $uploadCrop.on('update.croppie', function(e, cropData) {
                    //console.log(cropData);
                    $uploadCrop.croppie('result', {
                        type: 'canvas',
                        size: 'viewport'
                    }).then(function (resp) {
                        $('.fileToUpload',$widgetDiv).val(resp);
                    });
                });
            };

            reader.readAsDataURL(input.files[0]);
        }
        else {
            //swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('.cropper', $widgetDiv).croppie({
        viewport: {
            width: 300,
            height: 300,
            type: 'circle'
        },
        enableExif: true
    });

    $('.fileInput',$widgetDiv).on('change', function () {
        readFile(this);
    });
}