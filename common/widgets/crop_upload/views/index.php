<?php
/**
 * @var $this yii\web\View
 * @var $id string
 * @var $model ActiveRecord
 * @var $attribute string
 */


use common\widgets\crop_upload\CropUploadAsset;
use yii\db\ActiveRecord;
use yii\bootstrap\Html;

CropUploadAsset::register($this);

$fieldName = Html::getInputName($model, $attribute);
$fieldId = $id.'Input';
?>

<div id="<?php echo $id; ?>" class="uploadCropOuter">
    <?php echo Html::fileInput($fieldId.'_raw', null,
        ['id' => $fieldId, 'accept' => 'image/*', 'class' => 'fileInput']);?>
    <?php echo Html::label(Yii::t('app/frontend', 'Click to choose file'), $fieldId,
        ['class' => 'fileInputLabel'])?>
    <?php echo Html::hiddenInput($fieldName, null,
        ['class' => 'fileToUpload']);?>
    <div class="cropperWrap">
        <div class="cropper">
        </div>
    </div>
</div>

<?php $this->registerJs("bindCropUploadEvents(\"{$id}\");") ?>