<?php

namespace common\widgets\crop_upload;

use yii\web\AssetBundle;

/**
 * Class ImagesPickerAsset
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class CropUploadAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/crop_upload/assets/';
    public $baseUrl = '@web/assets';

    public $jsOptions = ['position' => \yii\web\View::POS_END];

    public $css = [
        'css/styles.css',
    ];
    public $js = [
        'js/scripts.js',
    ];
    public $depends = [
        'backend\assets\AppAsset',
        'common\assets\CroppieAsset'
    ];
}
