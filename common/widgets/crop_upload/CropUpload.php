<?php
/**
 * Widget over Croppie for cropping images and uploads them
 */

namespace common\widgets\crop_upload;

use yii\base\Widget;

class CropUpload extends Widget
{
    /** @var string */
    public $model = '';
    /** @var string */
    public $attribute = '';

    public static $autoIdPrefix = 'wCropUpload';

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('index', [
            'id' => $this->getId(),
            'model' => $this->model,
            'attribute' => $this->attribute
        ]);
    }
}