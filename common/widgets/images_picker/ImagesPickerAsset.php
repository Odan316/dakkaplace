<?php
/**
 */

namespace common\widgets\images_picker;


use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class ImagesPickerAsset
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class ImagesPickerAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/images_picker/assets/';
    public $baseUrl = '@web/assets';

    public $jsOptions = ['position' => View::POS_END];

    public $css = [
        'css/image_picker.css',
    ];
    public $js = [
        'js/image_picker.js',
    ];
    public $depends = [
        'common\assets\AppAsset',
        'common\assets\JQueryUIAsset'
    ];
}
