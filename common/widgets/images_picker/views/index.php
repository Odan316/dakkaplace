<?php /** @noinspection PhpUnhandledExceptionInspection */

/**
 * @var $this yii\web\View
 * @var $model ActiveRecord
 * @var $attribute string
 * @var $isMultiple boolean
 * @var $form ActiveForm
 */


use common\widgets\images_picker\ImagesPickerAsset;
use mihaildev\elfinder\InputFile;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;

ImagesPickerAsset::register($this);

$images = $model->$attribute;
if (!is_array($images)) {
    $images = [$images];
}

$fieldName = Html::getInputName($model, $attribute) . ($isMultiple ? '[]' : '');
?>
<div class="imagePickerGallery">
    <?php foreach ($images as $imageInfo) {
        $imageArr = explode(';', $imageInfo);
        $imagePath = $imageArr[0];
        if (!empty($imagePath)) { ?>
            <div class="imagePickerItem">
                <?php echo Html::a(
                    Html::img($imagePath, ['class' => 'galleryThumbnail']),
                    $imagePath,
                    ['class' => '', 'target' => '_blank']
                ); ?>
                <span class="deleteImage"></span>
                <?php echo Html::hiddenInput($fieldName, $imageInfo,
                    ['class' => 'preview']); ?>
            </div>
        <?php }
    } ?>
</div>

<?php echo InputFile::widget([
    'id'            => 'image-picker-' . $attribute,
    'name'          => $attribute,
    'language'      => 'ru',
    'controller'    => 'elfinder',
    // вставляем название контроллера, по умолчанию равен elfinder
    'filter'        => [],
    // фильтр файлов, можно задать массив фильтров
    // https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
    'template'      => '<div class="d-none">{input}</div>{button}',
    'options'       => [
        'class' => 'imagesPickerField form-control',
        'data'  => [
            'field-name' => $fieldName,
            'multiple'   => intval($isMultiple)
        ]
    ],
    'buttonOptions' => ['class' => 'btn btn-success'],
    'buttonName'    => $isMultiple
        ? Yii::t('app/common', 'Add')
        : Yii::t('app/common', 'Change'),
    'multiple'      => $isMultiple
]) ?>
<?php $this->registerJs("bindImagePickerEvents();") ?>
