function bindImagePickerEvents()
{
    $(document).on('change', '.imagesPickerField', function(){
        var previewDiv = $(this).parents('.form-group').find('.imagePickerGallery');
        var linksStr = $(this).val();
        var inputName = $(this).data('field-name');
        var multiple = !!+$(this).data('multiple');
        if(linksStr !== undefined && linksStr !== ''){
            var links = linksStr.split(', ');
            for(var i = 0; i < links.length; i++){
                var path = links[i];
                path = path.replace(/^.*\/\/[^\/]+/, '');
                if(multiple !== true){
                    previewDiv.html('');
                }
                var position = (previewDiv.find('.imagePickerItem').length + 1);
                if( !(previewDiv.find("[src='" + path + "']").length > 0)){
                    var itemDiv = $('<div>').addClass('imagePickerItem');
                    var a = $('<a>').attr('href', path).attr('target', '_blank');
                    var img = $('<img/>').attr('src', path).addClass('galleryThumbnail').appendTo(a);
                    a.appendTo(itemDiv);
                    var deleteSpan = $('<span>').addClass('deleteImage').appendTo(itemDiv);
                    var val = path;
                    if(multiple){
                        val += ';' + position;
                    }
                    var input = $('<input type="hidden"/>').addClass('preview')
                        .attr('name', inputName)
                        .val(val)
                        .appendTo(itemDiv);
                    itemDiv.appendTo(previewDiv);
                }
            }
        }
    });

    $(document).on('click', '.deleteImage', function() {
        var $container = $(this).parents('.imagePickerGallery');
        $(this).parents('.imagePickerItem').detach();
        reorderImages($container);
    });

    $( ".imagePickerGallery" ).each(function(){
        $(this).sortable({
            stop: function( event, ui ) {
                reorderImages(ui.item.parents('.imagePickerGallery'));
            }
        });
        $(this).disableSelection();
    });
    
    function reorderImages($imagesContainer) {
        $imagesContainer.find('.imagePickerItem').each(function(){
            var position = $(this).parent().children().index($(this)) + 1;
            var dataInput = $(this).find('input.preview');
            var data = dataInput.val().split(';');
            data[1] = position;
            dataInput.val(data.join(';'));
        });

    }
}