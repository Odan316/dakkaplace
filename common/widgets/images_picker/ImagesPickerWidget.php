<?php
/**
 * Widget over elFinder for picking images
 */

namespace common\widgets\images_picker;

use yii\base\Widget;
use yii\widgets\ActiveForm;

class ImagesPickerWidget extends Widget
{
    /** @var ActiveForm  */
    public $form;
    /** @var string  */
    public $model = '';
    /** @var string  */
    public $attribute = '';
    /** @var boolean  */
    public $isMultiple = '';

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('index', [
            'form' => $this->form,
            'model' => $this->model,
            'attribute' => $this->attribute,
            'isMultiple' => $this->isMultiple,
        ]);
    }
}