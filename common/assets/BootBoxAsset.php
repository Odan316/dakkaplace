<?php

namespace common\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class BootBoxAsset
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class BootBoxAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/bootbox';

    public $jsOptions = ['position' => View::POS_END];

    public $js = [
        'dist/bootbox.min.js'
    ];
}
