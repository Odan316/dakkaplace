<?php

namespace common\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class JQueryUIAsset
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class JQueryUIAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-ui';

    public $jsOptions = ['position' => View::POS_END];

    public $js = [
        'jquery-ui.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
