<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class CroppieAsset
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 * @link https://github.com/Foliotek/Croppie
 */
class CroppieAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/croppie';

    public $jsOptions = ['position' => \yii\web\View::POS_END];

    public $css = [
        'croppie.css'
    ];

    public $js = [
        'croppie.min.js'
    ];
}
