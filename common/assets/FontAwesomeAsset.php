<?php

namespace common\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class FontAwesomeAsset
 *
 * @author Andreev Sergey <si.andreev316@gmail.com>
 * @version 1.0
 * @since 1.0
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/fontawesome';

    public $jsOptions = ['position' => View::POS_END];

    public $css = [
        'css/all.min.css'
    ];
}
