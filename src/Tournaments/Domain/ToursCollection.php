<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

class ToursCollection
{
    /** @var array<Tour> */
    private array $tours;

    /**
     * @param array<Tour> $tours
     */
    public function __construct(array $tours = [])
    {
        // TODO: check for doubled tour nums, maybe sort and index by tour num
        $this->tours = $tours;
    }

    /**
     * @return array<Tour>
     */
    public function getAll(): array
    {
        return $this->tours;
    }

    public function getByNum(int $num): ?Tour
    {
        foreach ($this->tours as $tour) {
            if ($tour->getNum() === $num) {
                return $tour;
            }
        }

        return null;
    }

    public function getCount(): int
    {
        return count($this->tours);
    }

    public function addTour(Tour $tour): void
    {
        $this->tours[] = $tour;
    }
}