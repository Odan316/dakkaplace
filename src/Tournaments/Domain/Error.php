<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

use LitGroup\Enumerable\Enumerable;

final class Error extends Enumerable
{
    public static function directStateSet(): self
    {
        return self::createEnum('state can\'t be set directly');
    }

    public static function ownerChange(): self
    {
        return self::createEnum('owner can\'t be changed');
    }
}