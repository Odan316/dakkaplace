<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

final class Faction
{
    private int $id;
    private string $title;
    private GameSystem $gameSystem;

    public function __construct(int $id, string $title, GameSystem $gameSystem)
    {
        $this->id = $id;
        $this->title = $title;
        $this->gameSystem = $gameSystem;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getGameSystem(): GameSystem
    {
        return $this->gameSystem;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function setGameSystem(GameSystem $gameSystem): void
    {
        $this->gameSystem = $gameSystem;
    }
}