<?php

namespace App\Tournaments\Domain;

class Tour
{
    private int $num;
    private PairsCollection $pairs;

    public function __construct(int $num, PairsCollection $pairs)
    {
        $this->num = $num;
        $this->pairs = $pairs;
    }

    public static function initiate(Tournament $tournament): self
    {
        $num = $tournament->getToursAmount();
        $pairs = new PairsCollection();

        return new self($num, $pairs);
    }

    public function getNum(): int
    {
        return $this->num;
    }

    public function getPairs(): PairsCollection
    {
        return $this->pairs;
    }

    public function createPairs(ParticipantCollection $participantsCollection): void
    {
        $participants = $participantsCollection->getAll();
        $pairsCount = round($participantsCollection->getCount() / 2);

        for ($pairNum = 1; $pairNum <= $pairsCount; $pairNum++) {
            $playerOneKey = rand(0, (count($participants) - 1));
            $playerOne = new ParticipantResult($participants[$playerOneKey]->getUserId(), $this->getNum(), null);
            unset($participants[$playerOneKey]);
            $participants = array_values($participants);

            if (count($participants) !== 0) {
                $playerTwoKey = rand(0, (count($participants) - 1));
                $playerTwo = new ParticipantResult($participants[$playerTwoKey]->getUserId(), $this->getNum(), null);
                unset($participants[$playerTwoKey]);
                $participants = array_values($participants);
            } else {
                $playerTwo = new ParticipantResult(0, $this->getNum(), null);
            }

            $pair = new Pair($pairNum, new ParticipantResultCollection([$playerOne, $playerTwo]));
            $this->pairs->addPair($pair);
        }
    }

    public function setPairs(PairsCollection $pairs): void
    {
        $this->pairs = $pairs;
    }
}