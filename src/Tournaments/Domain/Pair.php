<?php

namespace App\Tournaments\Domain;

class Pair
{
    private int $num;
    private ParticipantResultCollection $participantResultCollection;

    public function __construct(int $num, ParticipantResultCollection $participantResultCollection)
    {
        $this->num = $num;
        $this->participantResultCollection = $participantResultCollection;
    }

    public function getNum(): int
    {
        return $this->num;
    }

    /**
     * @return ParticipantResultCollection
     */
    public function getPlayers(): ParticipantResultCollection
    {
        return $this->participantResultCollection;
    }
}