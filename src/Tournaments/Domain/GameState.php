<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

use LitGroup\Enumerable\Enumerable;

final class GameState extends Enumerable
{
    public static function new(): self
    {
        return self::createEnum(10);
    }
    
    public static function openToRegister(): self
    {
        return self::createEnum(12);
    }
    
    public static function registrationOver(): self
    {
        return self::createEnum(18);
    }
    
    public static function ongoing(): self
    {
        return self::createEnum(20);
    }
    
    public static function finished(): self
    {
        return self::createEnum(30);
    }
    
    public static function closed(): self
    {
        return self::createEnum(50);
    }
    
    public static function cancelled(): self
    {
        return self::createEnum(90);
    }
}