<?php

namespace App\Tournaments\Domain;

use LitGroup\Enumerable\Enumerable;

final class PairingError extends Enumerable
{
    public static function pairsAlreadySet(): self
    {
        return self::createEnum('pairs are already set');
    }

    public static function pairsAreNotSet()
    {
        return self::createEnum('pairs are not set');
    }

    public static function pairNotExist()
    {
        return self::createEnum('pair not exist');
    }
}