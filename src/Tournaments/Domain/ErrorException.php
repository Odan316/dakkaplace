<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

use Exception;

final class ErrorException extends Exception
{
    private Error $error;

    private function __construct(Error $error)
    {
        parent::__construct();
        $this->error = $error;
    }

    public function getError(): Error
    {
        return $this->error;
    }

    public static function directStateSet(): self
    {
        return new self(Error::directStateSet());
    }

    public static function ownerChange(): self
    {
        return new self(Error::ownerChange());
    }
}