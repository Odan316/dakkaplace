<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

class ParticipantResultCollection
{
    /**
     * @var array<ParticipantResult>
     */
    private array $participantResults;

    /**
     * @param array<ParticipantResult> $participantResults
     */
    public function __construct(array $participantResults = [])
    {
        $this->participantResults = $participantResults;
    }

    /**
     * @return array<ParticipantResult>
     */
    public function getAll(): array
    {
        return $this->participantResults;
    }

    public function add(ParticipantResult $participantResult): void
    {
        $this->participantResults[] = $participantResult;
    }

    public function isEmpty(): bool
    {
        return count($this->participantResults) === 0;
    }

    public function getByParticipantId(int $participantId): ?ParticipantResult
    {
        foreach($this->participantResults as $participantResult){
            if($participantResult->getParticipantId() === $participantId){
                return $participantResult;
            }
        }

        return null;
    }
}