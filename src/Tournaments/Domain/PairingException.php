<?php

namespace App\Tournaments\Domain;

use Exception;

class PairingException extends Exception
{
    private PairingError $error;

    public function __construct(PairingError $error)
    {
        parent::__construct();
        $this->error = $error;
    }

    public function getError(): PairingError
    {
        return $this->error;
    }

    public static function pairsAlreadySet(): self
    {
        return new self(PairingError::pairsAlreadySet());
    }
}