<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

class ParticipantResult
{
    private int $participantId;
    private int $tourNum;
    private ?int $result;

    public function __construct(int $participantId, int $tourNum, ?int $result)
    {
        $this->participantId = $participantId;
        $this->tourNum = $tourNum;
        $this->result = $result;
    }

    public function getParticipantId(): int
    {
        return $this->participantId;
    }

    public function getTourNum(): int
    {
        return $this->tourNum;
    }

    public function getResult(): ?int
    {
        return $this->result;
    }

    public function setResult(int $result): void
    {
        $this->result = $result;
    }
}