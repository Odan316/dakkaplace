<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

use LitGroup\Enumerable\Enumerable;

final class TeamType extends Enumerable
{
    public static function single(): self
    {
        return self::createEnum(1);
    }
    
    public static function pair(): self
    {
        return self::createEnum(10);
    }
}