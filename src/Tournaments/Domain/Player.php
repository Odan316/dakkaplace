<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

final class Player
{
    private int $userId;
    private string $username;

    public function __construct(int $userId, string $username)
    {
        $this->userId = $userId;
        $this->username = $username;
    }

    public function getUsername(): string
    {
        return $this->username;
    }
}