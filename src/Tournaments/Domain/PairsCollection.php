<?php

namespace App\Tournaments\Domain;

class PairsCollection
{
    /** @var array<Pair> */
    private array $pairs;

    /**
     * @param array<Pair> $pairs
     */
    public function __construct(array $pairs = [])
    {
        $this->pairs = $pairs;
    }

    /**
     * @return array<Pair>
     */
    public function getAll(): array
    {
        return $this->pairs;
    }

    public function areSet(): bool
    {
        return count($this->pairs) > 0;
    }

    public function getCount(): int
    {
        return count($this->pairs);
    }

    public function addPair(Pair $pair): void
    {
        $this->pairs[] = $pair;
    }

    public function getByNum(int $num): ?Pair
    {
        foreach($this->pairs as $pair){
            if($pair->getNum() === $num){
                return $pair;
            }
        }

        return null;
    }
}