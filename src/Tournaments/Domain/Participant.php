<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

use DateTimeImmutable;

final class Participant
{
    private int $userId;
    private string $username;
    private Faction $faction;
    private DateTimeImmutable $appliedAt;

    public function __construct(int $userId, string $username, Faction $faction, DateTimeImmutable $appliedAt)
    {
        $this->userId = $userId;
        $this->faction = $faction;
        $this->username = $username;
        $this->appliedAt = $appliedAt;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getFaction(): Faction
    {
        return $this->faction;
    }

    public function getAppliedAt(): DateTimeImmutable
    {
        return $this->appliedAt;
    }
}