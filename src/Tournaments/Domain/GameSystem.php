<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

final class GameSystem
{
    private int $id;
    private string $title;
    
    public function __construct(int $id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }
    
    public function getId(): int
    {
        return $this->id;
    }
    
    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }
}