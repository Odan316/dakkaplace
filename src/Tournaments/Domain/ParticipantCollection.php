<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

final class ParticipantCollection
{
    /** @var array<Participant> */
    private array $participants;

    /**
     * @param array<Participant> $participants
     */
    public function __construct(array $participants = [])
    {
        $this->participants = $participants;
    }

    public function getByUserId(int $userId): ?Participant
    {
        foreach ($this->participants as $participant){
            if($participant->getUserId() === $userId){
                return $participant;
            }
        }

        return null;
    }

    public function add(Participant $participant): void
    {
        $this->participants[] = $participant;
    }

    /**
     * @return array<Participant>
     */
    public function getAll(): array
    {
        return $this->participants;
    }

    public function getCount(): int
    {
        return count($this->participants);
    }
}