<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

use Ramsey\Uuid\UuidInterface;

final class TourResult
{
    private int $userId;
    private int $tourNum;
    private int $pairNum;
    private int $result;

    public function __construct(int $userId, int $tourNum, int $pairNum, int $result){
        $this->userId = $userId;
        $this->tourNum = $tourNum;
        $this->pairNum = $pairNum;
        $this->result = $result;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getTourNum(): int
    {
        return $this->tourNum;
    }

    public function getPairNum(): int
    {
        return $this->pairNum;
    }

    public function getResult(): int
    {
        return $this->result;
    }
}