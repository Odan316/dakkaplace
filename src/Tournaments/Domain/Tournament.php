<?php

declare(strict_types=1);

namespace App\Tournaments\Domain;

use DateTimeImmutable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class Tournament
{
    private UuidInterface $id;
    private int $number;
    private string $title;
    private TeamType $teamType;
    private GameSystem $gameSystem;
    private GameState $state;
    private Owner $owner;
    private DateTimeImmutable $createdAt;
    private DateTimeImmutable $updatedAt;
    private ?DateTimeImmutable $startAt;
    private ParticipantCollection $participants;
    private ToursCollection $tours;

    private function __construct(UuidInterface $id)
    {
        $this->id = $id;
    }

    public static function createNew(
        string $title,
        TeamType $teamType,
        GameSystem $gameSystem,
        Owner $owner
    ): Tournament {
        $tournament = new self(Uuid::uuid4());

        $tournament->title = $title;
        $tournament->teamType = $teamType;
        $tournament->gameSystem = $gameSystem;
        $tournament->owner = $owner;

        $tournament->state = GameState::new();
        $tournament->createdAt = new DateTimeImmutable();
        $tournament->updatedAt = new DateTimeImmutable();

        $tournament->participants = new ParticipantCollection();
        $tournament->tours = new ToursCollection();

        return $tournament;
    }

    public static function restore(
        UuidInterface $id,
        int $number,
        string $title,
        TeamType $teamType,
        GameSystem $gameSystem,
        GameState $state,
        Owner $owner,
        ParticipantCollection $participants,
        ToursCollection $tours,
        DateTimeImmutable $createdAt,
    ): Tournament {
        $tournament = new self($id);

        $tournament->number = $number;
        $tournament->title = $title;
        $tournament->teamType = $teamType;
        $tournament->gameSystem = $gameSystem;
        $tournament->state = $state;
        $tournament->owner = $owner;
        $tournament->participants = $participants;
        $tournament->tours = $tours;
        $tournament->createdAt = $createdAt;

        return $tournament;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getTeamType(): TeamType
    {
        return $this->teamType;
    }

    public function setTeamType(TeamType $teamType): void
    {
        $this->teamType = $teamType;
    }

    public function getGameSystem(): GameSystem
    {
        return $this->gameSystem;
    }

    public function setGameSystem(GameSystem $gameSystem): void
    {
        $this->gameSystem = $gameSystem;
    }

    public function setStartAt(?DateTimeImmutable $startAt): void
    {
        $this->startAt = $startAt;
    }

    public function getStartAt(): ?DateTimeImmutable
    {
        return $this->startAt;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getOwner(): Owner
    {
        return $this->owner;
    }

    /**
     * @throws ErrorException
     */
    public function setOwner(Owner $owner): void
    {
        throw ErrorException::ownerChange();
    }

    public function getState(): GameState
    {
        return $this->state;
    }

    /**
     * @throws ErrorException
     */
    public function setState(GameState $state): void
    {
        throw ErrorException::directStateSet();
    }

    public function getToursAmount(): int
    {
        return $this->tours->getCount();
    }

    public function getParticipants(): ParticipantCollection
    {
        return $this->participants;
    }


    public function isNew(): bool
    {
        return $this->state === GameState::new();
    }

    public function isRegistrationOpen(): bool
    {
        return $this->state === GameState::openToRegister();
    }

    public function isRegistrationOver(): bool
    {
        return $this->state === GameState::registrationOver();
    }

    public function isOngoing(): bool
    {
        return $this->state === GameState::ongoing();
    }

    public function isFinished(): bool
    {
        return $this->state === GameState::finished();
    }

    /**
     * @return GameState[]
     */
    public static function activeStates(): array
    {
        return [
            GameState::new(),
            GameState::openToRegister(),
            GameState::registrationOver(),
            GameState::ongoing(),
            GameState::finished(),
        ];
    }

    public function canBeCancelled(): bool
    {
        return in_array($this->state, [
            GameState::new(),
            GameState::openToRegister(),
            GameState::registrationOver(),
        ]);
    }

    public function notReadyToStart(): bool
    {
        return in_array($this->state, [
            GameState::new(),
            GameState::openToRegister(),
        ]);
    }

    public function hasStarted(): bool
    {
        return !in_array($this->state, [
            GameState::new(),
            GameState::openToRegister(),
            GameState::registrationOver(),
        ]);
    }

    public function openRegistration(): void
    {
        // TODO: invariants check
        $this->state = GameState::openToRegister();
    }

    public function closeRegistration(): void
    {
        // TODO: invariants check
        $this->state = GameState::registrationOver();
    }

    public function startGame(): void
    {
        // TODO: invariants check
        $this->state = GameState::ongoing();
        $this->tours->addTour(Tour::initiate($this));
    }

    public function finish(): void
    {
        // TODO: invariants check
        $this->state = GameState::finished();
    }

    public function close(): void
    {
        // TODO: invariants check
        $this->state = GameState::closed();
    }

    public function cancel(): void
    {
        // TODO: invariants check
        $this->state = GameState::cancelled();
    }

    public function getTours(): ToursCollection
    {
        return $this->tours;
    }

    public function getTour(int $num): ?Tour
    {
        return $this->tours->getByNum($num);
    }
}