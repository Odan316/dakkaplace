<?php

declare(strict_types = 1);

namespace App\Tournaments\Modules\Admin\Application;

use App\Common\Infrastructure\TransactionManager\TransactionManager;
use App\Tournaments\Application\GameSystemReadRepository;
use App\Tournaments\Application\GameSystemWriteRepository;
use App\Tournaments\Domain\GameSystem;
use App\Tournaments\Modules\Admin\Application\Requests\GameSystemUpdateRequest;

final class GameSystemUpdateProcessor
{
    private GameSystemWriteRepository $gameSystemWriteRepository;
    private GameSystemReadRepository $gameSystemReadRepository;
    private TransactionManager $transactionManager;

    public function __construct(
        GameSystemWriteRepository $gameSystemWriteRepository,
        GameSystemReadRepository $gameSystemReadRepository,
        TransactionManager $transactionManager
    ) {
        $this->gameSystemWriteRepository = $gameSystemWriteRepository;
        $this->gameSystemReadRepository = $gameSystemReadRepository;
        $this->transactionManager = $transactionManager;
    }

    public function update(GameSystemUpdateRequest $request): GameSystem
    {
        $gameSystem = $this->gameSystemReadRepository->get($request->getId());
        $gameSystem->setTitle($request->getTitle());

        $this->transactionManager->transactional(function () use ($gameSystem): void {
            $this->gameSystemWriteRepository->update($gameSystem);
        });

        return $gameSystem;
    }
}