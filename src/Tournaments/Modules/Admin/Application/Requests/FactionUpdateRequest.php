<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Application\Requests;

interface FactionUpdateRequest
{
    public function getId(): int;

    public function getTitle(): string;

    public function getGameSystemId(): int;
}