<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Application\Requests;

interface GameSystemUpdateRequest
{
    public function getId(): int;

    public function getTitle(): string;
}