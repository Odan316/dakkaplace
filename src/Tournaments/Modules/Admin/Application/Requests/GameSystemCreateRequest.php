<?php

declare(strict_types = 1);

namespace App\Tournaments\Modules\Admin\Application\Requests;

interface GameSystemCreateRequest
{
    public function getTitle(): string;
}