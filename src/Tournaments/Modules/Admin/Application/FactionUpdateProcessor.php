<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Application;

use App\Common\Infrastructure\TransactionManager\TransactionManager;
use App\Tournaments\Application\FactionReadRepository;
use App\Tournaments\Application\FactionWriteRepository;
use App\Tournaments\Application\GameSystemReadRepository;
use App\Tournaments\Domain\Faction;
use App\Tournaments\Modules\Admin\Application\Requests\FactionUpdateRequest;

final class FactionUpdateProcessor
{
    private FactionWriteRepository $factionWriteRepository;
    private FactionReadRepository $factionReadRepository;
    private GameSystemReadRepository $gameSystemReadRepository;
    private TransactionManager $transactionManager;

    public function __construct(
        FactionWriteRepository $factionWriteRepository,
        FactionReadRepository $factionReadRepository,
        GameSystemReadRepository $gameSystemReadRepository,
        TransactionManager $transactionManager
    ) {
        $this->factionWriteRepository = $factionWriteRepository;
        $this->factionReadRepository = $factionReadRepository;
        $this->gameSystemReadRepository = $gameSystemReadRepository;
        $this->transactionManager = $transactionManager;
    }

    public function update(FactionUpdateRequest $request): Faction
    {
        $faction = $this->factionReadRepository->get($request->getId());
        $faction->setTitle($request->getTitle());
        $faction->setGameSystem($this->gameSystemReadRepository->get($request->getGameSystemId()));

        $this->transactionManager->transactional(function () use ($faction): void {
            $this->factionWriteRepository->update($faction);
        });

        return $faction;
    }
}