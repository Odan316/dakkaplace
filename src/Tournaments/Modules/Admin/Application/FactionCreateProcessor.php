<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Application;

use App\Common\Infrastructure\TransactionManager\TransactionManager;
use App\Tournaments\Application\FactionReadRepository;
use App\Tournaments\Application\FactionWriteRepository;
use App\Tournaments\Application\GameSystemReadRepository;
use App\Tournaments\Application\RepositoryException;
use App\Tournaments\Domain\Faction;
use App\Tournaments\Modules\Admin\Application\Requests\FactionCreateRequest;

final class FactionCreateProcessor
{
    private FactionWriteRepository $factionWriteRepository;
    private FactionReadRepository $factionReadRepository;
    private GameSystemReadRepository $gameSystemReadRepository;
    private TransactionManager $transactionManager;

    public function __construct(
        FactionWriteRepository $factionWriteRepository,
        FactionReadRepository $factionReadRepository,
        GameSystemReadRepository $gameSystemReadRepository,
        TransactionManager $transactionManager
    ) {
        $this->factionWriteRepository = $factionWriteRepository;
        $this->factionReadRepository = $factionReadRepository;
        $this->gameSystemReadRepository = $gameSystemReadRepository;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws RepositoryException
     */
    public function create(FactionCreateRequest $request): Faction
    {
        $faction = new Faction($this->factionReadRepository->getNextId(), $request->getTitle(), $this->gameSystemReadRepository->get($request->getGameSystemId()));

        $this->transactionManager->transactional(function () use ($faction): void {
            $this->factionWriteRepository->add($faction);
        });

        return $faction;
    }
}