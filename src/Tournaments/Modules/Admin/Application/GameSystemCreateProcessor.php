<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Application;

use App\Common\Infrastructure\TransactionManager\TransactionManager;
use App\Tournaments\Application\GameSystemReadRepository;
use App\Tournaments\Application\GameSystemWriteRepository;
use App\Tournaments\Application\RepositoryException;
use App\Tournaments\Domain\GameSystem;
use App\Tournaments\Modules\Admin\Application\Requests\GameSystemCreateRequest;

final class GameSystemCreateProcessor
{
    private GameSystemWriteRepository $gameSystemWriteRepository;
    private GameSystemReadRepository $gameSystemReadRepository;
    private TransactionManager $transactionManager;

    public function __construct(
        GameSystemWriteRepository $gameSystemWriteRepository,
        GameSystemReadRepository $gameSystemReadRepository,
        TransactionManager $transactionManager
    ) {
        $this->gameSystemWriteRepository = $gameSystemWriteRepository;
        $this->gameSystemReadRepository = $gameSystemReadRepository;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws RepositoryException
     */
    public function create(GameSystemCreateRequest $request): GameSystem
    {
        $gameSystem = new GameSystem($this->gameSystemReadRepository->getNextId(), $request->getTitle());

        $this->transactionManager->transactional(function () use ($gameSystem): void {
            $this->gameSystemWriteRepository->add($gameSystem);
        });

        return $gameSystem;
    }
}