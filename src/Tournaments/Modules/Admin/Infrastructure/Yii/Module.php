<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Infrastructure\Yii;

use Yii;

class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'App\Tournaments\Modules\Admin\Presentation\Yii\Controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        Yii::setAlias('@TournamentsAdmin', '@App/Tournaments/Modules/Admin');

        if (Yii::$app->id == 'app-backend') {
            $controllers = [
                'tournaments',
                'game-system',
                'faction',
            ];

            $controllersList = implode('|', $controllers);
            Yii::$app->getUrlManager()->addRules([
                'tournaments/<controller:(' . $controllersList . ')>'                   => 'tournaments/admin/<controller>/index',
                'tournaments/<controller:(' . $controllersList . ')>/<action>/<id:\d+>' => 'tournaments/admin/<controller>/<action>',
                'tournaments/<controller:(' . $controllersList . ')>/<action>'          => 'tournaments/admin/<controller>/<action>',
            ], false);
            /*Yii::$app->urlManagerFront->addRules([
                '<controller:(' . $controllersList . ')>'                   => 'tournaments/admin/<controller>/index',
                '<controller:(' . $controllersList . ')>/<action>'          => 'tournaments/admin/<controller>/<action>',
            ], false);*/

        }
    }
}
