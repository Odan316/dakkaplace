<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Presentation\Yii\Forms;

use App\Tournaments\Modules\Admin\Application\Requests\GameSystemCreateRequest;
use Yii;
use yii\base\Model;

final class GameSystemCreateForm extends Model implements GameSystemCreateRequest
{
    /** @var string */
    public $title;

    public function rules(): array
    {
        return [
            [['title'], 'trim'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('tournaments', 'Title'),
        ];
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}