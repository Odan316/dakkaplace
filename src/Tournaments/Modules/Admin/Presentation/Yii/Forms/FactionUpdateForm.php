<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Presentation\Yii\Forms;

use App\Tournaments\Domain\Faction;
use App\Tournaments\Modules\Admin\Application\Requests\FactionUpdateRequest;
use Yii;
use yii\base\Model;

final class FactionUpdateForm extends Model implements FactionUpdateRequest
{
    /** @var */
    public $id;
    /** @var string */
    public $title;
    /** @var int */
    public $gameSystemId;

    public function __construct(Faction $faction, $config = [])
    {
        parent::__construct($config);

        $this->id = $faction->getId();
        $this->title = $faction->getTitle();
        $this->gameSystemId = $faction->getGameSystem()->getId();
    }

    public function rules(): array
    {
        return [
            [['title'], 'trim'],
            [['id', 'title', 'gameSystemId'], 'required'],
            [['id', 'gameSystemId'], 'integer'],
            [['title'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tournaments', 'Id'),
            'title' => Yii::t('tournaments', 'Title'),
            'gameSystemId' => Yii::t('tournaments', 'Game System'),
        ];
    }

    public function getId(): int
    {
        return (int)$this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getGameSystemId(): int
    {
        return (int)$this->gameSystemId;
    }
}