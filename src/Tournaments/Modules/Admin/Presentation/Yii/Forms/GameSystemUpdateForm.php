<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Presentation\Yii\Forms;

use App\Tournaments\Domain\GameSystem;
use App\Tournaments\Modules\Admin\Application\Requests\GameSystemUpdateRequest;
use Yii;
use yii\base\Model;

final class GameSystemUpdateForm extends Model implements GameSystemUpdateRequest
{
    /** @var */
    public $id;
    /** @var string */
    public $title;

    public function __construct(GameSystem $gameSystem, $config = [])
    {
        parent::__construct($config);

        $this->id = $gameSystem->getId();
        $this->title = $gameSystem->getTitle();
    }

    public function rules(): array
    {
        return [
            [['title'], 'trim'],
            [['id', 'title'], 'required'],
            [['id'], 'integer'],
            [['title'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tournaments', 'Id'),
            'title' => Yii::t('tournaments', 'Title'),
        ];
    }

    public function getId(): int
    {
        return (int)$this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}