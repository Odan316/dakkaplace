<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Presentation\Yii\Forms;

use App\Tournaments\Modules\Admin\Application\Requests\FactionCreateRequest;
use Yii;
use yii\base\Model;

final class FactionCreateForm extends Model implements FactionCreateRequest
{
    /** @var string */
    public $title;
    /** @var int */
    public $gameSystemId;

    public function rules(): array
    {
        return [
            [['title'], 'trim'],
            [['title', 'gameSystemId'], 'required'],
            [['gameSystemId'], 'integer'],
            [['title'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => Yii::t('tournaments', 'Title'),
            'gameSystemId' => Yii::t('tournaments', 'Game System'),
        ];
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getGameSystemId(): int
    {
        return (int)$this->gameSystemId;
    }
}