<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Presentation\Yii\Controllers;

use App\Tournaments\Application\GameSystemReadRepository;
use App\Tournaments\Application\RepositoryException;
use App\Tournaments\Domain\GameSystem;
use App\Tournaments\Modules\Admin\Application\GameSystemCreateProcessor;
use App\Tournaments\Modules\Admin\Application\GameSystemUpdateProcessor;
use App\Tournaments\Modules\Admin\Presentation\Yii\Forms\GameSystemCreateForm;
use App\Tournaments\Modules\Admin\Presentation\Yii\Forms\GameSystemUpdateForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

final class GameSystemController extends Controller
{
    private GameSystemReadRepository $gameSystemReadRepository;
    private GameSystemCreateProcessor $createProcessor;
    private GameSystemUpdateProcessor $updateProcessor;

    public function __construct(
        $id,
        $module,
        GameSystemReadRepository $gameSystemReadRepository,
        GameSystemCreateProcessor $createProcessor,
        GameSystemUpdateProcessor $updateProcessor,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->gameSystemReadRepository = $gameSystemReadRepository;
        $this->createProcessor = $createProcessor;
        $this->updateProcessor = $updateProcessor;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['backend/dictionaries/view'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'create'],
                        'roles' => ['backend/dictionaries/edit'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex(): string
    {
        $gameSystems = $this->gameSystemReadRepository->getAll();

        return $this->render('index', [
            'gameSystems' => $gameSystems,
        ]);
    }

    public function actionCreate(): string | Response
    {
        $form = new GameSystemCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $gameSystem = $this->createProcessor->create($form);
                return $this->redirect(['index']);
            } catch (RepositoryException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id): string | Response
    {
        $model = $this->getModel((int)$id);
        $form = new GameSystemUpdateForm($model);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $gameSystem = $this->updateProcessor->update($form);
                return $this->redirect(['index']);
            } catch (RepositoryException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    private function getModel(int $id): GameSystem
    {
        $model = $this->gameSystemReadRepository->get($id);

        if (empty($model)) {
            throw new NotFoundHttpException(Yii::t('tournaments', 'Game System is not found'));
        }

        return $model;
    }
}
