<?php

namespace App\Tournaments\Modules\Admin\Presentation\Yii\Controllers;

use App\Tournaments\Application\TournamentReadRepository;
use App\Tournaments\Application\TournamentRenderer;
use yii\filters\AccessControl;
use yii\web\Controller;

class TournamentsController extends Controller
{
    private TournamentReadRepository $tournamentsReadRepository;
    private TournamentRenderer $tournamentRenderer;

    public function __construct(
        $id,
        $module,
        TournamentReadRepository $tournamentsReadRepository,
        TournamentRenderer $tournamentRenderer,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->tournamentsReadRepository = $tournamentsReadRepository;
        $this->tournamentRenderer = $tournamentRenderer;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index'],
                        'roles'   => ['backend/tournaments/view']
                    ]
                ],
            ],
        ];
    }

    public function actionIndex(): string
    {
        $tournaments = $this->tournamentsReadRepository->getActive();

        return $this->render('index', [
            'tournamentRenderer' => $this->tournamentRenderer,
            'tournaments' => $tournaments,
        ]);
    }

}
