<?php

declare(strict_types=1);

namespace App\Tournaments\Modules\Admin\Presentation\Yii\Controllers;

use App\Tournaments\Application\FactionReadRepository;
use App\Tournaments\Application\GameSystemReadRepository;
use App\Tournaments\Application\RepositoryException;
use App\Tournaments\Domain\Faction;
use App\Tournaments\Modules\Admin\Application\FactionCreateProcessor;
use App\Tournaments\Modules\Admin\Application\FactionUpdateProcessor;
use App\Tournaments\Modules\Admin\Presentation\Yii\Forms\FactionCreateForm;
use App\Tournaments\Modules\Admin\Presentation\Yii\Forms\FactionUpdateForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FactionController extends Controller
{
    private FactionReadRepository $factionReadRepository;
    private FactionCreateProcessor $createProcessor;
    private FactionUpdateProcessor $updateProcessor;
    private GameSystemReadRepository $gameSystemReadRepository;

    public function __construct(
        $id,
        $module,
        FactionReadRepository $factionReadRepository,
        FactionCreateProcessor $createProcessor,
        FactionUpdateProcessor $updateProcessor,
        GameSystemReadRepository $gameSystemReadRepository,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->factionReadRepository = $factionReadRepository;
        $this->createProcessor = $createProcessor;
        $this->updateProcessor = $updateProcessor;
        $this->gameSystemReadRepository = $gameSystemReadRepository;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index'],
                        'roles'   => ['backend/dictionaries/view']
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['update', 'create'],
                        'roles'   => ['backend/dictionaries/edit']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex(): string
    {
        $factions = $this->factionReadRepository->getAll();

        return $this->render('index', [
            'factions' => $factions,
        ]);
    }

    public function actionCreate(): string | Response
    {
        $form = new FactionCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $faction = $this->createProcessor->create($form);
                return $this->redirect(['index']);
            } catch (RepositoryException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
            'gameSystems' => $this->gameSystemReadRepository->getAll(),
        ]);
    }

    public function actionUpdate($id): string | Response
    {
        $model = $this->getModel((int)$id);
        $form = new FactionUpdateForm($model);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $faction = $this->updateProcessor->update($form);
                return $this->redirect(['index']);
            } catch (RepositoryException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
            'gameSystems' => $this->gameSystemReadRepository->getAll(),
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    private function getModel(int $id): Faction
    {
        $model = $this->factionReadRepository->get($id);

        if (empty($model)) {
            throw new NotFoundHttpException(Yii::t('tournaments', 'Faction is not found'));
        }

        return $model;
    }

}
