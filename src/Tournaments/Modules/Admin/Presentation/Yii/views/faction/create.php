<?php

use App\Common\Presentation\Yii\Widgets\AdminActiveForm;
use App\Tournaments\Domain\GameSystem;
use App\Tournaments\Modules\Admin\Presentation\Yii\Forms\FactionCreateForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var View $this
 * @var FactionCreateForm $model
 * @var GameSystem[] $gameSystems
 */

$this->title = Yii::t('tournaments', 'Add game faction');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tournaments', "Game factions"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-faction-create">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php $form = AdminActiveForm::begin(); ?>

    <?php echo $form->field($model, 'title')->textInput(['autofocus' => true]) ?>
    <?php echo $form->field($model, 'gameSystemId')
        ->dropDownList(ArrayHelper::map($gameSystems, function (GameSystem $gameSystem) {
            return $gameSystem->getId();
        }, function (GameSystem $gameSystem) {
            return $gameSystem->getTitle();
        })) ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('tournaments', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php AdminActiveForm::end(); ?>

</div>
