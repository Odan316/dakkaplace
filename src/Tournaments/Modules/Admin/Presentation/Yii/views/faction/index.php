<?php
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use App\Tournaments\Domain\Faction;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var Faction[] $factions
 */

$this->title = Yii::t('tournaments', "Game factions");
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faction-index">

    <h1><?php echo $this->title ?></h1>

    <div class="form-group">
        <?php if (Yii::$app->user->can('backend/dictionaries/edit')) { ?>
            <?php echo Html::a(Yii::t('tournaments', 'Add'), ['faction/create'], ['class' => 'btn btn-primary']) ?>
        <?php } ?>
    </div>

    <?php echo GridView::widget([
        'dataProvider' => new ArrayDataProvider([
            'allModels' => $factions,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'layout' => "{items}\n{pager}",
        'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
        'emptyText' => Yii::t('tournaments', 'No factions yet!'),
        'columns' => [
            [
                'label' => Yii::t('tournaments', 'Title'),
                'format' => 'html',
                'value' => function (Faction $model) {
                    return $model->getTitle();
                },
            ],
            [
                'label' => Yii::t('tournaments', 'Title'),
                'format' => 'html',
                'value' => function (Faction $model) {
                    return $model->getGameSystem()->getTitle();
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{edit}',
                'buttons' => [
                    'edit' => function (string $url, Faction $model, $key) {
                        if (Yii::$app->user->can('backend/dictionaries/edit')) {
                            return Html::a('<span class="fas fa-pen"></span>',
                                ['update', 'id' => $model->getId()]);
                        }
                        return false;
                    },
                ],
            ],
        ],
    ]); ?>

</div>
