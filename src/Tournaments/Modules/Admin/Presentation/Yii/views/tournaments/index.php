<?php
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use App\Tournaments\Application\TournamentRenderer;
use App\Tournaments\Domain\Tournament;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\web\View;

/**
 * @var View $this
 * @var TournamentRenderer $tournamentRenderer
 * @var array<Tournament> $tournaments
 */

$this->title = Yii::t('tournaments', 'Tournaments');
$this->params['breadcrumbs'][] = Yii::t('tournaments', 'Tournaments');
?>
<div class="tournaments-index">

    <h1><?php echo $this->title ?></h1>

    <?php echo GridView::widget([
        'dataProvider' => new ArrayDataProvider([
            'allModels' => $tournaments,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'layout' => "{items}\n{pager}",
        'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
        'emptyText' => Yii::t('tournaments', 'No tournaments yet!'),
        'columns' => [
            [
                'label' => Yii::t('tournaments', 'Title'),
                'format' => 'html',
                'value' => function (Tournament $model) {
                    return $model->getTitle();
                },
            ],
            [
                'label' => Yii::t('tournaments', 'Status'),
                'value' => function (Tournament $model) use ($tournamentRenderer) {
                    return $tournamentRenderer->renderStateString($model->getState());
                },
            ],
            [
                'label' => Yii::t('tournaments', 'Game type'),
                'value' => function (Tournament $model) use ($tournamentRenderer) {
                    return $tournamentRenderer->renderTeamTypeString($model->getTeamType());
                },
            ],
            [
                'label' => Yii::t('tournaments', 'Ruleset'),
                'value' => function (Tournament $model) {
                    return $model->getGameSystem()->getTitle();
                },
            ],
            [
                'label' => Yii::t('tournaments', 'GM'),
                'value' => function (Tournament $model) {
                    return $model->getOwner()->getUsername();
                },
            ],
            [
                'label' => Yii::t('tournaments', 'Start at'),
                'value' => function (Tournament $model) {
                    return $model->getStartAt()->format('d.m.Y');
                },
            ],
        ],
    ]); ?>
</div>
