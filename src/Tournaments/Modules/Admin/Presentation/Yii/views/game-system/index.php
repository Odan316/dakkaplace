<?php
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use App\Tournaments\Domain\GameSystem;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var GameSystem[] $gameSystems
 */

$this->title = Yii::t('tournaments', "Game systems");
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-system-index">

    <h1><?php echo $this->title ?></h1>

    <div class="form-group">
        <?php if (Yii::$app->user->can('backend/dictionaries/edit')) { ?>
            <?php echo Html::a(Yii::t('tournaments', 'Add'), ['game-system/create'], ['class' => 'btn btn-primary']) ?>
        <?php } ?>
    </div>

    <?php echo GridView::widget([
        'dataProvider' => new ArrayDataProvider([
            'allModels' => $gameSystems,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'layout' => "{items}\n{pager}",
        'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
        'emptyText' => Yii::t('tournaments', 'No systems yet!'),
        'columns' => [
            [
                'label' => Yii::t('tournaments', 'Title'),
                'format' => 'html',
                'value' => function (GameSystem $model) {
                    return $model->getTitle();
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{edit}',
                'buttons' => [
                    'edit' => function (string $url, GameSystem $model, $key) {
                        if (Yii::$app->user->can('backend/dictionaries/edit')) {
                            return Html::a('<span class="fas fa-pen"></span>',
                                ['update', 'id' => $model->getId()]);
                        }
                        return false;
                    },
                ],
            ],
        ],
    ]); ?>

</div>
