<?php

use App\Common\Presentation\Yii\Widgets\AdminActiveForm;
use App\Tournaments\Modules\Admin\Presentation\Yii\Forms\GameSystemUpdateForm;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var GameSystemUpdateForm $model
 */

$this->title = Yii::t('tournaments', 'Edit game system');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tournaments', "Game systems"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-system-update">

    <h1><?php echo Html::encode($this->title) ?></h1>

    <?php $form = AdminActiveForm::begin(); ?>

    <?php echo $form->field($model, 'title')->textInput(['autofocus' => true]) ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('tournaments', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php AdminActiveForm::end(); ?>
</div>
