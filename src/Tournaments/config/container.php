<?php

declare(strict_types=1);

use yii\di\Container;
use yii\di\Instance;

return [
    'singletons' => [
        App\Tournaments\Application\TournamentReadRepository::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\TournamentRepository::class,
            '__construct()' => [
                Instance::of(App\Tournaments\Application\TournamentExporter::class),
                Instance::of(App\Tournaments\Infrastructure\Yii\Persistence\TournamentARMapper::class),
                Instance::of('common.plainErrorsRenderer'),
            ],
        ],
        App\Tournaments\Application\TournamentWriteRepository::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\TournamentRepository::class,
            '__construct()' => [
                Instance::of(App\Tournaments\Application\TournamentExporter::class),
                Instance::of(App\Tournaments\Infrastructure\Yii\Persistence\TournamentARMapper::class),
                Instance::of('common.plainErrorsRenderer'),
            ],
        ],
        App\Tournaments\Application\TournamentExporter::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\YiiTournamentExporter::class,
            '__construct()' => [
                Instance::of(App\Tournaments\Application\ParticipantExporter::class),
            ],
        ],
        App\Tournaments\Infrastructure\Yii\Persistence\TournamentARMapper::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\YiiTournamentARMapper::class,
        ],

        App\Tournaments\Application\GameSystemReadRepository::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\GameSystemRepository::class,
            '__construct()' => [
                Instance::of(App\Tournaments\Application\GameSystemExporter::class),
                Instance::of('common.plainErrorsRenderer'),
            ],
        ],
        App\Tournaments\Application\GameSystemWriteRepository::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\GameSystemRepository::class,
            '__construct()' => [
                Instance::of(App\Tournaments\Application\GameSystemExporter::class),
                Instance::of('common.plainErrorsRenderer'),
            ],
        ],
        App\Tournaments\Application\GameSystemExporter::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\YiiGameSystemExporter::class,
        ],

        App\Tournaments\Application\FactionReadRepository::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\FactionRepository::class,
            '__construct()' => [
                Instance::of(App\Tournaments\Application\FactionExporter::class),
                Instance::of('common.plainErrorsRenderer'),
            ],
        ],
        App\Tournaments\Application\FactionWriteRepository::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\FactionRepository::class,
            '__construct()' => [
                Instance::of(App\Tournaments\Application\FactionExporter::class),
                Instance::of('common.plainErrorsRenderer'),
            ],
        ],
        App\Tournaments\Application\FactionExporter::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\YiiFactionExporter::class,
        ],

        App\Tournaments\Application\OwnerReadRepository::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\OwnerRepository::class,
            '__construct()' => [
                Instance::of(App\Tournaments\Application\OwnerExporter::class),
            ],
        ],
        App\Tournaments\Application\OwnerExporter::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\YiiOwnerExporter::class,
        ],

        App\Tournaments\Application\ParticipantExporter::class => [
            'class' => App\Tournaments\Infrastructure\Yii\Persistence\YiiParticipantExporter::class,
        ],

        App\Tournaments\Application\TournamentRenderer::class => [
            'class' => App\Tournaments\Presentation\Yii\Renderers\HtmlTournamentRenderer::class,
        ],

        App\Tournaments\Application\PlayerReadRepository::class => static function(Container $container) {
            return new App\Tournaments\Infrastructure\Clients\ClientPlayerRepository();
        }
    ],
];