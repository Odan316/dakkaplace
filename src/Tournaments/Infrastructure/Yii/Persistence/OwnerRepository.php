<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Tournaments\Application\OwnerExporter;
use App\Tournaments\Application\OwnerReadRepository;
use App\Tournaments\Domain\Owner;
use common\models\user\User as UserAR;

final class OwnerRepository implements OwnerReadRepository
{
    private OwnerExporter $exporter;

    public function __construct(OwnerExporter $exporter)
    {
        $this->exporter = $exporter;
    }

    public function get(int $id): Owner
    {
        $activeRecord = UserAR::findOne($id);

        return $this->exporter->export($activeRecord);
    }
}