<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Tournaments\Application\OwnerExporter;
use App\Tournaments\Domain\Owner;
use common\models\user\User;

final class YiiOwnerExporter implements OwnerExporter
{
    /**
     * @param User $externalEntity
     * @return Owner
     */
    public function export(mixed $externalEntity): Owner
    {
        return new Owner($externalEntity->id, $externalEntity->username);
    }
}