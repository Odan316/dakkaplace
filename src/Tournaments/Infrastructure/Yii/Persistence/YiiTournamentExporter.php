<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Tournaments\Application\ExporterException;
use App\Tournaments\Application\ParticipantExporter;
use App\Tournaments\Application\TournamentExporter;
use App\Tournaments\Domain\GameState;
use App\Tournaments\Domain\GameSystem;
use App\Tournaments\Domain\Owner;
use App\Tournaments\Domain\Pair;
use App\Tournaments\Domain\PairsCollection;
use App\Tournaments\Domain\ParticipantCollection;
use App\Tournaments\Domain\ParticipantResult;
use App\Tournaments\Domain\ParticipantResultCollection;
use App\Tournaments\Domain\TeamType;
use App\Tournaments\Domain\Tour;
use App\Tournaments\Domain\Tournament;
use App\Tournaments\Domain\ToursCollection;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\GameSystem as GameSystemAR;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\Tournament as TournamentAR;
use common\models\user\User as UserAR;
use DateTimeImmutable;
use Exception;
use Ramsey\Uuid\Uuid;

final class YiiTournamentExporter implements TournamentExporter
{
    private ParticipantExporter $participantExporter;

    public function __construct(ParticipantExporter $participantExporter)
    {
        $this->participantExporter = $participantExporter;
    }

    /**
     * @param TournamentAR $externalEntity
     * @return Tournament
     * @throws ExporterException
     */
    public function export(mixed $externalEntity): Tournament
    {
        //TODO: if empty

        $participants = $this->exportParticipants($externalEntity);

        $tours = $this->exportTours($externalEntity);

        try {
            $createdAt = new DateTimeImmutable($externalEntity->createdAt);
        } catch (Exception $e) {
            throw new ExporterException("Date createdAt is invalid", 0, $e);
        }

        $tournament = Tournament::restore(
            Uuid::fromString($externalEntity->id),
            $externalEntity->number,
            $externalEntity->title,
            TeamType::getValueOf($externalEntity->teamType),
            $this->exportGameSystem($externalEntity->gameSystem),
            GameState::getValueOf($externalEntity->state),
            $this->exportOwner($externalEntity->owner),
            $participants,
            $tours,
            $createdAt,
        );

        try {
            $tournament->setStartAt(new DateTimeImmutable($externalEntity->startAt));
        } catch (Exception $e) {
            throw new ExporterException("Date startAt is invalid", 0, $e);
        }

        try {
            $tournament->setUpdatedAt(new DateTimeImmutable($externalEntity->updatedAt));
        } catch (Exception $e) {
            throw new ExporterException("Date updatedAt is invalid", 0, $e);
        }

        return $tournament;
    }

    private function exportGameSystem(?GameSystemAR $externalGameSystem): GameSystem
    {
        //TODO: if empty
        return new GameSystem($externalGameSystem->id, $externalGameSystem->title);
    }

    private function exportOwner(UserAR $owner): Owner
    {
        //TODO: if empty
        return new Owner($owner->id, $owner->username);
    }

    private function exportParticipants(TournamentAR $externalEntity): ParticipantCollection
    {
        $participantsARs = $externalEntity->participants;
        $participants = new ParticipantCollection();
        foreach ($participantsARs as $participantsAR) {
            $participants->add($this->participantExporter->export($participantsAR));
        }

        return $participants;
    }

    private function exportTours(TournamentAR $externalEntity): ToursCollection
    {
        $tours = new ToursCollection();
        for ($tourNum = 1; $tourNum <= $externalEntity->toursAmount; $tourNum++) {
            $tourPairsAR = $externalEntity->getTourResults()->forTour($tourNum)->byPair()->all();;
            $pairs = new PairsCollection();

            $lastPairNum = 0;
            $participantsResults = new ParticipantResultCollection();
            foreach ($tourPairsAR as $tourPairAR) {
                if ($tourPairAR->pairNum != $lastPairNum) {
                    if (!$participantsResults->isEmpty()) {
                        $pair = new Pair($lastPairNum, $participantsResults);
                        $pairs->addPair($pair);
                    }

                    $lastPairNum = $tourPairAR->pairNum;
                    $participantsResults = new ParticipantResultCollection();
                }

                $participantsResults->add(new ParticipantResult($tourPairAR->userId, $tourNum, $tourPairAR->result));
            }

            if (!$participantsResults->isEmpty()) {
                $pair = new Pair($lastPairNum, $participantsResults);
                $pairs->addPair($pair);
            }

            $tours->addTour(new Tour($tourNum, $pairs));
        }

        return $tours;
    }
}