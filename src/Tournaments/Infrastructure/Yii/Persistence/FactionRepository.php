<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Common\Presentation\Yii\ErrorsRenderer;
use App\Tournaments\Application\FactionExporter;
use App\Tournaments\Application\FactionReadRepository;
use App\Tournaments\Application\FactionWriteRepository;
use App\Tournaments\Application\RepositoryException;
use App\Tournaments\Domain\Faction;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\Faction as FactionAR;

final class FactionRepository implements FactionReadRepository, FactionWriteRepository
{
    private FactionExporter $exporter;
    private ErrorsRenderer $errorsRenderer;

    public function __construct(
        FactionExporter $exporter,
        ErrorsRenderer $errorsRenderer
    ) {
        $this->exporter = $exporter;
        $this->errorsRenderer = $errorsRenderer;
    }

    /**
     * @return array<Faction>
     */
    public function getAll(): array
    {
        $activeRecords = FactionAR::find()->all();

        $exporter = $this->exporter;
        return array_map(
            static function (FactionAR $item) use ($exporter) {
                return $exporter->export($item);
            },
            $activeRecords
        );
    }

    public function get(int $id): Faction
    {
        $activeRecord = FactionAR::findOne($id);

        return $this->exporter->export($activeRecord);
    }

    public function getNextId(): int
    {
        return FactionAR::find()->count() + 1;
    }

    public function add(Faction $faction): void
    {
        $activeRecord = new FactionAR();
        $activeRecord->title = $faction->getTitle();
        $activeRecord->gameSystemId = $faction->getGameSystem()->getId();

        if (!$activeRecord->validate()) {
            throw new RepositoryException($this->errorsRenderer->validationErrorsRender($activeRecord->getErrors()));
        }

        $activeRecord->save();
    }

    public function update(Faction $faction): void
    {
        $activeRecord = FactionAR::findOne($faction->getId());

        $activeRecord->title = $faction->getTitle();
        $activeRecord->gameSystemId = $faction->getGameSystem()->getId();

        if (!$activeRecord->validate()) {
            throw new RepositoryException($this->errorsRenderer->validationErrorsRender($activeRecord->getErrors()));
        }

        $activeRecord->save();
    }
}