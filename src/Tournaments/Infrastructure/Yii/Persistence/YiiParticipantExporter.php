<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Tournaments\Application\ExporterException;
use App\Tournaments\Application\FactionExporter;
use App\Tournaments\Application\ParticipantExporter;
use App\Tournaments\Domain\Participant;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\TournamentParticipant as ParticipantAR;
use DateTimeImmutable;
use Exception;

final class YiiParticipantExporter implements ParticipantExporter
{
    private FactionExporter $factionExporter;

    public function __construct(FactionExporter $factionExporter)
    {
        $this->factionExporter = $factionExporter;
    }

    /**
     * @param ParticipantAR $externalEntity
     * @return Participant
     * @throws ExporterException
     */
    public function export(mixed $externalEntity): Participant
    {
        try {
            $appliedAt = new DateTimeImmutable($externalEntity->createdAt);
        } catch (Exception $e) {
            throw new ExporterException("Date createdAt is invalid", 0, $e);
        }

        return new Participant(
            $externalEntity->userId,
            $externalEntity->user->username,
            $this->factionExporter->export($externalEntity->faction),
            $appliedAt
        );
    }
}