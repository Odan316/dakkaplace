<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Tournaments\Application\GameSystemExporter;
use App\Tournaments\Domain\GameSystem;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\GameSystem as GameSystemAR;

final class YiiGameSystemExporter implements GameSystemExporter
{
    /**
     * @param array<GameSystemAR> $externalEntity
     * @return GameSystem
     */
    public function export(mixed $externalEntity): GameSystem
    {
        return new GameSystem($externalEntity->id, $externalEntity->title);
    }
}