<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Tournaments\Domain\Tournament;

final class YiiTournamentARMapper implements TournamentARMapper
{
    public function map(Tournament $tournament): array
    {
        return [
            'id' => $tournament->getId()->toString(),
            'title' => $tournament->getTitle(),
            'state' => $tournament->getState()->getRawValue(),
            'teamType' => $tournament->getTeamType()->getRawValue(),
            'ownerId' => $tournament->getOwner()->getUserId(),
            'gameSystemId' => $tournament->getGameSystem()->getId(),
            'startAt' => !is_null($tournament->getStartAt()) ? $tournament->getStartAt()->format('Y-m-d H:i') : null,
            'createdAt' => !is_null($tournament->getStartAt()) ? $tournament->getCreatedAt()->format('Y-m-d H:i') : null,
            'updatedAt' => !is_null($tournament->getStartAt()) ? $tournament->getUpdatedAt()->format('Y-m-d H:i') : null,
        ];
    }
}