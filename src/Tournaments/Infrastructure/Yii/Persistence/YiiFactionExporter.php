<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Tournaments\Application\FactionExporter;
use App\Tournaments\Application\GameSystemExporter;
use App\Tournaments\Domain\Faction;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\Faction as FactionAR;

final class YiiFactionExporter implements FactionExporter
{
    private YiiGameSystemExporter $gameSystemExporter;

    public function __construct(GameSystemExporter $gameSystemExporter)
    {
        $this->gameSystemExporter = $gameSystemExporter;
    }

    /**
     * @param array<FactionAR> $externalEntity
     * @return Faction
     */
    public function export(mixed $externalEntity): Faction
    {
        return new Faction($externalEntity->id, $externalEntity->title, $this->gameSystemExporter->export($externalEntity->gameSystem));
    }
}