<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Common\Presentation\Yii\ErrorsRenderer;
use App\Tournaments\Application\GameSystemExporter;
use App\Tournaments\Application\GameSystemReadRepository;
use App\Tournaments\Application\GameSystemWriteRepository;
use App\Tournaments\Application\RepositoryException;
use App\Tournaments\Domain\GameSystem;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\GameSystem as GameSystemAR;

final class GameSystemRepository implements GameSystemReadRepository, GameSystemWriteRepository
{
    private GameSystemExporter $exporter;
    private ErrorsRenderer $errorsRenderer;

    public function __construct(
        GameSystemExporter $exporter,
        ErrorsRenderer $errorsRenderer
    ) {
        $this->exporter = $exporter;
        $this->errorsRenderer = $errorsRenderer;
    }

    /**
     * @return array<GameSystem>
     */
    public function getAll(): array
    {
        $activeRecords = GameSystemAR::find()->all();

        $exporter = $this->exporter;
        return array_map(
            static function (GameSystemAR $item) use ($exporter) {
                return $exporter->export($item);
            },
            $activeRecords
        );
    }

    public function get(int $id): GameSystem
    {
        $activeRecord = GameSystemAR::findOne($id);

        return $this->exporter->export($activeRecord);
    }

    /**
     * @deprecated
     * TODO: remake IDs on UUIDs
     */
    public function getNextId(): int
    {
        return GameSystemAR::find()->count() + 1;
    }

    /**
     * @throws RepositoryException
     */
    public function add(GameSystem $gameSystem): void
    {
        $activeRecord = new GameSystemAR();
        $activeRecord->title = $gameSystem->getTitle();

        if (!$activeRecord->validate()) {
            throw new RepositoryException($this->errorsRenderer->validationErrorsRender($activeRecord->getErrors()));
        }

        $activeRecord->save();
    }

    public function update(GameSystem $gameSystem): void
    {
        $activeRecord = GameSystemAR::findOne($gameSystem->getId());
        $activeRecord->title = $gameSystem->getTitle();

        if (!$activeRecord->validate()) {
            throw new RepositoryException($this->errorsRenderer->validationErrorsRender($activeRecord->getErrors()));
        }

        $activeRecord->save();
    }
}