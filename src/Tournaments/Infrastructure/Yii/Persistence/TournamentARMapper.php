<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Tournaments\Domain\Tournament;

interface TournamentARMapper
{
    public function map(Tournament $tournament): array;
}