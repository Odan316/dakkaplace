<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Common\Presentation\Yii\ErrorsRenderer;
use App\Tournaments\Application\ExporterException;
use App\Tournaments\Application\RepositoryException;
use App\Tournaments\Application\TournamentExporter;
use App\Tournaments\Application\TournamentReadRepository;
use App\Tournaments\Application\TournamentWriteRepository;
use App\Tournaments\Domain\GameState;
use App\Tournaments\Domain\Pair;
use App\Tournaments\Domain\Participant;
use App\Tournaments\Domain\Tour;
use App\Tournaments\Domain\Tournament;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\Tournament as TournamentAR;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\TournamentParticipant as ParticipantAR;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\TourResult as TourResultAR;
use DateTimeImmutable;
use Ramsey\Uuid\UuidInterface;
use yii\db\Expression;

final class TournamentRepository implements TournamentReadRepository, TournamentWriteRepository
{
    private TournamentExporter $exporter;
    private TournamentARMapper $toARMapper;
    private ErrorsRenderer $errorsRenderer;

    public function __construct(
        TournamentExporter $exporter,
        TournamentARMapper $toARMapper,
        ErrorsRenderer $errorsRenderer
    ) {
        $this->exporter = $exporter;
        $this->toARMapper = $toARMapper;
        $this->errorsRenderer = $errorsRenderer;
    }


    public function get(UuidInterface $id): ?Tournament
    {
        $activeRecord = TournamentAR::findOne($id);

        return $this->exporter->export($activeRecord);
    }

    public function getByNumber(int $number): ?Tournament
    {
        $activeRecord = TournamentAR::find()->andWhere(['number' => $number])->one();

        return $this->exporter->export($activeRecord);
    }

    /**
     * @return array<Tournament>
     */
    public function getActive(): array
    {
        $states = array_map(function (GameState $state) {
            return $state->getRawValue();
        }, Tournament::activeStates());

        $activeRecords = TournamentAR::find()->andWhere(['state' => $states])->all();

        return $this->exportList($activeRecords);
    }

    public function getActiveByOwner(int $userId): array
    {
        $states = array_map(function (GameState $state) {
            return $state->getRawValue();
        }, Tournament::activeStates());

        $activeRecords = TournamentAR::find()
            ->andWhere(['state' => $states])
            ->andWhere(['ownerId' => $userId])
            ->all();

        return $this->exportList($activeRecords);
    }

    public function getActiveByParticipant(int $userId): array
    {
        $states = array_map(function (GameState $state) {
            return $state->getRawValue();
        }, Tournament::activeStates());

        $activeRecords = TournamentAR::find()
            ->andWhere(['state' => $states])
            ->andWhere("0=1")
            ->all();

        return $this->exportList($activeRecords);
    }

    /**
     * @param Tournament $tournament
     * @throws RepositoryException
     */
    public function add(Tournament $tournament): void
    {
        // TODO: transaction
        $activeRecord = new TournamentAR();
        $activeRecord->setAttributes($this->toARMapper->map($tournament), false);
        $activeRecord->number = new Expression("(SELECT counter FROM (SELECT COUNT(t.id)+1 As counter FROM `tournament` t) AS t_count)");
        $activeRecord->createdAt = new Expression("CURRENT_TIMESTAMP()");
        $activeRecord->updatedAt = new Expression("CURRENT_TIMESTAMP()");

        if (!$activeRecord->validate()) {
            throw new RepositoryException($this->errorsRenderer->validationErrorsRender($activeRecord->getErrors()));
        }

        $activeRecord->save();
    }

    public function update(Tournament $tournament): void
    {
        // TODO: transaction
        $activeRecord = TournamentAR::findOne($tournament->getId());
        $activeRecord->setAttributes($this->toARMapper->map($tournament), false);
        $activeRecord->updatedAt = new Expression("CURRENT_TIMESTAMP()");

        if (!$activeRecord->validate()) {
            throw new RepositoryException($this->errorsRenderer->validationErrorsRender($activeRecord->getErrors()));
        }

        $activeRecord->save();

        foreach ($tournament->getParticipants()->getAll() as $participant) {
            $participantAR = ParticipantAR::find()->exact($tournament->getId(), $participant->getUserId())->one();
            if (empty($participantAR)) {
                $participantAR = new ParticipantAR();
                $participantAR->tournamentId = $tournament->getId()->toString();
                $participantAR->userId = $participant->getUserId();
                $participantAR->createdAt = (new DateTimeImmutable())->format('Y-m-d H:i:s');
            }
            $participantAR->factionId = $participant->getFaction()->getId();
            $participantAR->updatedAt = (new DateTimeImmutable())->format('Y-m-d H:i:s');

            if (!$participantAR->validate()) {
                throw new RepositoryException($this->errorsRenderer->validationErrorsRender($participantAR->getErrors()));
            }

            $participantAR->save();
        }

        $this->updatePairs($tournament);
    }

    private function getTourResultAR(
        array $tourResultARs,
        Tournament $tournament,
        Tour $tour,
        Pair $pair,
        Participant $participant
    ): TourResultAR {
        foreach ($tourResultARs as $tourResultAR) {
            if ($tourResultAR->tourNum = $tour->getNum()
                && $tourResultAR->pairNum === $pair->getNum()
                && $tourResultAR->userId === $participant->getUserId()) {
                return $tourResultAR;
            }
        }

        $tourResultAR = new TourResultAR();
        $tourResultAR->tournamentId = $tournament->getId()->toString();
        $tourResultAR->tourNum = $tour->getNum();
        $tourResultAR->pairNum = $pair->getNum();
        $tourResultAR->userId = $participant->getUserId();

        return $tourResultAR;
    }

    /**
     * @param array<TournamentAR> $activeRecords
     * @return array<Tournament>
     * @throws ExporterException
     */
    private function exportList(array $activeRecords): array
    {
        $exporter = $this->exporter;
        return array_map(
            static function (TournamentAR $item) use ($exporter) {
                return $exporter->export($item);
            },
            $activeRecords
        );
    }

    private function updatePairs(Tournament $tournament): void
    {
        foreach ($tournament->getTours()->getAll() as $tour) {
            $tourResultARs = TourResultAR::find()
                ->forTournament($tournament->getId())
                ->forTour($tour->getNum())
                ->byPair()
                ->all();

            $participantsInDb = [];
            foreach ($tourResultARs as $tourResultAR) {
                $participantsInDb[$tourResultAR->userId] = $tourResultAR;
            }

            $savedParticipants = [];
            foreach ($tour->getPairs()->getAll() as $pair) {
                foreach ($pair->getPlayers()->getAll() as $participantResult) {
                    if (array_key_exists($participantResult->getParticipantId(), $participantsInDb)) {
                        $tourResultAR = $participantsInDb[$participantResult->getParticipantId()];
                    } else {
                        $tourResultAR = new TourResultAR();
                        $tourResultAR->tournamentId = $tournament->getId()->toString();;
                        $tourResultAR->tourNum = $tour->getNum();
                        $tourResultAR->userId = $participantResult->getParticipantId();
                    }

                    $tourResultAR->pairNum = $pair->getNum();
                    $tourResultAR->result = $participantResult->getResult();

                    if (!$tourResultAR->validate()) {
                        throw new RepositoryException($this->errorsRenderer->validationErrorsRender($tourResultAR->getErrors()));
                    }

                    if (!$tourResultAR->save()) {
                        throw new RepositoryException($this->errorsRenderer->validationErrorsRender($tourResultAR->getErrors()));
                    }
                    $savedParticipants[] = $tourResultAR->userId;
                }
            }

            foreach($participantsInDb as $tourResultAR){
                if(!in_array($tourResultAR->userId, $savedParticipants)){
                    $tourResultAR->delete();
                }
            }
        }
    }
}