<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\Persistence;

use App\Tournaments\Application\ParticipantWriteRepository;
use App\Tournaments\Domain\Participant;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\Tournament as TournamentAR;

final class ParticipantRepository implements ParticipantWriteRepository
{
    public function save(Participant $participant)
    {
        $isNew = TournamentAR::find()->exact();
    }
}