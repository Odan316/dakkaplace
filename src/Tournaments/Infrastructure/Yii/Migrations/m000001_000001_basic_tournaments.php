<?php

use console\components\SchemaHelper;

class m000001_000001_basic_tournaments extends \yii\db\Migration
{
    /**
     * @return bool|void
     * @throws Exception
     */
    public function up()
    {
        $tableOptions = SchemaHelper::getTableOptions($this->db->driverName);
        
        $this->createTable('tournament', [
            'id' => $this->string(36)->notNull(),
            'number' => $this->integer()->notNull(),
            'createdAt' => $this->timestamp()->notNull()->defaultValue(new yii\db\Expression('CURRENT_TIMESTAMP()')),
            'updatedAt' => $this->timestamp()->notNull()->defaultValue(new yii\db\Expression('CURRENT_TIMESTAMP()')),
            'startAt' => $this->timestamp()->null(),
            'title' => $this->string(64)->notNull(),
            'regulations' => $this->text(),
            'description' => $this->text(),
            'state' => $this->integer()->notNull(),
            'ownerId' => $this->integer()->notNull(),
            'gameSystemId' => $this->integer()->notNull(),
            'teamType' => $this->integer()->notNull(),
            'teamSize' => $this->integer(),
            'isPublic' => $this->tinyInteger()->notNull()->defaultValue(1),
            'armyPointsLimit' => $this->integer(),
            'armyPPLimit' => $this->integer(),
            'paintingRequirements' => $this->integer()->notNull()->defaultValue(0),
            'toursAmount' => $this->integer()->notNull()->defaultValue(1),
            'daysLength' => $this->integer()->notNull()->defaultValue(1),
            'armyListsPublic' => $this->tinyInteger()->notNull()->defaultValue(1),
        ], $tableOptions);
        $this->addPrimaryKey('PK_tournament', 'tournament', 'id');
        $this->createIndex('UX_tournament_number', 'tournament', 'number', true);
        $this->createIndex('IX_tournament_ownerId', 'tournament', 'ownerId');
        
        $this->createTable('tournament_game_system', [
            'id' => $this->primaryKey(),
            'title' => $this->string(64)->notNull()
        ], $tableOptions);
        
        $this->createTable('tournament_game_faction', [
            'id' => $this->primaryKey(),
            'gameSystemId' => $this->integer()->notNull(),
            'title' => $this->string(64)->notNull()
        ], $tableOptions);
        
        $this->createTable('tournament_participant', [
            'tournamentId' => $this->string(36)->notNull(),
            'userId' => $this->integer()->notNull(),
            'factionId' => $this->integer()->notNull(),
            'result' => $this->integer(),
            'createdAt' => $this->timestamp()->notNull()->defaultValue(new yii\db\Expression('CURRENT_TIMESTAMP()')),
            'updatedAt' => $this->timestamp()->notNull()->defaultValue(new yii\db\Expression('CURRENT_TIMESTAMP()')),
        ], $tableOptions);

        $this->addPrimaryKey('PK_tournament_participants', 'tournament_participant', ['tournamentId', 'userId']);
        
        $this->createTable('tournament_tour_result', [
            'tournamentId' => $this->string(36)->notNull(),
            'userId' => $this->integer()->notNull()->defaultValue(0),
            'tourNum' => $this->integer()->notNull(),
            'pairNum' => $this->integer()->notNull(),
            'result' => $this->integer()->defaultValue(null)
        ], $tableOptions);
        $this->createIndex('UX_tour_pair',
            'tournament_tour_result',
            ['tournamentId', 'userId', 'tourNum'], true);
        
        $this->createTable('tournament_award', [
            'id' => $this->primaryKey(),
            'title' => $this->string(64)->notNull()
        ], $tableOptions);
        
        $this->createTable('tournament_award_map', [
            'tournamentId' => $this->string(36)->notNull(),
            'awardId' => $this->integer()->notNull()
        ], $tableOptions);
        $this->addPrimaryKey('PK_tournament_award_map', 'tournament_award_map', ['tournamentId', 'awardId']);
        
        $this->createTable('tournament_user_award_map', [
            'tournamentId' => $this->string(36)->notNull(),
            'userId' => $this->integer()->notNull(),
            'awardId' => $this->integer()->notNull()
        ], $tableOptions);
        $this->addPrimaryKey('PK_tournament_user_award_map',
            'tournament_user_award_map',
            ['tournamentId', 'userId', 'awardId']);
    }
    
    /**
     * @return bool|void
     */
    public function down()
    {
        $this->dropTable('tournament');
        $this->dropTable('tournament_game_system');
        $this->dropTable('tournament_game_faction');
        $this->dropTable('tournament_participant');
        $this->dropTable('tournament_tour_result');
        
        $this->dropTable('tournament_award');
        $this->dropTable('tournament_award_map');
        $this->dropTable('tournament_user_award_map');
    }
}
