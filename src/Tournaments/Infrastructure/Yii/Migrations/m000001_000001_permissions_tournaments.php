<?php

use common\modules\tournament\rbac\TournamentEditRule;

class m000001_000001_permissions_tournaments extends \yii\db\Migration
{
    /**
     * @return bool|void
     * @throws Exception
     */
    public function up()
    {
        $auth = Yii::$app->authManager;
        $adminRole = $auth->getRole('admin');
        $customerRole = $auth->getRole('customer');
        $vipRole = $auth->getRole('vip_customer');

        $perm = $auth->createPermission('backend/tournaments/view');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $perm = $auth->createPermission('backend/tournaments/edit');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);

        $perm = $auth->createPermission('frontend/tournaments/create');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $auth->addChild($customerRole, $perm);
        $auth->addChild($vipRole, $perm);

        $rule = new TournamentEditRule();
        $auth->add($rule);
        $perm = $auth->createPermission('frontend/tournaments/edit');
        $perm->ruleName = $rule->name;
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $auth->addChild($customerRole, $perm);
        $auth->addChild($vipRole, $perm);

        $perm = $auth->createPermission('frontend/tournaments/apply');
        $auth->add($perm);
        $auth->addChild($adminRole, $perm);
        $auth->addChild($customerRole, $perm);
        $auth->addChild($vipRole, $perm);
    }

    /**
     * @return bool|void
     */
    public function down()
    {
        $auth = Yii::$app->authManager;

        $perm = $auth->getPermission('backend/tournaments/view');
        $auth->remove($perm);
        $perm = $auth->getPermission('backend/tournaments/edit');
        $auth->remove($perm);

        $perm = $auth->getPermission('frontend/tournaments/create');
        $auth->remove($perm);
        $perm = $auth->getPermission('frontend/tournaments/edit');
        $auth->remove($perm);
        $rule = new TournamentEditRule();
        $auth->remove($rule);
        $perm = $auth->getPermission('frontend/tournaments/apply');
        $auth->remove($perm);
    }
}
