<?php

namespace App\Tournaments\Infrastructure\Yii\ActiveRecords;

use App\Tournaments\Infrastructure\Yii\ActiveQueries\TournamentQuery;
use App\Tournaments\Infrastructure\Yii\ActiveQueries\TourResultQuery;
use common\models\user\User;
use common\models\user\UserQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $tournamentId
 * @property int $userId
 * @property int $tourNum
 * @property int $pairNum
 * @property int $result
 *
 * @property Tournament $tournament
 * @property User $user
 */
class TourResult extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'tournament_tour_result';
    }

    public function rules(): array
    {
        return [
            [['tournamentId', 'tourNum', 'pairNum'], 'required'],
            [['tournamentId'], 'string'],
            [['userId', 'tourNum', 'pairNum', 'result'], 'integer'],
        ];
    }

    public function getTournament(): TournamentQuery | ActiveQuery
    {
        return $this->hasOne(Tournament::class, ['id' => 'tournamentId']);
    }

    public function getUser(): UserQuery | ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'userId']);
    }

    public static function find(): TourResultQuery
    {
        return new TourResultQuery(get_called_class());
    }
}
