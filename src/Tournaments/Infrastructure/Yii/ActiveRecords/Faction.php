<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\ActiveRecords;

use App\Tournaments\Infrastructure\Yii\ActiveQueries\FactionQuery;
use App\Tournaments\Infrastructure\Yii\ActiveQueries\GameSystemQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $gameSystemId
 * @property string $title
 *
 * @property GameSystem $gameSystem
 */
class Faction extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'tournament_game_faction';
    }

    public function rules(): array
    {
        return [
            [['gameSystemId', 'title'], 'required'],
            [['gameSystemId'], 'integer'],
            [['title'], 'string', 'max' => 64],
            [
                ['gameSystemId'],
                'exist',
                'targetClass' => GameSystem::class,
                'targetAttribute' => ['gameSystemId' => 'id'],
            ],
        ];
    }

    public function getGameSystem(): GameSystemQuery | ActiveQuery
    {
        return $this->hasOne(GameSystem::class, ['id' => 'gameSystemId']);
    }

    public static function find(): FactionQuery
    {
        return new FactionQuery(get_called_class());
    }
}
