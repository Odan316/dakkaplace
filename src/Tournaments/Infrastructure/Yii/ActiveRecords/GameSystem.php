<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\ActiveRecords;

use App\Tournaments\Infrastructure\Yii\ActiveQueries\GameSystemQuery;
use App\Tournaments\Infrastructure\Yii\ActiveQueries\TournamentQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $title
 *
 * @property Tournament[] $tournaments
 */
class GameSystem extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'tournament_game_system';
    }

    public function rules(): array
    {
        return [
            [['title'], 'trim'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 64],
        ];
    }

    public function getTournaments(): TournamentQuery | ActiveQuery
    {
        return $this->hasMany(Tournament::class, ['gameSystemId' => 'id']);
    }

    public static function find(): GameSystemQuery
    {
        return new GameSystemQuery(get_called_class());
    }
}
