<?php

namespace App\Tournaments\Infrastructure\Yii\ActiveRecords;

use App\Tournaments\Domain\GameState;
use App\Tournaments\Domain\TeamType;
use App\Tournaments\Infrastructure\Yii\ActiveQueries\GameSystemQuery;
use App\Tournaments\Infrastructure\Yii\ActiveQueries\TournamentParticipantQuery;
use App\Tournaments\Infrastructure\Yii\ActiveQueries\TournamentQuery;
use App\Tournaments\Infrastructure\Yii\ActiveQueries\TourResultQuery;
use common\models\user\User;
use common\models\user\UserQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property string $id
 * @property int $number
 * @property int $createdAt
 * @property int $updatedAt
 * @property string $title
 * @property string $regulations
 * @property string $description
 * @property int $state
 * @property int $ownerId
 * @property int $gameSystemId
 * @property int $teamType
 * @property boolean $isPublic
 * @property int $teamSize
 * @property int $armyPointsLimit
 * @property int $armyPPLimit
 * @property int $paintingRequirements
 * @property int $toursAmount
 * @property int $startAt
 * @property int $daysLength
 * @property boolean $armyListsPublic
 *
 * @property User $owner
 * @property GameSystem $gameSystem
 * @property TournamentParticipant[] $participants
 *
 * @property string $stateText
 * @property string $typeText
 * @property string $paintingRequirementsText
 */
class Tournament extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'tournament';
    }

    public function rules(): array
    {
        return [
            [['title', 'ownerId', 'gameSystemId', 'teamType', 'startAt'], 'required'],
            [
                [
                    'ownerId',
                    'gameSystemId',
                    'state',
                    'teamType',
                    'teamSize',
                    'armyPointsLimit',
                    'armyPPLimit',
                    'paintingRequirements',
                    'toursAmount',
                    'daysLength'
                ],
                'integer'
            ],
            [['number', 'startAt', 'createdAt', 'updatedAt'], 'safe'],
            [['title'], 'trim'],
            [['title'], 'string', 'max' => 64],
            [['regulations', 'description'], 'string'],
            [['state'], 'in', 'range' => array_keys(GameState::getValues())],
            [['teamType'], 'in', 'range' => array_keys(TeamType::getValues())],
            [['paintingRequirements'], 'in', 'range' => array_keys(self::getPaintingRequirements())],
            [['paintingRequirements'], 'default', 'value' => self::PAINT_REQ_NO],
            [['toursAmount', 'daysLength', 'isPublic', 'armyListsPublic'], 'default', 'value' => 1],
            [['isPublic', 'armyListsPublic'], 'boolean'],
            [
                ['ownerId'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['ownerId' => 'id']
            ],
            [
                ['gameSystemId'],
                'exist',
                'skipOnError' => true,
                'targetClass' => GameSystem::class,
                'targetAttribute' => ['gameSystemId' => 'id']
            ],
        ];
    }

    public function getOwner(): UserQuery | ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'ownerId']);
    }

    public function getGameSystem(): GameSystemQuery | ActiveQuery
    {
        return $this->hasOne(GameSystem::class, ['id' => 'gameSystemId']);
    }

    public function getParticipants(): TournamentParticipantQuery | ActiveQuery
    {
        return $this->hasMany(TournamentParticipant::class, ['tournamentId' => 'id']);
    }

    public function getTourResults(): TourResultQuery | ActiveQuery
    {
        return $this->hasMany(TourResult::class, ['tournamentId' => 'id']);
    }

    public static function find(): TournamentQuery
    {
        return new TournamentQuery(get_called_class());
    }

    /** @deprecated  */
    const PAINT_REQ_NO = 0;
    /** @deprecated  */
    const PAINT_REQ_PARTIAL = 10;
    /** @deprecated  */
    const PAINT_REQ_FULL = 20;

    /**
     * @deprecated
     */
    public static function getPaintingRequirements()
    {
        return [
            self::PAINT_REQ_NO => Yii::t('tournaments', 'No requirements'),
            self::PAINT_REQ_PARTIAL => Yii::t('tournaments', 'Partially painted armies allowed'),
            self::PAINT_REQ_FULL => Yii::t('tournaments', 'Only full-painted armies allowed'),
        ];
    }

    /**
     * @deprecated
     */
    public function getPaintingRequirementsText()
    {
        return self::getPaintingRequirements()[$this->paintingRequirements];
    }
}
