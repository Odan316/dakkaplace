<?php

namespace App\Tournaments\Infrastructure\Yii\ActiveRecords;

use App\Tournaments\Infrastructure\Yii\ActiveQueries\FactionQuery;
use App\Tournaments\Infrastructure\Yii\ActiveQueries\TournamentParticipantQuery;
use App\Tournaments\Infrastructure\Yii\ActiveQueries\TournamentQuery;
use common\models\user\User;
use common\models\user\UserQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $tournamentId
 * @property int $userId
 * @property int $factionId
 * @property int $result
 * @property string $createdAt
 * @property string $updatedAt
 *
 * @property Tournament $tournament
 * @property User $user
 * @property Faction $faction
 */
class TournamentParticipant extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'tournament_participant';
    }

    public function rules(): array
    {
        return [
            [['tournamentId', 'userId'], 'required'],
            [['tournamentId'], 'string'],
            [['userId', 'result'], 'integer'],
            [['tournamentId', 'userId'], 'unique', 'targetAttribute' => ['tournamentId', 'userId']],
        ];
    }

    public function getTournament(): TournamentQuery | ActiveQuery
    {
        return $this->hasOne(Tournament::class, ['id' => 'tournamentId']);
    }

    public function getUser(): UserQuery | ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'userId']);
    }

    public function getFaction(): FactionQuery | ActiveQuery
    {
        return $this->hasOne(Faction::class, ['id' => 'factionId']);
    }

    public static function find(): TournamentParticipantQuery
    {
        return new TournamentParticipantQuery(get_called_class());
    }
}
