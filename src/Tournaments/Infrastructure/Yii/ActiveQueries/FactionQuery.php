<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\ActiveQueries;

use App\Tournaments\Infrastructure\Yii\ActiveRecords\Faction;
use yii\db\ActiveQuery as ActiveQueryAlias;

/**
 * @method Faction[]|array all($db = null)
 * @method Faction|array|null one($db = null)
 */
class FactionQuery extends ActiveQueryAlias
{
}
