<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\ActiveQueries;

use App\Tournaments\Infrastructure\Yii\ActiveRecords\Tournament;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\TournamentParticipant;
use Ramsey\Uuid\UuidInterface;
use yii\db\ActiveQuery;

/**
 * @method Tournament[]|array all($db = null)
 * @method Tournament|array|null one($db = null)
 */
class TournamentQuery extends ActiveQuery
{

    public function hasId(UuidInterface $id): self
    {
        return $this->andWhere(['id' => $id->toString()]);
    }

    public function hasNumber(int $number): self
    {
        return $this->andWhere(['number' => $number]);
    }

    public function withOwner(int $userId): self
    {
        return $this->andWhere(['ownerId' => $userId]);
    }

    public function withParticipant(int $userId): self
    {
        $this->joinWith('participants')->andWhere([TournamentParticipant::tableName().'.userId' => $userId]);

        return $this;
    }
}
