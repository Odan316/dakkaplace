<?php

namespace App\Tournaments\Infrastructure\Yii\ActiveQueries;

use App\Tournaments\Infrastructure\Yii\ActiveRecords\TourResult;
use Ramsey\Uuid\UuidInterface;
use yii\db\ActiveQuery;

/**
 * @method TourResult[]|array all($db = null)
 * @method TourResult|array|null one($db = null)
 */
class TourResultQuery extends ActiveQuery
{
    public function forTour(int $tourNum): self
    {
        return $this->andWhere(['tourNum' => $tourNum]);
    }

    public function forTournament(UuidInterface $tournamentId): self
    {
        return $this->andWhere(['tournamentId' => $tournamentId->toString()]);
    }

    public function forPlayer(int $userId): self
    {
        return $this->andWhere(['userId' => $userId]);
    }

    public function exact(UuidInterface $tournamentId, int $tourNum, int $pairNum): self
    {
        return $this->andWhere([
            'tournamentId' => $tournamentId->toString(),
            'tourNum' => $tourNum,
            'pairNum' => $pairNum,
        ]);
    }

    public function byPair(): self
    {
        return $this->orderBy(['pairNum' => SORT_ASC]);
    }
}
