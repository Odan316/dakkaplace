<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii\ActiveQueries;

use App\Tournaments\Infrastructure\Yii\ActiveRecords\GameSystem;
use yii\db\ActiveQuery;

/**
 * @method GameSystem[]|array all($db = null)
 * @method GameSystem|array|null one($db = null)
 */
class GameSystemQuery extends ActiveQuery
{
}
