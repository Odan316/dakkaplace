<?php

namespace App\Tournaments\Infrastructure\Yii\ActiveQueries;

use App\Tournaments\Infrastructure\Yii\ActiveRecords\TournamentParticipant;
use Ramsey\Uuid\UuidInterface;

/**
 * This is the ActiveQuery class for [[TournamentParticipant]].
 *
 * @see TournamentParticipant
 */
class TournamentParticipantQuery extends \yii\db\ActiveQuery
{
    /**
     * @return TournamentParticipant[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @return TournamentParticipant|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $id
     * @return TournamentParticipantQuery
     */
    public function withTournament($id)
    {
        return $this->andWhere(['tournamentId' => $id]);
    }

    /**
     * @param $id
     * @return TournamentParticipantQuery
     */
    public function withUser($id)
    {
        return $this->andWhere(['userId' => $id]);
    }

    public function byUserId()
    {
        return $this->orderBy(['userId' => SORT_ASC]);
    }

    public function exact(UuidInterface $tournamentId, int $userId): self
    {
        return $this
            ->andWhere(['tournamentId' => $tournamentId->toString()])
            ->andWhere(['userId' => $userId]);
    }
}
