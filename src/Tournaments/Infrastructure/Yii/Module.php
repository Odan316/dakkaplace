<?php

declare(strict_types=1);

namespace App\Tournaments\Infrastructure\Yii;

use Yii;

class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'App\Tournaments\Presentation\Yii\Controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        Yii::setAlias('@Tournaments', '@App/Tournaments');

        Yii::$app->i18n->translations['tournaments*'] = [
            'class'          => 'yii\i18n\GettextMessageSource',
            'basePath'       => '@Tournaments/messages',
            'catalog'        => 'messages',
            'sourceLanguage' => 'en'
        ];

        if(Yii::$app->id == 'app-frontend') {
            $controllers = [
                'tournaments'
            ];

            $controllersList = implode('|', $controllers);
            Yii::$app->getUrlManager()->addRules([
                '<controller:(' . $controllersList . ')>'                   => 'tournaments/<controller>/index',
                '<controller:(' . $controllersList . ')>/<action>'          => 'tournaments/<controller>/<action>',
            ], false);
        }
    }
}
