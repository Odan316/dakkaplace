<?php

declare(strict_types = 1);

namespace App\Tournaments\Infrastructure\Clients;

use App\Tournaments\Application\PlayerReadRepository;
use App\Tournaments\Domain\Player;
use common\models\user\User;

/**
 * @deprecated
 */
final class ClientPlayerRepository implements PlayerReadRepository
{
    public function get(int $playerId): Player
    {
        $userAR = User::find()->hasId($playerId)->one();

        return new Player($userAR->id, $userAR->username);
    }
}