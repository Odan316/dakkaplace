$(document).ready(function () {
    //--- PAIRS ---//

    $(document).on('click', '.btnAutoPair', function () {
        $parentDiv = $(this).parents('.tourContent');
        $.post(
            $(this).data('url'),
            {},
            function (data) {
                $parentDiv.html(data.html);
            });
    });
    $(document).on('click', '.btnManualPair', function () {
        $parentDiv = $(this).parents('.tourContent');
        $.post(
            $(this).data('url'),
            {},
            function (data) {
                $parentDiv.html(data.html);
                bindFormValidation($(tourFormSelector), handleTourSaveSuccess);
            });
    });


    $(document).on('click', '.btnSaveManualPair', function () {
        $(tourFormSelector).trigger('submit');
    });

    $(document).on('click', '.btnEditPairResults', function () {
        $parentDiv = $(this).parents('.tourContent');
        $.post(
            $(this).data('url'),
            {},
            function (data) {
                $parentDiv.html(data.html);
                bindFormValidation($(tourFormSelector), handleTourSaveSuccess);
            });
    });


    $(document).on('click', '.btnSaveTourResults', function () {
        $(tourFormSelector).trigger('submit');
    });

});

var tourFormSelector = '#tour-form';
var $parentDiv;

function handleTourSaveSuccess(data) {
    if (data.result === 'success') {
        $parentDiv.html(data.html);
    } else {
        $parentDiv.html(data.html);
        bindFormValidation($(tourFormSelector), handleTourSaveSuccess);
    }
}