<?php

declare(strict_types=1);

namespace App\Tournaments\Presentation\Yii\Renderers;

use App\Tournaments\Application\TournamentRenderer;
use App\Tournaments\Domain\GameState;
use App\Tournaments\Domain\TeamType;
use Yii;

final class HtmlTournamentRenderer implements TournamentRenderer
{
    public function renderStateString(GameState $gameState): string
    {
        $map = [
            GameState::new()->getRawValue() => Yii::t('tournaments', 'New'),
            GameState::openToRegister()->getRawValue() => Yii::t('tournaments', 'Open to register'),
            GameState::registrationOver()->getRawValue() => Yii::t('tournaments', 'Registration is over'),
            GameState::ongoing()->getRawValue() => Yii::t('tournaments', 'Ongoing'),
            GameState::finished()->getRawValue() => Yii::t('tournaments', 'Finished'),
            GameState::closed()->getRawValue() => Yii::t('tournaments', 'Closed'),
            GameState::cancelled()->getRawValue() => Yii::t('tournaments', 'Cancelled'),
        ];

        return array_key_exists($gameState->getRawValue(), $map)
            ? $map[$gameState->getRawValue()]
            : Yii::t('tournaments', 'Unknown state');
    }

    public function renderTeamTypeString(TeamType $teamType): string
    {
        $map = [
            TeamType::single()->getRawValue() => Yii::t('tournaments', 'Single game'),
            TeamType::pair()->getRawValue() => Yii::t('tournaments', 'Pair game'),
        ];

        return array_key_exists($teamType->getRawValue(), $map)
            ? $map[$teamType->getRawValue()]
            : Yii::t('tournaments', 'Unknown type');
    }

    public function getTeamTypeListValues(): array
    {
        return [
            TeamType::single()->getRawValue() => Yii::t('tournaments', 'Single game'),
            TeamType::pair()->getRawValue() => Yii::t('tournaments', 'Pair game'),
        ];
    }
}