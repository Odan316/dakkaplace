<?php

namespace App\Tournaments\Presentation\Yii\Console;

use App\Tournaments\Domain\GameState;
use App\Tournaments\Domain\TeamType;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\Faction as FactionAR;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\GameSystem as GameSystemAR;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\Tournament;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\TournamentParticipant;
use common\models\user\Role;
use common\models\user\Status;
use common\models\user\User;
use Exception;
use Ramsey\Uuid\Uuid;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Expression;
use yii\helpers\VarDumper;

class FixturesController extends Controller
{
    /**
     * @param int $max
     * @return int
     * @throws \yii\base\Exception
     * @throws Exception
     */
    public function actionUsers($max = 30)
    {
        $auth = Yii::$app->authManager;

        $firstId = User::find()->count();

        $maxId = $firstId + $max;

        for ($i = $firstId + 1; $i <= $maxId; $i++) {
            $user = new User();
            $user->username = "test{$i}";
            $user->nickname = "Test User {$i}";
            $user->email = "test{$i}@dakkaplace.com";
            $user->status = Status::active();
            $user->generateConfirmationToken();
            $user->setPassword('111111');
            $user->generateAuthKey();


            if ($user->save()) {
                $role = $auth->getRole(Role::CUSTOMER_ROLE);
                $auth->assign($role, $user->getId());

                $this->stdout("User 'Test User {$i}' created \n");
            }
        }

        return ExitCode::OK;
    }

    public function actionDicts()
    {
        $gameSystems = [
            1 => 'Warhammer 40k 9ed',
            2 => 'Kill Team 2021',
        ];

        foreach ($gameSystems as $id => $title) {
            $gs = new GameSystemAR([
                'id' => $id,
                'title' => $title,
            ]);
            $gs->save();
            $this->stdout("Game system '{$title}' added \n");
        }

        $gameFractions = [
            ['gs' => 1, 'title' => 'Space Marines'],
            ['gs' => 1, 'title' => 'Chaos Space Marines'],
            ['gs' => 1, 'title' => 'Eldar'],
            ['gs' => 1, 'title' => 'Orks'],
            ['gs' => 1, 'title' => 'Tau'],
            ['gs' => 2, 'title' => 'Space Marines'],
            ['gs' => 2, 'title' => 'Chaos Space Marines'],
            ['gs' => 2, 'title' => 'Eldar'],
            ['gs' => 2, 'title' => 'Orks'],
            ['gs' => 2, 'title' => 'Tau'],
        ];

        foreach ($gameFractions as $data) {
            $gf = new FactionAR([
                'gameSystemId' => $data['gs'],
                'title' => $data['title'],
            ]);
            $gf->save();
            $this->stdout("Game faction '{$data['title']}' added \n");
        }
    }

    public function actionTours()
    {
        $owner = User::find()->withRole(Role::CUSTOMER_ROLE)->one();

        $tId = Tournament::find()->count() + 1;

        $tournament = new Tournament();
        $tournament->id = Uuid::uuid4()->toString();
        $tournament->number = $tId;
        $tournament->ownerId = $owner->id;
        $tournament->title = "Single {$tId}";
        $tournament->gameSystemId = 1;
        $tournament->state = GameState::registrationOver()->getRawValue();
        $tournament->teamType = TeamType::single()->getRawValue();
        $tournament->startAt = (new \DateTimeImmutable())->add(new \DateInterval('P3D'))->format('Y-m-d H:i');
        $tournament->createdAt = (new \DateTimeImmutable())->format('Y-m-d H:i:s');
        $tournament->updatedAt = (new \DateTimeImmutable())->format('Y-m-d H:i:s');

        $tournament->toursAmount = 3;

        if ($tournament->save()) {
            $this->stdout("Tournament '{$tournament->title}' created \n");
            $participants = User::find()->withRole(Role::CUSTOMER_ROLE)->limit(20)->orderBy(new Expression("RAND()"))->all();

            foreach ($participants as $user) {
                $tp = new TournamentParticipant();
                $tp->tournamentId = $tournament->id;
                $tp->factionId = rand(1, 10);
                $tp->userId = $user->id;
                $tp->createdAt = (new \DateTimeImmutable())->format('Y-m-d H:i');
                $tp->updatedAt = (new \DateTimeImmutable())->format('Y-m-d H:i');
                if ($tp->save()) {
                    $this->stdout("Participant '{$user->nickname}' added \n");
                } else {
                    VarDumper::dump($tp->getErrors());
                }
            }
        } else {
            VarDumper::dump($tournament->getErrors());
        }
    }
}