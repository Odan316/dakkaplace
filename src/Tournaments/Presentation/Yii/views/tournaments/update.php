<?php

declare(strict_types=1);

use App\Tournaments\Application\TournamentRenderer;
use App\Tournaments\Application\TournamentUpdateRequest;
use App\Tournaments\Domain\GameSystem;
use kartik\datetime\DateTimePicker;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var View $this
 * @var TournamentRenderer $tournamentRenderer
 * @var TournamentUpdateRequest $model
 * @var GameSystem[] $gameSystems
 */

$this->title = Yii::t('tournaments', 'Edit tournament');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tournaments', "Tournaments"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-update">

    <h1><?php echo Html::encode($this->title) ?></h1>


    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
    ]); ?>

    <?php echo $form->field($model, 'title')->textInput(['autofocus' => true]) ?>
    <?php echo $form->field($model, 'gameSystemId')
        ->dropDownList(ArrayHelper::map($gameSystems, function (GameSystem $gameSystem) {
            return $gameSystem->getId();
        }, function (GameSystem $gameSystem) {
            return $gameSystem->getTitle();
        }),
            ['disabled' => !$model->isNotReadyToStart()]) ?>
    <?php echo $form->field($model, 'teamType')
        ->dropDownList($tournamentRenderer->getTeamTypeListValues(), ['disabled' => !$model->isNotReadyToStart()]) ?>
    <?php //echo $form->field($model, 'teamSize')->textInput(['disabled' => !$model->canBeEdited()]) ?>
    <?php echo $form->field($model, 'startAt')->widget(DateTimePicker::class, [
        'options' => [
            'placeholder' => Yii::t('tournaments', 'Choose date and time'),
            'disabled' => !$model->isNotReadyToStart(),
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy hh:ii',
            'todayHighlight' => true,
        ],
    ]); ?>
    <?php //echo $form->field($model, 'regulations')->textarea() ?>
    <?php //echo $form->field($model, 'description')->textarea() ?>
    <?php //echo $form->field($model, 'isPublic')->checkbox(['checked ' => true]) ?>
    <?php //echo $form->field($model, 'armyListsPublic')->checkbox(['checked ' => true]) ?>
    <?php //echo $form->field($model, 'armyPointsLimit')->textInput(['disabled' => !$model->isNotReadyToStart()]) ?>
    <?php //echo $form->field($model, 'armyPPLimit')->textInput(['disabled' => !$model->isNotReadyToStart()]) ?>
    <?php /*echo $form->field($model, 'paintingRequirements')
    ->dropDownList(Tournament::getPaintingRequirements(), ['disabled' => !$model->canBeEdited()])*/ ?>
    <?php //echo $form->field($model, 'toursAmount')->textInput(['disabled' => !$model->isNotReadyToStart()]) ?>
    <?php //echo $form->field($model, 'daysLength')->textInput(['disabled' => !$model->isNotReadyToStart()]) ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('tournaments', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>