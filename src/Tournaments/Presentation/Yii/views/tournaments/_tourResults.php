<?php

declare(strict_types=1);

use App\Tournaments\Domain\Tour;
use App\Tournaments\Domain\Tournament;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var Tournament $tournament
 * @var Tour $tour
 */

?>

<div class="col-sm-12 col-md-6">
    <h2>
        <?php echo Yii::t('tournaments', 'Tour #{tourNum}', ['tourNum' => $tour->getNum()]) ?>
        <?php $pairs = $tour->getPairs();
        if ($tournament->isOngoing()) {
            if (!$pairs->areSet()) { ?>
                <?php echo Html::button(Yii::t('tournaments', "Autopair"),
                    [
                        'class' => 'btn btn-warning btn-sm btnAutoPair',
                        'data-url' => Url::to([
                            'tournaments/autopair',
                            'id' => $tournament->getNumber(),
                            'tour' => $tour->getNum(),
                        ]),
                    ]); ?>
                <?php echo Html::button(Yii::t('tournaments', "Manual pair"),
                    [
                        'class' => 'btn btn-warning btn-sm btnManualPair',
                        'data-url' => Url::to([
                            'tournaments/manual-pair',
                            'id' => $tournament->getNumber(),
                            'tour' => $tour->getNum(),
                        ]),
                    ]); ?>
            <?php } else { ?>
                <?php echo Html::button(Yii::t('tournaments', "Edit pairs"),
                    [
                        'class' => 'btn btn-warning btn-sm btnManualPair',
                        'data-url' => Url::to([
                            'tournaments/manual-pair',
                            'id' => $tournament->getNumber(),
                            'tour' => $tour->getNum(),
                        ]),
                    ]); ?>
                <?php echo Html::button(Yii::t('tournaments', "Edit results"),
                    [
                        'class' => 'btn btn-warning btn-sm btnEditPairResults',
                        'data-url' => Url::to([
                            'tournaments/edit-tour-results',
                            'id' => $tournament->getNumber(),
                            'tour' => $tour->getNum(),
                        ]),
                    ]); ?>
            <?php }
        } ?>
    </h2>
    <table class="table table-sm mt-2">
        <colgroup>
            <col class="col-sm-5">
            <col class="col-sm-2">
            <col class="col-sm-5">
        </colgroup>
        <thead>
        <tr>
            <th class="text-right text-nowrap"><?php echo Yii::t('tournaments', 'Player 1') ?></th>
            <th class="text-center text-nowrap"><?php echo Yii::t('tournaments', 'Score') ?></th>
            <th class="text-left text-nowrap"><?php echo Yii::t('tournaments', 'Player 2') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($pairs->getAll() as $pair) { ?>
            <?php
            $participantResultOne = $pair->getPlayers()->getAll()[0];
            $participantResultTwo = $pair->getPlayers()->getAll()[1]; ?>
            <tr>
                <td class="text-right text-nowrap"><?php echo $this->render('_participantInTour', [
                        'tournament' => $tournament,
                        'participantResult' => $participantResultOne,
                    ]); ?></td>
                <td class="text-center text-nowrap">
                    <?php if (!is_null($participantResultOne->getResult())) { ?>
                        <?php echo $participantResultOne->getResult() . " : " . $participantResultTwo->getResult() ?>
                    <?php } else { ?>
                        <?php echo '-:-'; ?>
                    <?php } ?>
                </td>
                <td class="text-left text-nowrap"><?php echo $this->render('_participantInTour', [
                        'tournament' => $tournament,
                        'participantResult' => $participantResultTwo,
                    ]); ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>


