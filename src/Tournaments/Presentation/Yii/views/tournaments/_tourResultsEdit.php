<?php

declare(strict_types=1);

use App\Tournaments\Domain\Tournament;
use App\Tournaments\Presentation\Yii\Forms\TourResultsForm;
use yii\base\Model;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var Tournament $tournament
 * @var int $tourNum
 * @var TourResultsForm $tourResultsForm
 */

?>

<div class="col-sm-12 col-md-6">
    <h2>
        <?php echo Yii::t('tournaments', 'Tour #{tourNum}', ['tourNum' => $tourNum]) ?>
        <?php echo Html::button(Yii::t('tournaments', "Save"), [
            'class' => 'btn btn-success btn-sm btnSaveTourResults',
        ]); ?>
    </h2>
    <?php $form = ActiveForm::begin([
        'id' => 'tour-form',
        'action' => Url::to(['edit-tour-results', 'id' => $tournament->getNumber(), 'tour' => $tourNum]),
        'layout' => 'inline',
    ]); ?>
    <table class="table table-condensed">
        <colgroup>
            <col class="col-sm-4">
            <col class="col-sm-1">
            <col class="col-sm-1">
            <col class="col-sm-1">
            <col class="col-sm-4">
        </colgroup>
        <thead>
        <tr>
            <td class="text-right"><?php echo Yii::t('tournaments', 'Player 1') ?></td>
            <td colspan="3" class="text-center text-nowrap"><?php echo Yii::t('tournaments', 'Score') ?></td>
            <td class="text-left"><?php echo Yii::t('tournaments', 'Player 2') ?></td>
        </tr>
        </thead>
        <tbody>

        <tbody>
        <?php
        $pairs = $tournament->getTours()->getByNum($tourNum)->getPairs();
        foreach ($tourResultsForm->getTourResults() as $pairNum => $pairForm) { ?>
            <tr>
                <?php
                $pair = $pairs->getByNum($pairNum);
                $participantResultOne = $pair->getPlayers()->getAll()[0];
                $participantResultTwo = $pair->getPlayers()->getAll()[1];
                ?>
                <td class="text-right text-nowrap"><?php echo $this->render('_participantInTour', [
                        'tournament' => $tournament,
                        'participantResult' => $participantResultOne,
                    ]); ?></td>
                <td class="tourResultInputs">
                    <div class="form-group">
                        <?php echo Html::textInput(
                            Html::getInputName($tourResultsForm, "tourResults[{$pairNum}][{$pairForm->getPlayer1Id()}]"),
                            $pairForm->getPlayer1Result(),
                            ['class' => 'form-control form-control-sm input-xs'],
                        ); ?>
                    </div>
                </td>
                <td>
                    <div class="form-group">:</div>
                </td>
                <td class="tourResultInputs">
                    <div class="form-group">
                        <?php echo Html::textInput(
                            Html::getInputName($tourResultsForm, "tourResults[{$pairNum}][{$pairForm->getPlayer2Id()}]"),
                            $pairForm->getPlayer2Result(),
                            ['class' => 'form-control form-control-sm'],
                        ); ?>
                    </div>
                </td>
                <td class="text-left text-nowrap"><?php echo $this->render('_participantInTour', [
                        'tournament' => $tournament,
                        'participantResult' => $participantResultTwo,
                    ]); ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php ActiveForm::end(); ?>
</div>