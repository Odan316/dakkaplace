<?php
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use App\Tournaments\Application\TournamentRenderer;
use App\Tournaments\Domain\Tournament;
use yii\web\View;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\bootstrap4\Html;

/**
 * @var View $this
 * @var TournamentRenderer $tournamentRenderer
 * @var array<Tournament> $tournaments
 */

$this->title = Yii::t('tournaments', 'Current tournaments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournaments-index">

    <h1><?php echo $this->title ?></h1>

    <div class="form-group">
        <?php echo Html::a(Yii::t('tournaments', 'Create'),
            ['create'],
            [
                'class' => [
                    'btn btn-primary',
                    (!Yii::$app->user->can('frontend/tournaments/create') ? 'disabled' : ''),
                ],
                'title' => (!Yii::$app->user->can('frontend/tournaments/create')
                    ? Yii::t('tournaments', 'Register to create tournaments')
                    : Yii::t('tournaments', 'Create new tournament')),
            ]); ?>
    </div>

    <?php echo GridView::widget([
        'dataProvider' => new ArrayDataProvider([
            'allModels' => $tournaments,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]),
        'layout' => "{items}\n{pager}",
        'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
        'emptyText' => Yii::t('tournaments', 'No tournaments yet!'),
        'columns' => [
            [
                'label' => Yii::t('tournaments', 'Title'),
                'format' => 'html',
                'value' => function (Tournament $model) {
                    return Html::a($model->getTitle(), ['tournaments/view', 'id' => $model->getNumber()]);
                },
            ],
            [
                'label' => Yii::t('tournaments', 'Status'),
                'value' => function (Tournament $model) use ($tournamentRenderer): string {
                    return $tournamentRenderer->renderStateString($model->getState());
                },
            ],
            [
                'label' => Yii::t('tournaments', 'Game type'),
                'value' => function (Tournament $model) use ($tournamentRenderer): string {
                    return $tournamentRenderer->renderTeamTypeString($model->getTeamType());
                },
            ],
            [
                'label' => Yii::t('tournaments', 'Ruleset'),
                'value' => function (Tournament $model) {
                    return $model->getGameSystem()->getTitle();
                },
            ],
            [
                'label' => Yii::t('tournaments', 'GM'),
                'value' => function (Tournament $model) {
                    return $model->getOwner()->getUsername();
                },
            ],
            [
                'label' => Yii::t('tournaments', 'Start at'),
                'value' => function (Tournament $model) {
                    return $model->getStartAt()->format('d.m.Y');
                },
            ],
        ],
    ]); ?>

</div>
