<?php

declare(strict_types=1);

use App\Tournaments\Application\ApplyToTournamentRequest;
use App\Tournaments\Domain\Faction;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var View $this
 * @var ApplyToTournamentRequest $model
 * @var array<Faction> $gameFactions
 */

$this->title = Yii::t('tournaments', 'Apply to tournament');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tournaments', "Tournaments"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-apply">

    <h1><?php echo Html::encode($this->title); ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model, 'factionId')
        ->dropDownList(ArrayHelper::map($gameFactions, function (Faction $faction) {
            return $faction->getId();
        }, function (Faction $faction) {
            return $faction->getTitle();
        })); ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('tournaments', 'Apply'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
