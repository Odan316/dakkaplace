<?php
/** @noinspection PhpUnhandledExceptionInspection */

declare(strict_types=1);

use App\Tournaments\Application\TournamentRenderer;
use App\Tournaments\Domain\Participant;
use App\Tournaments\Domain\Tournament;
use App\Tournaments\Presentation\Yii\Assets\TournamentsAsset;
use yii\data\ArrayDataProvider;
use yii\web\View;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\bootstrap4\Html;

/**
 * @var View $this
 * @var TournamentRenderer $tournamentRenderer
 * @var Tournament $tournament
 */

TournamentsAsset::register($this);

$this->title = $tournament->getTitle();
$this->params['breadcrumbs'][] = ['label' => Yii::t('tournaments', "Tournaments"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tournament-view">

    <h1><?php echo Yii::t('tournaments', 'Tournament "{tournamentTitle}"', [
            'tournamentTitle' => Html::encode($this->title),
        ]); ?></h1>

    <?php echo $this->render('_topButtons', ['tournament' => $tournament]) ?>

    <div class="tournamentTabs">
        <ul class="nav nav-tabs" id="tournamentTabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="infoTab" data-toggle="tab" href="#info" role="tab"
                   aria-controls="info" aria-selected="false">
                    <?php echo Yii::t("tournaments", "Main info"); ?>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="participantsTab" data-toggle="tab" href="#participants" role="tab"
                   aria-controls="participants" aria-selected="false">
                    <?php echo Yii::t("tournaments", "Participants"); ?>
                </a>
            </li>
            <?php if ($tournament->hasStarted()) { ?>
                <li class="nav-item">
                    <a class="nav-link" id="toursTab" data-toggle="tab" href="#tours" role="tab"
                       aria-controls="tours" aria-selected="false">
                        <?php echo Yii::t("tournaments", "Tours results"); ?>
                    </a>
                </li>
            <?php } ?>
        </ul>

        <div class="tab-content" id="tournamentTabsContent">
            <div id="info" class="tab-pane fade show active pt-3" role="tabpanel" aria-labelledby="infoTab">
                <?php echo DetailView::widget([
                    'model' => $tournament,
                    'options' => ['class' => 'table table-striped table-bordered detail-view table-condensed'],
                    'attributes' => [
                        [
                            'label' => Yii::t('tournaments', '#'),
                            'attribute' => 'number',
                            'value' => function (Tournament $tournament) {
                                return $tournament->getNumber();
                            },
                            'captionOptions' => ['class' => 'col-md-2'],
                            'contentOptions' => ['class' => 'col-md-10'],
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Title'),
                            'value' => function (Tournament $tournament) {
                                return $tournament->getTitle();
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Status'),
                            'value' => function (Tournament $tournament) use ($tournamentRenderer) {
                                return $tournamentRenderer->renderStateString($tournament->getState());
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'GM'),
                            'format' => 'html',
                            'value' => function ($tournament) {
                                /** @var $tournament Tournament */
                                return Html::a($tournament->getOwner()->getUsername(), [
                                    'profile/view',
                                    'id' => $tournament->getOwner()->getUserId(),
                                ]);
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Ruleset'),
                            'value' => function (Tournament $tournament) {
                                return $tournament->getGameSystem()->getTitle();
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Game type'),
                            'value' => function (Tournament $tournament) use ($tournamentRenderer) {
                                return $tournamentRenderer->renderTeamTypeString($tournament->getTeamType());
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Start at'),
                            'value' => function (Tournament $tournament) {
                                return $tournament->getStartAt()->format('d.m.Y H:i');
                            },
                        ],
                        /*'typeText',
                        'regulations',
                        'description',
                        'isPublic:boolean',
                        'armyListsPublic:boolean',
                        'teamSize',
                        'armyPointsLimit',
                        'armyPPLimit',
                        'paintingRequirementsText',
                        'toursAmount',
                        'daysLength',*/
                    ],
                ]) ?>
            </div>
            <div id="participants" class="tab-pane fade pt-3" role="tabpanel" aria-labelledby="participantsTab">
                <?php $order = ($tournament->isFinished() ? ['result' => SORT_DESC] : ['createdAt' => SORT_DESC]) ?>

                <?php echo GridView::widget([
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => $tournament->getParticipants()->getAll(),
                        'pagination' => ['pageSize' => 50],
                        //'sort' => ['defaultOrder' => $order],
                    ]),
                    'layout' => "{items}\n{pager}",
                    'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
                    'emptyText' => Yii::t('tournaments', 'No participants yet!'),
                    'columns' => [
                        [
                            'label' => Yii::t('tournaments', 'Username'),
                            'value' => function (Participant $tournament) {
                                return $tournament->getUsername();
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Faction'),
                            'value' => function (Participant $tournament) {
                                return $tournament->getFaction()->getTitle();
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Applied At'),
                            'value' => function (Participant $tournament) {
                                return $tournament->getAppliedAt()->format('Y-m-d H:i');
                            },
                        ],
                        //'result',
                    ],
                ]); ?>
            </div>
            <?php if ($tournament->hasStarted()) { ?>
                <div id="tours" class="tab-pane fade pt-3" role="tabpanel" aria-labelledby="toursTab">
                    <?php if ($tournament->getToursAmount() > 0) { ?>
                        <?php foreach ($tournament->getTours()->getAll() as $tour) { ?>
                            <div class="row tourContent">
                                <?php echo $this->render('_tourResults', [
                                    'tournament' => $tournament,
                                    'tour' => $tour,
                                ]); ?>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="row">
                            <div class="col-md">
                                <?php echo Yii::t('tournaments', "Game isn't set properly and has no tours") ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
