<?php

declare(strict_types=1);

use App\Tournaments\Presentation\Yii\Forms\TourResultForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\web\View;

/**
 * @var View $this
 * @var TourResultForm[] $resultModels
 */

?>

<?php $form = ActiveForm::begin(); ?>

<table class="table">
    <colgroup>
        <col class="col-md-6"/>
        <col class="col-md-6"/>
    </colgroup>
    <thead>
    <tr>
        <th><?php echo Yii::t('tournaments', 'Player') ?></th>
        <th><?php echo Yii::t('tournaments', 'Result') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($resultModels as $model) { ?>
        <tr>
            <td><?php echo $model->getUser()->username ?></td>
            <td><?php echo $form->field($model, '[' . $model->userId . ']result')->label(false)->textInput() ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<div class="form-group">
    <?php echo Html::submitButton(Yii::t('tournaments', 'Save'), ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
