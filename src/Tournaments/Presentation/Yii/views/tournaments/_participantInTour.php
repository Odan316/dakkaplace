<?php

declare(strict_types=1);

use App\Tournaments\Domain\ParticipantResult;
use App\Tournaments\Domain\Tournament;
use yii\web\View;

/**
 * @var View $this
 * @var Tournament $tournament
 * @var ParticipantResult $participantResult
 */

?>

<?php if ($participantResult->getParticipantId() === 0) { ?>
    <span class="font-italic"><?php echo Yii::t('tournaments', 'Proxy bot'); ?></span>
<?php } else {
    $participant = $tournament->getParticipants()->getByUserId($participantResult->getParticipantId()); ?>
    <?php echo $participant->getUsername(); ?>
<?php } ?>