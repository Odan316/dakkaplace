<?php

declare(strict_types=1);

use App\Tournaments\Domain\Tournament;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var Tournament $tournament
 */
?>

<div class="form-group">
    <?php if (
        Yii::$app->user->can('frontend/tournaments/apply')
        && $tournament->isRegistrationOpen()
        //&& $tournament->getOwner()->getUserId() !== (int)Yii::$app->user->id
        && is_null($tournament->getParticipants()->getByUserId((int)Yii::$app->user->id))) { ?>
        <?php echo Html::a(Yii::t('tournaments', "Apply"),
            ['tournaments/apply', 'id' => $tournament->getNumber()],
            ['class' => 'btn btn-success']) ?>
    <?php } ?>

    <?php if (Yii::$app->user->can('frontend/tournaments/edit', ['tournamentId' => $tournament->getNumber()])) { ?>
        <?php echo Html::a(Yii::t('tournaments', "Edit"), [
            'tournaments/update',
            'id' => $tournament->getNumber(),
        ],
            ['class' => 'btn btn-primary']) ?>

        <?php if ($tournament->isNew() || $tournament->isRegistrationOver()) { ?>
            <?php echo Html::a(Yii::t('tournaments', "Open registration"),
                ['tournaments/open-registration', 'id' => $tournament->getNumber()],
                ['class' => 'btn btn-success']) ?>
        <?php } ?>
        <?php if ($tournament->isRegistrationOpen()) { ?>
            <?php echo Html::a(Yii::t('tournaments', "Close registration"),
                ['tournaments/close-registration', 'id' => $tournament->getNumber()],
                ['class' => 'btn btn-success']) ?>
        <?php } ?>
        <?php if ($tournament->isRegistrationOver()) { ?>
            <?php echo Html::a(Yii::t('tournaments', "Start game"),
                ['tournaments/start-game', 'id' => $tournament->getNumber()],
                ['class' => 'btn btn-success']) ?>
        <?php } ?>
        <?php if ($tournament->isOngoing()) { ?>
            <?php echo Html::a(Yii::t('tournaments', "Finalize"),
                ['tournaments/finalize', 'id' => $tournament->getNumber()],
                ['class' => 'btn btn-success']) ?>
        <?php } ?>
        <?php if ($tournament->isFinished()) { ?>
            <?php echo Html::a(Yii::t('tournaments', "Close"),
                ['tournaments/close-game', 'id' => $tournament->getNumber()],
                ['class' => 'btn btn-success']) ?>
        <?php } ?>
        <?php if ($tournament->canBeCancelled()) { ?>
            <?php echo Html::a(Yii::t('tournaments', "Cancel"),
                ['tournaments/cancel-game', 'id' => $tournament->getNumber()],
                ['class' => 'btn btn-danger']) ?>
        <?php } ?>
    <?php } ?>

</div>