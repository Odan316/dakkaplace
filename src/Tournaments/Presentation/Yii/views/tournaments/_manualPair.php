<?php

declare(strict_types=1);

use App\Tournaments\Application\Requests\TourPairingRequest;
use App\Tournaments\Domain\Participant;
use App\Tournaments\Domain\Tournament;
use yii\base\Model;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var Tournament $tournament
 * @var int $tourNum
 * @var TourPairingRequest|Model $pairingForm
 * @var Participant[] $availablePlayers
 */

?>

<div class="col-sm-12 col-md-6">
    <h2>
        <?php echo Yii::t('tournaments', 'Tour #{tourNum}', ['tourNum' => $tourNum]) ?>
        <?php echo Html::button(Yii::t('tournaments', "Save"), [
            'class' => 'btn btn-success btn-sm btnSaveManualPair',
        ]); ?>
    </h2>
    <?php $form = ActiveForm::begin([
        'id' => 'tour-form',
        'action' => Url::to(['manual-pair', 'id' => $tournament->getNumber(), 'tour' => $tourNum]),
        'layout' => 'inline',
    ]); ?>
    <table class="table table-condensed">
        <colgroup>
            <col class="col-sm-2">
            <col class="col-sm-6">
            <col class="col-sm-6">
        </colgroup>
        <thead>
        <tr>
            <td class="text-center"><?php echo Yii::t('tournaments', 'Player 1') ?></td>
            <td class="text-center"><?php echo Yii::t('tournaments', 'Player 2') ?></td>
        </tr>
        </thead>
        <tbody>

        <tbody>
        <?php
        $pairNum = 0;
        foreach ($pairingForm->getPairsRequests() as $pairForm) { ?>
            <tr>
                <td>
                    <div class="form-group">
                        <?php echo Html::dropDownList(
                            Html::getInputName($pairingForm, "pairs[{$pairNum}][0]"),
                            $pairForm->getPlayer1Id(),
                            $availablePlayers,
                            [
                                'class' => 'form-control form-control-sm',
                                'prompt' => Yii::t('tournaments', 'Proxy bot'),
                            ],
                        ); ?>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <?php echo Html::dropDownList(
                            Html::getInputName($pairingForm, "pairs[{$pairNum}][1]"),
                            $pairForm->getPlayer2Id(),
                            $availablePlayers,
                            [
                                'class' => 'form-control form-control-sm',
                                'prompt' => Yii::t('tournaments', 'Proxy bot'),
                            ],
                        ); ?>
                    </div>
                </td>
            </tr>
            <?php $pairNum++;
        } ?>
        </tbody>
    </table>
    <?php ActiveForm::end(); ?>
</div>

