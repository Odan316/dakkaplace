<?php

declare(strict_types=1);

namespace App\Tournaments\Presentation\Yii\Assets;

use yii\web\AssetBundle;

class TournamentsAsset extends AssetBundle
{
    public $sourcePath = '@App/Tournaments/Presentation/Yii/web';
    public $baseUrl = '@web';
    public $css = [
        //'css/tournaments.css',
    ];
    public $js = [
        'js/tournaments.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'common\assets\FontAwesomeAsset',
    ];
}