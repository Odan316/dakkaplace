<?php

declare(strict_types=1);

namespace App\Tournaments\Presentation\Yii\Widgets\MainPageWidget;

use App\Common\Presentation\Yii\Widgets\MainPageWidget as WidgetInterface;
use App\Tournaments\Application\TournamentReadRepository;
use App\Tournaments\Application\TournamentRenderer;
use yii\base\Widget;

final class MainPageWidget extends Widget implements WidgetInterface
{
    private TournamentReadRepository $tournamentsRepository;
    private TournamentRenderer $tournamentRenderer;

    public function __construct(
        TournamentReadRepository $tournamentsRepository,
        TournamentRenderer $tournamentRenderer,
        $config = []
    ) {
        parent::__construct($config);
        $this->tournamentsRepository = $tournamentsRepository;
        $this->tournamentRenderer = $tournamentRenderer;
    }

    public static function getRendered(): string
    {
        return self::widget();
    }

    public function run(): string
    {
        $tournaments = $this->tournamentsRepository->getActive();

        return $this->render('index', [
            'tournamentRenderer' => $this->tournamentRenderer,
            'tournaments' => $tournaments,
        ]);
    }
}