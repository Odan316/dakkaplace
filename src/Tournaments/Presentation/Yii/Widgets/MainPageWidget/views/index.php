<?php

declare(strict_types=1);

use App\Tournaments\Application\TournamentRenderer;
use App\Tournaments\Domain\Tournament;
use yii\bootstrap4\Html;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\web\View;

/**
 * @var View $this
 * @var TournamentRenderer $tournamentRenderer
 * @var array<Tournament> $tournaments
 */

?>

<h2><?php echo Yii::t('tournaments', 'Current tournaments'); ?></h2>

<div class="row mainpageList">
    <div class="col-md-12 mb-3">
        <?php echo GridView::widget([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $tournaments,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]),
            'layout' => "{items}\n{pager}",
            'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
            'emptyText' => Yii::t('tournaments', 'No tournaments active'),
            'columns' => [
                [
                    'label' => Yii::t('tournaments', 'Title'),
                    'format' => 'html',
                    'value' => function (Tournament $model) {
                        return Html::a($model->getTitle(), ['tournaments/view', 'id' => $model->getNumber()]);
                    },
                ],
                [
                    'label' => Yii::t('tournaments', 'Game type'),
                    'value' => function (Tournament $model) use ($tournamentRenderer) {
                        return $tournamentRenderer->renderTeamTypeString($model->getTeamType());
                    },
                ],
                [
                    'label' => Yii::t('tournaments', 'Ruleset'),
                    'value' => function (Tournament $model) {
                        return $model->getGameSystem()->getTitle();
                    },
                ],
                [
                    'label' => Yii::t('tournaments', 'GM'),
                    'value' => function (Tournament $model) {
                        return $model->getOwner()->getUsername();
                    },
                ],
                [
                    'label' => Yii::t('tournaments', 'Start at'),
                    'value' => function (Tournament $model) {
                        return $model->getStartAt()->format('d.m.Y');
                    },
                ],
            ],
        ]); ?>
    </div>

</div>

<div class="row mainpageList">
    <div class="col-md-12 mb-3">
        <p><?= Html::a(
                Yii::t('tournaments', 'All tournaments &raquo;'),
                ['tournaments/index'],
                ['class' => "btn btn-block btn-primary btn-lg"]
            ) ?></p>
    </div>
</div>
