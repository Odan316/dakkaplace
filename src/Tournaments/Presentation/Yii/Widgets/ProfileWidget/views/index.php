<?php

declare(strict_types=1);

use App\Tournaments\Application\TournamentRenderer;
use App\Tournaments\Domain\Tournament;
use yii\bootstrap4\Html;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\web\View;

/**
 * @var View $this
 * @var TournamentRenderer $tournamentRenderer
 * @var array<Tournament> $ownedTournaments
 * @var array<Tournament> $participatedTournaments
 */

?>

<div class="col-12 col-md-7">
    <h3><?php echo Yii::t('tournaments', 'My tournaments') ?></h3>
    <div class="tournamentsTabs">
        <ul class="nav nav-tabs" id="profileTournamentsTabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="hostTab" data-toggle="tab" href="#host" role="tab" aria-controls="host"
                   aria-selected="false">
                    <?php echo Yii::t('tournaments', "I host"); ?>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="playTab" data-toggle="tab" href="#play" role="tab" aria-controls="play"
                   aria-selected="false">
                    <?php echo Yii::t('tournaments', "I play"); ?>
                </a>
            </li>
        </ul>

        <div class="tab-content" id="profileTournamentsTabsContent">
            <div id="host" class="tab-pane fade show active" role="tabpanel" aria-labelledby="hostTab">
                <?php echo GridView::widget([
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => $ownedTournaments,
                        'pagination' => [
                            'pageSize' => 50,
                        ],
                    ]),
                    'layout' => "{items}\n{pager}",
                    'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
                    'emptyText' => Yii::t('tournaments', 'No tournaments yet!'),
                    'columns' => [
                        [
                            'label' => Yii::t('tournaments', 'Title'),
                            'format' => 'html',
                            'value' => function (Tournament $model) {
                                return Html::a($model->getTitle(), ['tournaments/view', 'id' => $model->getNumber()]);
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Status'),
                            'value' => function (Tournament $model) use ($tournamentRenderer) {
                                return $tournamentRenderer->renderStateString($model->getState());
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Ruleset'),
                            'value' => function (Tournament $model) {
                                return $model->getGameSystem()->getTitle();
                            },
                        ],
                    ],
                ]); ?>
            </div>
            <div id="play" class="tab-pane fade" role="tabpanel" aria-labelledby="playTab">
                <?php echo GridView::widget([
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => $participatedTournaments,
                        'pagination' => [
                            'pageSize' => 50,
                        ],
                    ]),
                    'layout' => "{items}\n{pager}",
                    'tableOptions' => ['class' => 'table table-bordered table-condensed table-striped'],
                    'emptyText' => Yii::t('tournaments', 'No tournaments yet!'),
                    'columns' => [
                        [
                            'label' => Yii::t('tournaments', 'Title'),
                            'format' => 'html',
                            'value' => function (Tournament $model) {
                                return Html::a($model->getTitle(), ['tournaments/view', 'id' => $model->getNumber()]);
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Status'),
                            'value' => function (Tournament $model) use ($tournamentRenderer) {
                                return $tournamentRenderer->renderStateString($model->getState());
                            },
                        ],
                        [
                            'label' => Yii::t('tournaments', 'Ruleset'),
                            'value' => function (Tournament $model) {
                                return $model->getGameSystem()->getTitle();
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>