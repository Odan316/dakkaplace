<?php

declare(strict_types=1);

namespace App\Tournaments\Presentation\Yii\Widgets\ProfileWidget;

use App\Common\Presentation\Yii\Widgets\ProfileWidget as WidgetInterface;
use App\Tournaments\Application\TournamentReadRepository;
use App\Tournaments\Application\TournamentRenderer;
use yii\base\Widget;

final class ProfileWidget extends Widget implements WidgetInterface
{
    public int $userId;

    private TournamentReadRepository $tournamentsRepository;
    private array $config;
    private TournamentRenderer $tournamentRenderer;

    public function __construct(
        TournamentReadRepository $tournamentsRepository,
        TournamentRenderer $tournamentRenderer,
        $config = []
    ) {
        parent::__construct($config);
        $this->tournamentsRepository = $tournamentsRepository;
        $this->tournamentRenderer = $tournamentRenderer;
    }

    public static function getRendered(int $userId): string
    {
        return self::widget(['userId' => $userId]);
    }

    public function run(): string
    {
        return $this->render('index', [
            'tournamentRenderer' => $this->tournamentRenderer,
            'ownedTournaments' => $this->tournamentsRepository->getActiveByOwner($this->userId),
            'participatedTournaments' => $this->tournamentsRepository->getActiveByParticipant($this->userId),
        ]);
    }
}