<?php

namespace App\Tournaments\Presentation\Yii\Forms;

use App\Tournaments\Application\Requests\TourPairRequest;
use App\Tournaments\Infrastructure\Yii\ActiveRecords\TourResult;
use Yii;
use yii\base\Model;

/**
 * Tournament results form
 *
 * @see TourResult
 */
class TourPairForm extends Model implements TourPairRequest
{
    public $playerId1 = null;
    public $playerId2 = null;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            /*[['result1', 'result2'], 'filter', 'filter' => function ($value) {
                return $value === '' ? null : $value;
            }],
            [['result1', 'result2'], 'integer', 'when' => function($model, $attribute) {
                return !is_null($model->$attribute);
            }],*/
            [['result1', 'result2'], 'safe'],
            //['playerId1', 'compare', 'compareAttribute' => 'playerId2', 'operator' => '!=', 'type' => 'number']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'playerId1' => Yii::t('tournaments', 'Player 1'),
            'playerId2' => Yii::t('tournaments', 'Player 2'),
        ];
    }

    public function getPlayer1Id(): ?int
    {
        return (int)$this->playerId1;
    }

    public function getPlayer2Id(): ?int
    {
        return (int)$this->playerId2;
    }
}
