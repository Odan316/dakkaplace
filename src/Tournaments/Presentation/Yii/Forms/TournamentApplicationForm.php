<?php

namespace App\Tournaments\Presentation\Yii\Forms;

use App\Tournaments\Application\ApplyToTournamentRequest;
use App\Tournaments\Domain\Participant;
use App\Tournaments\Domain\Tournament;
use Yii;
use yii\base\Model;

/**
 * Tournament form
 *
 * @see TournamentPatricipant
 */
class TournamentApplicationForm extends Model implements ApplyToTournamentRequest
{
    /** @var int */
    public $factionId;

    /**
     * @param Tournament $tournament
     * @param array $config
     */
    public function __construct(Tournament $tournament, array $config = [])
    {
        parent::__construct($config);

        //$tournamentPart = TournamentParticipant::find()->exact($tournamentId, Yii::$app->user->getId())->one();

        if (!empty($participant = $tournament->getParticipants()->getByUserId(Yii::$app->getUser()->getId()))) {
            $this->loadFromModel($participant);
        }
    }

    public function rules(): array
    {
        return [
            [['factionId'], 'required'],
            [['factionId'], 'integer'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'factionId' => Yii::t('tournaments', 'Game faction'),
        ];
    }

    private function loadFromModel(Participant $participant)
    {
        $this->factionId = $participant->getFaction()->getId();
    }

    public function getUserId(): int
    {
        return (int)Yii::$app->user->getId();
    }

    public function getFactionId(): int
    {
        return (int)$this->factionId;
    }
}
