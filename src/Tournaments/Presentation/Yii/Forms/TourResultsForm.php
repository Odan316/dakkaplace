<?php

declare(strict_types=1);

namespace App\Tournaments\Presentation\Yii\Forms;

use App\Tournaments\Application\Requests\TourResultRequest;
use App\Tournaments\Application\Requests\TourResultsRequest;
use App\Tournaments\Domain\PairingError;
use App\Tournaments\Domain\PairingException;
use App\Tournaments\Domain\Tournament;
use yii\base\Model;

final class TourResultsForm extends Model implements TourResultsRequest
{
    private int $tourNum;
    /** @var array<int> */
    public array $tourResults = [];

    /**
     * @throws PairingException
     */
    public function __construct(Tournament $tournament, int $tourNum, $config = [])
    {
        parent::__construct($config);

        $this->tourNum = $tourNum;
        $tour = $tournament->getTours()->getByNum($tourNum);
        if (!$tour->getPairs()->areSet()) {
            throw new PairingException(PairingError::pairsAreNotSet());
        }

        foreach ($tournament->getTours()->getByNum($tourNum)->getPairs()->getAll() as $pair) {
            $this->tourResults[$pair->getNum()] = [
                $pair->getPlayers()->getAll()[0]->getParticipantId() => $pair->getPlayers()->getAll()[0]->getResult(),
                $pair->getPlayers()->getAll()[1]->getParticipantId() => $pair->getPlayers()->getAll()[1]->getResult(),
            ];
        }
    }

    public function rules()
    {
        return [
            ['tourResults', 'safe'],
        ];
    }

    /**
     * @return array<TourResultRequest>
     */
    public function getTourResults(): array
    {
        $tourResultRequests = [];
        foreach ($this->tourResults as $pairNum => $pairsResult) {
            $tourResultRequest = new TourResultForm();
            $i = 1;
            foreach ($pairsResult as $participantId => $result) {
                if ($i == 1) {
                    $tourResultRequest->player1Id = $participantId;
                    $tourResultRequest->player1Result = $result;
                } elseif ($i == 2) {
                    $tourResultRequest->player2Id = $participantId;
                    $tourResultRequest->player2Result = $result;
                }
                $i++;
            }
            $tourResultRequests[$pairNum] = $tourResultRequest;
        }
        return $tourResultRequests;
    }

    public function getTourNum(): int
    {
        return $this->tourNum;
    }
}