<?php

declare(strict_types=1);

namespace App\Tournaments\Presentation\Yii\Forms;

use App\Tournaments\Application\Requests\TourResultRequest;
use yii\base\Model;

final class TourResultForm extends Model implements TourResultRequest
{
    /** @var int */
    public $player1Id = null;
    /** @var int */
    public $player2Id = null;
    /** @var int */
    public $player1Result = 0;
    /** @var int */
    public $player2Result = 0;

    public function rules(): array
    {
        return [
            [['player1Id', 'player2Id', 'player1Result', 'player2Result'], 'integer'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'player1Result' => '',
            'player2Result' => '',
        ];
    }

    public function getPlayer1Result(): int
    {
        return (int)$this->player1Result;
    }

    public function getPlayer2Result(): int
    {
        return (int)$this->player2Result;
    }

    public function getPlayer1Id(): int
    {
        return (int)$this->player1Id;
    }

    public function getPlayer2Id(): int
    {
        return (int)$this->player2Id;
    }
}