<?php

namespace App\Tournaments\Presentation\Yii\Forms;

use App\Tournaments\Application\TournamentCreateRequest;
use App\Tournaments\Domain\TeamType;
use DateTimeImmutable;
use Yii;
use yii\base\Model;

class TournamentCreateForm extends Model implements TournamentCreateRequest
{
    public $title;
    //public $regulations;
    //public $description;
    public $gameSystemId;
    public $teamType;
    public $startAt;
    //public $isPublic;
    //public $teamSize;
    //public $armyPointsLimit = 0;
    //public $armyPPLimit = 0;
    //public $paintingRequirements;
    //public $toursAmount = 3;
    //public $daysLength = 1;
    //public $armyListsPublic;

    public function rules(): array
    {
        return [
            [['title', 'gameSystemId', 'teamType'], 'required'],
            [
                [
                    'gameSystemId',
                    'teamType',
                    //'teamSize',
                    //'armyPointsLimit',
                    //'armyPPLimit',
                    //'paintingRequirements',
                    //'toursAmount',
                    //'daysLength',
                ],
                'integer',
            ],
            [['startAt',], 'date', 'format' => 'php:d.m.Y H:i'],
            [['title'], 'trim'],
            [['title'], 'string', 'max' => 64],
            //[['description', 'regulations'], 'string'],
            [['teamType'], 'in', 'range' => array_keys(TeamType::getValues())],
            //[['paintingRequirements'], 'in', 'range' => array_keys(Tournament::getPaintingRequirements())],
            //[['paintingRequirements'], 'default', 'value' => Tournament::PAINT_REQ_NO],
            //[['isPublic', 'armyListsPublic'], 'boolean'],
            //[['toursAmount', 'daysLength', 'isPublic', 'armyListsPublic'], 'default', 'value' => 1],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'title' => Yii::t('tournaments', 'Title'),
            'regulations' => Yii::t('tournaments', 'Regulations'),
            'description' => Yii::t('tournaments', 'Description'),
            'state' => Yii::t('tournaments', 'State'),
            'ownerId' => Yii::t('tournaments', 'Owner ID'),
            'gameSystemId' => Yii::t('tournaments', 'Game system'),
            'teamType' => Yii::t('tournaments', 'Game type'),
            'isPublic' => Yii::t('tournaments', 'Public'),
            'teamSize' => Yii::t('tournaments', 'Team size'),
            'armyPointsLimit' => Yii::t('tournaments', 'Army limit (points)'),
            'armyPPLimit' => Yii::t('tournaments', 'Army limit (PP)'),
            'paintingRequirements' => Yii::t('tournaments', 'Painting requirements'),
            'toursAmount' => Yii::t('tournaments', 'Tours amount'),
            'startAt' => Yii::t('tournaments', 'Start at'),
            'daysLength' => Yii::t('tournaments', 'Days length'),
            'armyListsPublic' => Yii::t('tournaments', 'Army lists are public'),
        ];
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getTeamType(): TeamType
    {
        return TeamType::getValueOf($this->teamType);
    }

    public function getGameSystemId(): int
    {
        return (int)$this->gameSystemId;
    }

    public function getOwnerId(): int
    {
        return (int)Yii::$app->user->getId();
    }

    public function getStartDate(): ?DateTimeImmutable
    {
        if (empty($this->startAt)) {
            return null;
        }

        return new DateTimeImmutable($this->startAt);
    }
}
