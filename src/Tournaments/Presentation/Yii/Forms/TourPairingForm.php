<?php

declare(strict_types=1);

namespace App\Tournaments\Presentation\Yii\Forms;

use App\Tournaments\Application\Requests\TourPairingRequest;
use App\Tournaments\Application\Requests\TourPairRequest;
use App\Tournaments\Domain\Tournament;
use yii\base\Model;

final class TourPairingForm extends Model implements TourPairingRequest
{
    private int $tourNum;
    /** @var array<int> */
    public array $pairs = [];

    public function __construct(Tournament $tournament, int $tourNum, $config = [])
    {
        parent::__construct($config);

        $this->tourNum = $tourNum;
        $tour = $tournament->getTours()->getByNum($tourNum);
        if ($tour->getPairs()->areSet()) {
            foreach ($tournament->getTours()->getByNum($tourNum)->getPairs()->getAll() as $pair) {
                $this->pairs[] = [
                    $pair->getPlayers()->getAll()[0]->getParticipantId(),
                    $pair->getPlayers()->getAll()[1]->getParticipantId(),
                ];
            }
        } else {
            for ($i = 0; $i < ceil($tournament->getParticipants()->getCount() / 2); $i++) {
                $this->pairs[] = [0, 0];
            }
        }
    }

    public function rules()
    {
        return [
            ['pairs', 'safe'],
        ];
    }

    /**
     * @return array<TourPairRequest>
     */
    public function getPairsRequests(): array
    {
        $pairRequests = [];
        foreach ($this->pairs as $pair) {
            $pairRequest = new TourPairForm();
            $pairRequest->playerId1 = $pair[0];
            $pairRequest->playerId2 = $pair[1];
            $pairRequests[] = $pairRequest;
        }
        return $pairRequests;
    }

    public function getTourNum(): int
    {
        return $this->tourNum;
    }
}