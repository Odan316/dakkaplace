<?php

namespace App\Tournaments\Presentation\Yii\Controllers;

use App\Tournaments\Application\FactionReadRepository;
use App\Tournaments\Application\RepositoryException;
use App\Tournaments\Application\TournamentApplyingProcessor;
use App\Tournaments\Application\TournamentCreateProcessor;
use App\Tournaments\Application\GameSystemReadRepository;
use App\Tournaments\Application\TournamentFlowProcessor;
use App\Tournaments\Application\TournamentReadRepository;
use App\Tournaments\Application\TournamentRenderer;
use App\Tournaments\Application\TournamentUpdateProcessor;
use App\Tournaments\Application\TourPairingProcessor;
use App\Tournaments\Application\TourResultsProcessor;
use App\Tournaments\Domain\PairingError;
use App\Tournaments\Domain\PairingException;
use App\Tournaments\Domain\Participant;
use App\Tournaments\Domain\Tournament;
use App\Tournaments\Presentation\Yii\Forms\TournamentApplicationForm;
use App\Tournaments\Presentation\Yii\Forms\TournamentCreateForm;
use App\Tournaments\Presentation\Yii\Forms\TournamentUpdateForm;
use App\Tournaments\Presentation\Yii\Forms\TourPairingForm;
use App\Tournaments\Presentation\Yii\Forms\TourResultsForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class TournamentsController extends Controller
{
    private TournamentReadRepository $tournamentsReadRepository;
    private TournamentCreateProcessor $createProcessor;
    private GameSystemReadRepository $gameSystemReadRepository;
    private TournamentRenderer $tournamentRenderer;
    private TournamentUpdateProcessor $updateProcessor;
    private TournamentFlowProcessor $flowProcessor;
    private FactionReadRepository $gameFactionReadRepository;
    private TournamentApplyingProcessor $applyingProcessor;
    private TourPairingProcessor $pairingProcessor;
    private TourResultsProcessor $tourResultsProcessor;

    public function __construct(
        $id,
        $module,
        TournamentReadRepository $tournamentsReadRepository,
        GameSystemReadRepository $gameSystemReadRepository,
        FactionReadRepository $gameFactionReadRepository,
        TournamentCreateProcessor $createProcessor,
        TournamentUpdateProcessor $updateProcessor,
        TournamentFlowProcessor $flowProcessor,
        TournamentApplyingProcessor $applyingProcessor,
        TourPairingProcessor $pairingProcessor,
        TourResultsProcessor $tourResultsProcessor,
        TournamentRenderer $tournamentRenderer,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->tournamentsReadRepository = $tournamentsReadRepository;
        $this->createProcessor = $createProcessor;
        $this->gameSystemReadRepository = $gameSystemReadRepository;
        $this->tournamentRenderer = $tournamentRenderer;
        $this->updateProcessor = $updateProcessor;
        $this->flowProcessor = $flowProcessor;
        $this->gameFactionReadRepository = $gameFactionReadRepository;
        $this->applyingProcessor = $applyingProcessor;
        $this->pairingProcessor = $pairingProcessor;
        $this->tourResultsProcessor = $tourResultsProcessor;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => [],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [
                            'frontend/tournaments/create',
                        ],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'open-registration',
                            'close-registration',
                            'start-game',
                            'finalize',
                            'close-game',
                            'cancel-game',
                            'autopair',
                            'manual-pair',
                            'edit-tour-results',
                        ],
                        'roles' => [
                            'frontend/tournaments/edit',
                        ],
                        'roleParams' => function () {
                            return ['tournamentId' => Yii::$app->request->get('id', 0)];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['apply'],
                        'roles' => [
                            'frontend/tournaments/apply',
                        ],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex(): string
    {
        $tournaments = $this->tournamentsReadRepository->getActive();

        return $this->render('index', [
            'tournamentRenderer' => $this->tournamentRenderer,
            'tournaments' => $tournaments,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionView($id): string
    {
        $model = $this->getModel((int)$id);

        return $this->render('view', [
            'tournamentRenderer' => $this->tournamentRenderer,
            'tournament' => $model,
        ]);
    }

    public function actionCreate(): string | Response
    {
        $form = new TournamentCreateForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $tournament = $this->createProcessor->create($form);
                return $this->redirect(['view', 'id' => $tournament->getNumber()]);
            } catch (RepositoryException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'tournamentRenderer' => $this->tournamentRenderer,
            'model' => $form,
            'gameSystems' => $this->gameSystemReadRepository->getAll(),
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id): string | Response
    {
        $model = $this->getModel((int)$id);

        $form = new TournamentUpdateForm($model);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $tournament = $this->updateProcessor->update($form);
                return $this->redirect(['view', 'id' => $tournament->getNumber()]);
            } catch (RepositoryException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'tournamentRenderer' => $this->tournamentRenderer,
            'model' => $form,
            'gameSystems' => $this->gameSystemReadRepository->getAll(),
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionOpenRegistration($id): Response
    {
        $model = $this->getModel((int)$id);

        $this->flowProcessor->openRegistration($model);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionCloseRegistration($id): Response
    {
        $model = $this->getModel((int)$id);

        $this->flowProcessor->closeRegistration($model);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionStartGame($id): Response
    {
        $model = $this->getModel((int)$id);

        $this->flowProcessor->startGame($model);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionFinalize($id): Response
    {
        $model = $this->getModel((int)$id);

        $this->flowProcessor->finalize($model);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionCloseGame($id): Response
    {
        $model = $this->getModel((int)$id);

        $this->flowProcessor->closeGame($model);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionCancelGame($id): Response
    {
        $model = $this->getModel((int)$id);

        $this->flowProcessor->cancelGame($model);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionApply($id): string | Response
    {
        $model = $this->getModel((int)$id);

        $form = new TournamentApplicationForm($model);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->applyingProcessor->apply($model, $form);
                return $this->redirect(['view', 'id' => $model->getNumber()]);
            } catch (RepositoryException $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('apply', [
            'model' => $form,
            'gameFactions' => $this->gameFactionReadRepository->getAll(),
        ]);
    }

    /**
     * @throws BadRequestHttpException
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionAutopair($id, $tour)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $tournament = $this->getModel((int)$id);
            try {
                $this->pairingProcessor->autopair($tournament, $tour);
            } catch (PairingException $e) {
                if ($e->getError()->equals(PairingError::pairsAlreadySet())) {
                    throw new BadRequestHttpException(Yii::t('tournaments',
                        'Pairs already set, you should edit them manually'));
                }
                throw new BadRequestHttpException(Yii::t('tournaments',
                    'Unknown pairing error'));
            } catch (RepositoryException $e) {
                throw new BadRequestHttpException(Yii::t('tournaments',
                    'Pairs cannot be saved'));
            }

            return [
                'result' => 'success',
                'html' => $this->renderAjax('_tourResults',
                    ['tournament' => $tournament, 'tour' => $tournament->getTour($tour)]),
            ];
        } else {
            throw new ForbiddenHttpException();
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionManualPair($id, $tour)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $tournament = $this->getModel((int)$id);
            $form = new TourPairingForm($tournament, $tour);
            $availablePlayers = ArrayHelper::map(
                $tournament->getParticipants()->getAll(),
                function (Participant $participant) {
                    return $participant->getUserId();
                }, function (Participant $participant) {
                return $participant->getUsername();
            });

            if ($form->load(Yii::$app->request->post()) && $form->validate()) {
                try {
                    $this->pairingProcessor->savePairs($tournament, $form);

                    return [
                        'result' => 'success',
                        'html' => $this->renderAjax('_tourResults',
                            ['tournament' => $tournament, 'tour' => $tournament->getTour($tour)]),
                    ];
                } catch (RepositoryException $e) {
                    return [
                        'result' => 'error',
                        'html' => $this->renderAjax('_manualPair',
                            [
                                'tournament' => $tournament,
                                'tourNum' => $tour,
                                'pairingForm' => $form,
                                'availablePlayers' => $availablePlayers,
                            ]),
                    ];
                }
            }

            return [
                'result' => '',
                'html' => $this->renderAjax('_manualPair',
                    [
                        'tournament' => $tournament,
                        'tourNum' => $tour,
                        'pairingForm' => $form,
                        'availablePlayers' => $availablePlayers,
                    ]),
            ];
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @throws NotFoundHttpException
     * @throws PairingException
     */
    public function actionEditTourResults($id, $tour)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $tournament = $this->getModel((int)$id);
            $form = new TourResultsForm($tournament, $tour);

            if ($form->load(Yii::$app->request->post()) && $form->validate()) {
                try {
                    $this->tourResultsProcessor->saveResults($tournament, $form);

                    return [
                        'result' => 'success',
                        'html' => $this->renderAjax('_tourResults',
                            ['tournament' => $tournament, 'tour' => $tournament->getTour($tour)]),
                    ];
                } catch (RepositoryException $e) {
                    return [
                        'result' => 'error',
                        'html' => $this->renderAjax('_tourResultsEdit',
                            [
                                'tournament' => $tournament,
                                'tourNum' => $tour,
                                'tourResultsForm' => $form,
                            ]),
                    ];
                }
            }

            return [
                'result' => '',
                'html' => $this->renderAjax('_tourResultsEdit',
                    [
                        'tournament' => $tournament,
                        'tourNum' => $tour,
                        'tourResultsForm' => $form,
                    ]),
            ];
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    private function getModel(int $tournamentNumber): Tournament
    {
        $model = $this->tournamentsReadRepository->getByNumber($tournamentNumber);

        if (empty($model)) {
            throw new NotFoundHttpException(Yii::t('tournaments', 'Tournament is not found'));
        }

        return $model;
    }

}
