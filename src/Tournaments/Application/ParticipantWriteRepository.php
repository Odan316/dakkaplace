<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Participant;

interface ParticipantWriteRepository
{
    public function save(Participant $participant);
}