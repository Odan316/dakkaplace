<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Faction;

interface FactionReadRepository
{
    /**
     * @return array<Faction>
     */
    public function getAll(): array;

    public function get(int $id): Faction;

    public function getNextId(): int;
}