<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Participant;

interface ParticipantExporter
{
    /**
     * @param mixed $externalEntity
     * @return Participant
     * @throws ExporterException
     */
    public function export(mixed $externalEntity): Participant;
}