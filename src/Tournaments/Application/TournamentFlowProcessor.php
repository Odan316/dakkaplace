<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Tournament;

final class TournamentFlowProcessor
{
    private TournamentWriteRepository $tournamentWriteRepository;

    public function __construct(TournamentWriteRepository $tournamentWriteRepository)
    {
        $this->tournamentWriteRepository = $tournamentWriteRepository;
    }

    public function openRegistration(Tournament $tournament): void
    {
        $tournament->openRegistration();
        $this->tournamentWriteRepository->update($tournament);
    }

    public function closeRegistration(Tournament $tournament)
    {
        $tournament->closeRegistration();
        $this->tournamentWriteRepository->update($tournament);
    }

    public function startGame(Tournament $tournament)
    {
        $tournament->startGame();
        $this->tournamentWriteRepository->update($tournament);
    }

    public function finalize(Tournament $tournament): void
    {
        /*if($model->isFinished()){
            $model->calculateResults();
        }*/
        $tournament->finish();
        $this->tournamentWriteRepository->update($tournament);
    }

    public function closeGame(Tournament $tournament)
    {
        $tournament->close();
        $this->tournamentWriteRepository->update($tournament);
    }

    public function cancelGame(Tournament $tournament)
    {
        $tournament->cancel();
        $this->tournamentWriteRepository->update($tournament);
    }
}