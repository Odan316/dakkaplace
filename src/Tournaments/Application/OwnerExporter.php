<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Owner;

interface OwnerExporter
{
    /**
     * @param mixed $externalEntity
     * @return Owner
     */
    public function export(mixed $externalEntity): Owner;
}