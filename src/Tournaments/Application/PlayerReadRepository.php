<?php

declare(strict_types = 1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Player;

interface PlayerReadRepository
{
    public function get(int $playerId): Player;
}