<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\TeamType;
use DateTimeImmutable;

interface TournamentCreateRequest
{
    public function getTitle(): string;
    
    public function getTeamType(): TeamType;
    
    public function getGameSystemId(): int;
    
    public function getOwnerId(): int;
    
    public function getStartDate(): ?DateTimeImmutable;
}