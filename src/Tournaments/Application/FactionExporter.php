<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Faction;

interface FactionExporter
{
    /**
     * @param mixed $externalEntity
     * @return Faction
     */
    public function export(mixed $externalEntity): Faction;
}