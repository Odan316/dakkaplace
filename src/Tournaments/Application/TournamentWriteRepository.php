<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Tournament;

interface TournamentWriteRepository
{
    /**
     * @param Tournament $tournament
     * @throws RepositoryException
     */
    public function add(Tournament $tournament): void;

    /**
     * @param Tournament $tournament
     * @throws RepositoryException
     */
    public function update(Tournament $tournament): void;
}