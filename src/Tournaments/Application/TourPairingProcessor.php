<?php

namespace App\Tournaments\Application;

use App\Tournaments\Application\Requests\TourPairingRequest;
use App\Tournaments\Domain\Pair;
use App\Tournaments\Domain\PairingException;
use App\Tournaments\Domain\PairsCollection;
use App\Tournaments\Domain\ParticipantResult;
use App\Tournaments\Domain\ParticipantResultCollection;
use App\Tournaments\Domain\Tournament;

class TourPairingProcessor
{
    private TournamentWriteRepository $tournamentWriteRepository;

    public function __construct(TournamentWriteRepository $tournamentWriteRepository)
    {
        $this->tournamentWriteRepository = $tournamentWriteRepository;
    }

    /**
     * @param Tournament $tournament
     * @param int $tourNum
     * @throws PairingException
     * @throws RepositoryException
     */
    public function autopair(Tournament $tournament, int $tourNum): void
    {
        $tour = $tournament->getTour($tourNum);
        if ($tour->getPairs()->areSet()) {
            throw PairingException::pairsAlreadySet();
        }

        $tour->createPairs($tournament->getParticipants());

        $this->tournamentWriteRepository->update($tournament);
    }

    /**
     * @param Tournament $tournament
     * @param TourPairingRequest $form
     * @throws RepositoryException
     */
    public function savePairs(Tournament $tournament, TourPairingRequest $form): void
    {
        $tour = $tournament->getTour($form->getTourNum());
        $pairs = new PairsCollection();
        $pairNum = 0;
        foreach($form->getPairsRequests() as $pairRequest){
            $pairNum++;
            $pairs->addPair(new Pair($pairNum, new ParticipantResultCollection([
                new ParticipantResult($pairRequest->getPlayer1Id(), $tour->getNum(), null),
                new ParticipantResult($pairRequest->getPlayer2Id(), $tour->getNum(), null),
            ])));
        }
        $tour->setPairs($pairs);

        $this->tournamentWriteRepository->update($tournament);
    }
}