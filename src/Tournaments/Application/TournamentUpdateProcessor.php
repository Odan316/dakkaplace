<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Tournament;

final class TournamentUpdateProcessor
{
    private TournamentWriteRepository $tournamentsWriteRepository;
    private GameSystemReadRepository $gameSystemReadRepository;
    private TournamentReadRepository $tournamentReadRepository;

    public function __construct(
        TournamentWriteRepository $tournamentsWriteRepository,
        TournamentReadRepository $tournamentReadRepository,
        GameSystemReadRepository $gameSystemReadRepository,
    ) {
        $this->tournamentsWriteRepository = $tournamentsWriteRepository;
        $this->tournamentReadRepository = $tournamentReadRepository;
        $this->gameSystemReadRepository = $gameSystemReadRepository;
    }

    /**
     * @throws RepositoryException
     */
    public function update(TournamentUpdateRequest $updateRequest): Tournament
    {
        $tournament = $this->tournamentReadRepository->get($updateRequest->getId());

        $tournament->setTitle($updateRequest->getTitle());
        $tournament->setTeamType($updateRequest->getTeamType());
        $tournament->setGameSystem($this->gameSystemReadRepository->get($updateRequest->getGameSystemId()));
        $tournament->setStartAt($updateRequest->getStartDate());

        $this->tournamentsWriteRepository->update($tournament);

        return $this->tournamentReadRepository->get($tournament->getId());
    }
}