<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Faction;

interface FactionWriteRepository
{
    /**
     * @throws RepositoryException
     */
    public function add(Faction $faction): void;

    /**
     * @throws RepositoryException
     */
    public function update(Faction $faction): void;
}