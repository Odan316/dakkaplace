<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\TeamType;
use DateTimeImmutable;
use Ramsey\Uuid\UuidInterface;

interface TournamentUpdateRequest
{
    public function getId(): UuidInterface;
    
    public function getTitle(): string;

    public function getTeamType(): TeamType;

    public function getGameSystemId(): int;
    
    public function getStartDate(): ?DateTimeImmutable;

    public function isNotReadyToStart(): bool;
}