<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Participant;
use App\Tournaments\Domain\Tournament;
use DateTimeImmutable;

final class TournamentApplyingProcessor
{
    private FactionReadRepository $factionReadRepository;
    private TournamentWriteRepository $tournamentWriteRepository;
    private PlayerReadRepository $playerReadRepository;

    public function __construct(
        FactionReadRepository $factionReadRepository,
        TournamentWriteRepository $tournamentWriteRepository,
        PlayerReadRepository $playerReadRepository
    ) {
        $this->factionReadRepository = $factionReadRepository;
        $this->tournamentWriteRepository = $tournamentWriteRepository;
        $this->playerReadRepository = $playerReadRepository;
    }

    /**
     * @throws RepositoryException
     */
    public function apply(Tournament $tournament, ApplyToTournamentRequest $form): void
    {
        $newParticipant = new Participant(
            $form->getUserId(),
            $this->playerReadRepository->get($form->getUserId())->getUsername(),
            $this->factionReadRepository->get($form->getFactionId()),
            new DateTimeImmutable()
        );

        $participants = $tournament->getParticipants();
        $participants->add($newParticipant);

        $this->tournamentWriteRepository->update($tournament);
    }
}