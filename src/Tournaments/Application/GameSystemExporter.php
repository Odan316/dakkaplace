<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\GameSystem;

interface GameSystemExporter
{
    /**
     * @param mixed $externalEntity
     * @return GameSystem
     */
    public function export(mixed $externalEntity): GameSystem;
}