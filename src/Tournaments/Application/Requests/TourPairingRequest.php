<?php

declare(strict_types=1);

namespace App\Tournaments\Application\Requests;

interface TourPairingRequest
{
    public function getTourNum(): int;

    /**
     * @return array<TourPairRequest>
     */
    public function getPairsRequests(): array;
}