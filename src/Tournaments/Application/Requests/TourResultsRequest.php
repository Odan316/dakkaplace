<?php

declare(strict_types=1);

namespace App\Tournaments\Application\Requests;

interface TourResultsRequest
{
    /**
     * @return array<TourResultRequest>
     */
    public function getTourResults(): array;
}