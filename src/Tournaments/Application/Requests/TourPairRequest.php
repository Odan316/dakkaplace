<?php

declare(strict_types=1);

namespace App\Tournaments\Application\Requests;

interface TourPairRequest
{
    public function getPlayer1Id(): ?int;

    public function getPlayer2Id(): ?int;
}