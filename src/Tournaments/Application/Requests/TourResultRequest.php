<?php

declare(strict_types=1);

namespace App\Tournaments\Application\Requests;

interface TourResultRequest
{
    public function getPlayer1Id(): int;

    public function getPlayer2Id(): int;

    public function getPlayer1Result(): int;

    public function getPlayer2Result(): int;
}