<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use Exception;

final class RepositoryException extends Exception
{
    
}