<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Tournament;
use Ramsey\Uuid\UuidInterface;

interface TournamentReadRepository
{
    public function get(UuidInterface $id): ?Tournament;

    public function getByNumber(int $number): ?Tournament;
    
    /**
     * @return array<Tournament>
     */
    public function getActive(): array;

    /**
     * @return array<Tournament>
     */
    public function getActiveByOwner(int $userId): array;

    /**
     * @return array<Tournament>
     */
    public function getActiveByParticipant(int $userId): array;

}