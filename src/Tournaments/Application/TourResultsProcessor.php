<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\PairingError;
use App\Tournaments\Domain\PairingException;
use App\Tournaments\Domain\Tournament;
use App\Tournaments\Presentation\Yii\Forms\TourResultsForm;

final class TourResultsProcessor
{
    private TournamentWriteRepository $tournamentWriteRepository;

    public function __construct(TournamentWriteRepository $tournamentWriteRepository)
    {
        $this->tournamentWriteRepository = $tournamentWriteRepository;
    }

    public function saveResults(Tournament $tournament, TourResultsForm $form)
    {
        $pairs = $tournament->getTour($form->getTourNum())->getPairs();
        foreach($form->getTourResults() as $pairNum => $pairRequest){
            $pair = $pairs->getByNum($pairNum);
            if(is_null($pair)){
                throw new PairingException(PairingError::pairNotExist());
            }

            $playerResult = $pair->getPlayers()->getByParticipantId($pairRequest->getPlayer1Id());
            $playerResult->setResult($pairRequest->getPlayer1Result());

            $playerResult = $pair->getPlayers()->getByParticipantId($pairRequest->getPlayer2Id());
            $playerResult->setResult($pairRequest->getPlayer2Result());
        }

        $this->tournamentWriteRepository->update($tournament);
    }

}