<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\GameSystem;

interface GameSystemReadRepository
{
    /**
     * @return array<GameSystem>
     */
    public function getAll(): array;

    public function get(int $id): GameSystem;

    public function getNextId(): int;
}