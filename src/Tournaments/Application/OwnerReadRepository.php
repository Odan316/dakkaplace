<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Owner;

interface OwnerReadRepository
{
    public function get(int $id): Owner;
}