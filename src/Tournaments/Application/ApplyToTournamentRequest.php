<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

interface ApplyToTournamentRequest
{
    public function getUserId(): int;

    public function getFactionId(): int;
}