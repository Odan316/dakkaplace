<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\GameState;
use App\Tournaments\Domain\TeamType;

interface TournamentRenderer
{
    public function renderStateString(GameState $gameState): string;

    public function renderTeamTypeString(TeamType $teamType): string;

    /**
     * @return array<string>
     */
    public function getTeamTypeListValues(): array;
}