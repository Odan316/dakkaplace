<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\GameSystem;

interface GameSystemWriteRepository
{
    /**
     * @throws RepositoryException
     */
    public function add(GameSystem $gameSystem): void;

    /**
     * @throws RepositoryException
     */
    public function update(GameSystem $gameSystem): void;
}