<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Tournaments\Domain\Tournament;

interface TournamentExporter
{
    /**
     * @param mixed $externalEntity
     * @return Tournament
     * @throws ExporterException
     */
    public function export(mixed $externalEntity): Tournament;
}