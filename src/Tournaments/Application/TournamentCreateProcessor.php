<?php

declare(strict_types=1);

namespace App\Tournaments\Application;

use App\Common\Infrastructure\TransactionManager\TransactionManager;
use App\Tournaments\Domain\Tournament;

final class TournamentCreateProcessor
{
    private TournamentWriteRepository $tournamentsWriteRepository;
    private GameSystemReadRepository $gameSystemReadRepository;
    private OwnerReadRepository $ownerReadRepository;
    private TournamentReadRepository $tournamentReadRepository;
    private TransactionManager $transactionManager;

    public function __construct(
        TournamentWriteRepository $tournamentsWriteRepository,
        TournamentReadRepository $tournamentReadRepository,
        GameSystemReadRepository $gameSystemReadRepository,
        OwnerReadRepository $ownerReadRepository,
        TransactionManager $transactionManager
    ) {
        $this->tournamentsWriteRepository = $tournamentsWriteRepository;
        $this->tournamentReadRepository = $tournamentReadRepository;
        $this->gameSystemReadRepository = $gameSystemReadRepository;
        $this->ownerReadRepository = $ownerReadRepository;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws RepositoryException
     */
    public function create(TournamentCreateRequest $createRequest): Tournament
    {
        $tournament = Tournament::createNew(
            $createRequest->getTitle(),
            $createRequest->getTeamType(),
            $this->gameSystemReadRepository->get($createRequest->getGameSystemId()),
            $this->ownerReadRepository->get($createRequest->getOwnerId()),
        );
        $tournament->setStartAt($createRequest->getStartDate());

        $this->transactionManager->transactional(function () use ($tournament): void {
            $this->tournamentsWriteRepository->add($tournament);
        });

        return $this->tournamentReadRepository->get($tournament->getId());
    }
}