<?php

declare(strict_types=1);

namespace App\Common\Presentation\Yii;

use Yii;

final class YiiPlainHtmlErrorsRenderer implements ErrorsRenderer
{
    public function validationErrorsRender(array $validationErrors): string
    {
        $text = Yii::t('common', "Errors:<br/>");
        foreach ($validationErrors as $attribute => $errors) {
            foreach ($errors as $error) {
                $text .= Yii::t('common', "{attribute}: {errorText}<br/>", [
                    'attribute' => $attribute,
                    'errorText' => $error,
                ]);
            }
        }

        return $text;
    }
}