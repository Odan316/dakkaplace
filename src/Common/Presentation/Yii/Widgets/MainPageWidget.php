<?php

declare(strict_types=1);

namespace App\Common\Presentation\Yii\Widgets;

interface MainPageWidget
{
    public static function getRendered(): string;
}