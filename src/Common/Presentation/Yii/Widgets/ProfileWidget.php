<?php

declare(strict_types=1);

namespace App\Common\Presentation\Yii\Widgets;

interface ProfileWidget
{
    public static function getRendered(int $userId): string;
}