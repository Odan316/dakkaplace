<?php

declare(strict_types=1);

namespace App\Common\Presentation\Yii;

interface ErrorsRenderer
{
    public function validationErrorsRender(array $validationErrors): string;
}