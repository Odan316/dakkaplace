<?php

declare(strict_types=1);

use yii\di\Container;

return [
    'singletons' => [
        'common.plainErrorsRenderer' => [
            'class' => App\Common\Presentation\Yii\YiiPlainHtmlErrorsRenderer::class,
        ],
        App\Common\Infrastructure\TransactionManager\TransactionManager::class => static function (Container $container
        ) {
            return new App\Common\Infrastructure\TransactionManager\SbookerTransactionManager(
                $container->get(Sbooker\TransactionManager\TransactionManager::class)
            );
        },
        Sbooker\TransactionManager\TransactionManager::class => static function (Container $container) {
            return new Sbooker\TransactionManager\TransactionManager(
                $container->get(Sbooker\TransactionManager\TransactionHandler::class)
            );
        },
        Sbooker\TransactionManager\TransactionHandler::class => static function (Container $container) {
            /** @var Sbooker\TransactionManager\Yii2ActiveRecord\UnitOfWork $uow */
            //$uow = $container->get(Sbooker\TransactionManager\Yii2ActiveRecord\UnitOfWork::class);
            return new Sbooker\TransactionManager\Yii2ActiveRecord\TransactionHandler(
                \Yii::$app->getDb(),
                new Sbooker\TransactionManager\Yii2ActiveRecord\UnitOfWork()
            );
        },
    ],
];