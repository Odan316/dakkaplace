<?php

declare(strict_types = 1);

namespace App\Common\Infrastructure\TransactionManager;

use Throwable;

interface TransactionManager
{
    /**
     * @throws Throwable
     */
    public function transactional(callable $function): mixed;
}