<?php

declare(strict_types = 1);

namespace App\Common\Infrastructure\TransactionManager;

use Sbooker\TransactionManager\TransactionManager as VendorTransactionManager;
use Throwable;

final class SbookerTransactionManager implements TransactionManager
{
    private VendorTransactionManager $transactionManager;

    public function __construct(VendorTransactionManager $transactionManager)
    {
        $this->transactionManager = $transactionManager;
    }

    /**
     * @throws Throwable
     */
    public function transactional(callable $function): mixed
    {
        return $this->transactionManager->transactional($function);
    }
}