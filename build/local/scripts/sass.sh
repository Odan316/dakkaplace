#!/usr/bin/env bash

sass ./backend/web/css/styles.scss:./backend/web/css/styles.css
sass ./frontend/web/css/styles.scss:./frontend/web/css/styles.css
sass ./src/Tournaments/Presentation/Yii/web/css/tournaments.scss:./src/Tournaments/Presentation/Yii/web/css/tournaments.css