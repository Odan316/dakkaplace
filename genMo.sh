#!/usr/bin/env bash

msgfmt -ocommon/messages/ru_RU/yii.mo -v common/messages/ru_RU/yii.po
msgfmt -ocommon/messages/ru_RU/common.mo -v common/messages/ru_RU/common.po
msgfmt -ofrontend/messages/ru_RU/yii.mo -v frontend/messages/ru_RU/frontend.po
msgfmt -obackend/messages/ru_RU/yii.mo -v backend/messages/ru_RU/backend.po
msgfmt -ocommon/modules/video/messages/ru_RU/yii.mo -v common/modules/video/messages/ru_RU/video.po
msgfmt -ocommon/modules/community/messages/ru_RU/yii.mo -v common/modules/community/messages/ru_RU/community.po